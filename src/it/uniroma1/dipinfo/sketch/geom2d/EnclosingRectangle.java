package it.uniroma1.dipinfo.sketch.geom2d;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Calculates the enclosing rectangle for a geometrical figure, known its
 * convex hull;
 */
public class EnclosingRectangle {

    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(EnclosingRectangle.class.getName());

    private EnclosingRectangle() {
    }

    /**
     * Calculates the enclosing rectangle for a geometrical figure, known its
     * convex hull;
     *
     * @param theConvexHullPoints list of points of the Convex Hull
     * @return list of points of the enclosing rectangle
     */
    public static List calcEnclosingRect(List theConvexHullPoints) {

        List retEnclosingRect = null;

        // Controllo casi banali
        if (theConvexHullPoints.size() == 1) {

            // Caso banale 1: un solo punto
            LOG_.info("[Enclosing] Caso: 1 solo punto");

            retEnclosingRect = new ArrayList();

            Point2D p = (Point2D) theConvexHullPoints.get(0);
            retEnclosingRect.add(p);
            retEnclosingRect.add(p);
            retEnclosingRect.add(p);
            retEnclosingRect.add(p);

        } else if (theConvexHullPoints.size() == 2) {

            // Caso banale 2: due soli punti
            LOG_.info("[Enclosing] Caso: 2 soli punti");

            retEnclosingRect = new ArrayList();

            Point2D p1 = (Point2D) theConvexHullPoints.get(0);
            Point2D p2 = (Point2D) theConvexHullPoints.get(1);
            retEnclosingRect.add(p1);
            retEnclosingRect.add(p2);
            retEnclosingRect.add(p2);
            retEnclosingRect.add(p1);

        } else {

            // Caso normale
            double minx = Double.MAX_VALUE, miny = Double.MAX_VALUE;
            double maxx = Double.MIN_VALUE, maxy = Double.MIN_VALUE;
            int minxp = 0, minyp = 0, maxxp = 0, maxyp = 0;
            double min_area = 0;
            Point2D p1ok = null, p2ok = null, p3ok = null, p4ok = null;

            for (int i = 0; i < theConvexHullPoints.size() - 1; i++) {
                LOG_.debug("p = " + theConvexHullPoints.get(i));
                for (int a = 0; a < theConvexHullPoints.size(); a++) {

                    Line2D v1 = new Line2D.Double(
                            (Point2D) theConvexHullPoints.get(i),
                            (Point2D) theConvexHullPoints.get(i + 1));
                    Line2D v2 = new Line2D.Double(
                            (Point2D) theConvexHullPoints.get(i),
                            (Point2D) theConvexHullPoints.get(a));
                    double ang = MiscGeometry.angle(v1, v2);
                    LOG_.debug("\tv1=(" + v1.getP1() + "-" + v1.getP2() + ")");
                    LOG_.debug("\tv2=(" + v2.getP1() + "-" + v2.getP2() + ")");
                    LOG_.debug("\tangle(v1,v2)=" + ang);

                    double dis = v2.getP1().distance(v2.getP2());
                    LOG_.debug("\tv2.length=" + dis);
                    double xx = dis * Math.cos(ang);
                    double yy = dis * Math.sin(ang);

                    if (a == 0) /*(!a)*/ {
                        minx = maxx = xx;
                        miny = maxy = yy;
                        minxp = maxxp = minyp = maxyp = 0;
                    }
                    if (xx < minx) {
                        minxp = a;
                        minx = xx;
                    }
                    if (xx > maxx) {
                        maxxp = a;
                        maxx = xx;
                    }
                    if (yy < miny) {
                        minyp = a;
                        miny = yy;
                    }
                    if (yy > maxy) {
                        maxyp = a;
                        maxy = yy;
                    }
                    LOG_.debug("\txx,yy    =(" + xx + "," + yy + ")");
                    LOG_.debug("\tminx,maxx=(" + minx + "," + maxx + ")");
                    LOG_.debug("\tminy,maxy=(" + miny + "," + maxy + ")");
                }

                Line2D l = new Line2D.Double(
                        (Point2D) theConvexHullPoints.get(i),
                        (Point2D) theConvexHullPoints.get(i + 1));
                Point2D p1 = MiscGeometry.closest(l, (Point2D) theConvexHullPoints.get(minxp));
                LOG_.debug("closest(l,minxp)=" + p1);
                Point2D p2 = MiscGeometry.closest(l, (Point2D) theConvexHullPoints.get(maxxp));
                LOG_.debug("closest(l,maxxp)=" + p2);

                Point2D paux = new Point2D.Double(
                        ((Point2D) theConvexHullPoints.get(i)).getX() + 100,
                        ((Point2D) theConvexHullPoints.get(i)).getY());
                Line2D v3 = new Line2D.Double((Point2D) theConvexHullPoints.get(i), paux);
                Line2D v4 = new Line2D.Double(
                        (Point2D) theConvexHullPoints.get(i),
                        (Point2D) theConvexHullPoints.get(i + 1));
                double ang = MiscGeometry.angle(v3, v4);

                Point2D paux1 = new Point2D.Double(
                        p1.getX() + 100 * Math.cos(ang + Math.PI / 2),
                        p1.getY() + 100 * Math.sin(ang + Math.PI / 2));
                Point2D paux2 = new Point2D.Double(
                        p2.getX() + 100 * Math.cos(ang + Math.PI / 2),
                        p2.getY() + 100 * Math.sin(ang + Math.PI / 2));

                Point2D p3 = MiscGeometry.closest(
                        //                    new Line2D.Double(p2, paux2), (Point2D) theConvexHullPoints.get(maxyp));
                        new Line2D.Double(p2, paux2), (Point2D) theConvexHullPoints.get(minyp));
                Point2D p4 = MiscGeometry.closest(
                        new Line2D.Double(p1, paux1), (Point2D) theConvexHullPoints.get(minyp));

                double area = MiscGeometry.calcQuadArea(p1, p2, p3, p4);

                if (i == 0 /*(!i)*/ || (area < min_area)) {
                    min_area = area;
                    p1ok = p1;
                    p2ok = p2;
                    p3ok = p3;
                    p4ok = p4;
                }
            }

            retEnclosingRect = new ArrayList();
            retEnclosingRect.add(p1ok);
            retEnclosingRect.add(p2ok);
            retEnclosingRect.add(p3ok);
            retEnclosingRect.add(p4ok);
        }
        return retEnclosingRect;
    }
}

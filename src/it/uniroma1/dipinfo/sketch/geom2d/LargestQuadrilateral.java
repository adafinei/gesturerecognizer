package it.uniroma1.dipinfo.sketch.geom2d;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Calculates the list of points of the largest enclosed quadrilateral for 
 * a figure, given its convex hull points
 */
public class LargestQuadrilateral extends AbstractLargestPolygon {

    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(LargestQuadrilateral.class.getName());

    private LargestQuadrilateral() {
    }

    /**
     * Calculates the list of points of the largest enclosed quadrilateral for
     * a figure, given its convex hull points
     *
     * @param theConvexHullPoints List of Convex Hull points
     *
     * @return list of the largest quadrilateral points
     */
    public static List calcLargestQuad(List theConvexHullPoints) {

        List retLargestQuadPoints = null;

        // Controllo caso banale
        if (theConvexHullPoints.size() <= 4) {

            retLargestQuadPoints = new ArrayList();
            for (int i = 0; i < theConvexHullPoints.size(); i++) {
                Point2D p = (Point2D) theConvexHullPoints.get(i);
                retLargestQuadPoints.add(p);
            }
            Point2D p0 = (Point2D) theConvexHullPoints.get(0);
            for (int i = theConvexHullPoints.size(); i < 5; i++) {
                retLargestQuadPoints.add(p0);
            }

        } else {

            // Calcolo di uno tra i rooted triangle

            // Indici dei punti del rooted triangle
            int ripa = 0;
            int ripb = 0;
            int ripc = 0;

            int ia = 0;
            double triArea = 0;
            LOG_.debug("[1] ia = " + ia);
            LOG_.debug("[1] ib = [" + 1 + "," + (theConvexHullPoints.size() - 2) + "]");
            for (int ib = 1; ib <= theConvexHullPoints.size() - 2; ib++) {
                LOG_.debug("[1]\t ib = " + ib);
                int ic = (ib >= 2 ? ib + 1 : 2);
                LOG_.debug("[1]\t\t ic = " + ic);
                Object[] rootedTriInfo = compRootedTri(theConvexHullPoints, ia, ib, ic);
                double area = ((Double) rootedTriInfo[0]).doubleValue();
                ic = ((Integer) rootedTriInfo[1]).intValue();
                LOG_.debug("[1]\t\t ic = " + ic + " ==> area = " + area);
                if (area > triArea) {
                    triArea = area;
                    ripa = ia;
                    ripb = ib;
                    ripc = ic;
                    LOG_.debug("[1]\t\t Trovato un rooted triangle di area = "
                            + triArea + " per i punti " + ripa + "," + ripb + "," + ripc);
                }
            }

            // Calcolo del rooted quadrilateral basato su di un rooted triangle

            // Indici per i valori finali
            int fipa = 0;
            int fipb = 0;
            int fipc = 0;
            int fipd = 0;

            double quadArea = 0;
            LOG_.debug("[2] ia = " + ia);
            LOG_.debug("[2] ib = [" + (ripa + 1) + "," + ripb + "]");
            for (int ib = ripa + 1; ib <= ripb; ib++) {
                LOG_.debug("[2]\t ib = " + ib);
                int ic0 = (ib == ripb ? ripb + 1 : ripb);
                LOG_.debug("[2]\t ic = [" + ic0 + "," + ripc + "]");
                for (int ic = ic0; ic <= ripc; ic++) {
                    LOG_.debug("[2]\t\t ic = " + ic);
                    int id = (ic == ripc ? ripc + 1 : ripc);
                    Object[] rootedQuadInfo = compRootedQuad(theConvexHullPoints, ia, ib, ic, id);
                    double area = ((Double) rootedQuadInfo[0]).doubleValue();
                    id = ((Integer) rootedQuadInfo[1]).intValue();
                    LOG_.debug("[2]\t\t id = " + id + " ==> area = " + area);
                    if (area > quadArea) {
                        quadArea = area;
                        fipa = ia;
                        fipb = ib;
                        fipc = ic;
                        fipd = id;
                        LOG_.debug("[2]\t\t Trovato rooted quadrilateral di area = "
                                + quadArea + " per i punti " + fipa + "," + fipb + "," + fipc + "," + fipd);
                    }
                }
            }

            // Calcola gli altri quadrilaterals e ne sceglie il piu' grande
            double finalArea = quadArea;
            int pf0 = fipa;
            int pf1 = fipb;
            int pf2 = fipc;
            int pf3 = fipd;
            ripa = fipa;
            ripb = fipb;
            ripc = fipc;
            int ripd = fipd;

            LOG_.debug("[3] ia = [" + (ripa + 1) + "," + ripb + "]");
            for (ia = ripa + 1; ia <= ripb; ia++) {
                LOG_.debug("[3]\t ia = " + ia);
                int ib0 = (ia == ripb ? ripb + 1 : ripb);
                quadArea = 0;
                LOG_.debug("[3]\t ib = [" + ib0 + "," + ripc + "]");
                for (int ib = ib0; ib <= ripc; ib++) {
                    LOG_.debug("[3]\t\t ib = " + ib);
                    int ic0 = (ib == ripc ? ripc + 1 : ripc);
                    LOG_.debug("[3]\t\t ic = [" + ic0 + "," + ripd + "]");
                    for (int ic = ic0; ic <= ripd; ic++) {
                        int id = (ic == ripd ? ripd + 1 : ripd);
                        Object[] rootedQuadInfo = compRootedQuad(theConvexHullPoints, ia, ib, ic, id);
                        double area = ((Double) rootedQuadInfo[0]).doubleValue();
                        id = ((Integer) rootedQuadInfo[1]).intValue();
                        LOG_.debug("[3]\t\t\t id = " + id + " ==> area = " + area);
                        if (area > quadArea) {
                            quadArea = area;
                            fipa = ia;
                            fipb = ib;
                            fipc = ic;
                            fipd = id;
                            LOG_.debug("[3] Trovato rooted quadrilateral di area = "
                                    + quadArea + " per i punti " + fipa + "," + fipb + "," + fipc + "," + fipd);
                        }
                    }
                }

                if (quadArea > finalArea) {
                    finalArea = quadArea;
                    pf0 = fipa;
                    pf1 = fipb;
                    pf2 = fipc;
                    pf3 = fipd;
                    LOG_.debug("Trovato largest quadrilateral di area = " + quadArea);
                }
            }

            // Aggiunge i punti alla lista punti ritornata (largest quadrilateral)
            retLargestQuadPoints = new ArrayList();
            retLargestQuadPoints.add(theConvexHullPoints.get(pf0));
            retLargestQuadPoints.add(theConvexHullPoints.get(pf1));
            retLargestQuadPoints.add(theConvexHullPoints.get(pf2));
            retLargestQuadPoints.add(theConvexHullPoints.get(pf3));
            // retLargestQuadPoints.add(theConvexHullPoints.get(pf0));
        }

        return retLargestQuadPoints;
    }
}

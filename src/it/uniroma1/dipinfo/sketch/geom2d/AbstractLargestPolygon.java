package it.uniroma1.dipinfo.sketch.geom2d;

import java.awt.geom.Point2D;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Abstract class for the Largest Polygon calculation
 */
abstract public class AbstractLargestPolygon {
    
    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(AbstractLargestPolygon.class.getName());


    protected AbstractLargestPolygon() {}


    /**
     * Auxiliary method for the calculations of the rooted triangle
     * 
     * @param theConvexHullPoints convex hull points list
     * @param ripa index of the first point of the rooted triangle
     * @param ripb index of the second point of the rooted triangle
     * @param ripc index of the third point of the rooted triangle
     *
     * @return array 
     */
    protected static Object[] compRootedTri(List theConvexHullPoints,
        int ripa, int ripb, int ripc) {

        double retTrigArea = 0;
        int    retRipc     = ripc;

        // Calcola un rooted triangle
        int ia = ripa;
        int ib = ripb;
        LOG_.debug("[1t]\t\t ic = [" + retRipc + "," + (theConvexHullPoints.size() - 1) + "]");
        for (int ic = retRipc; ic <= /* ATT < */ theConvexHullPoints.size() - 1; ic++) {
            Point2D pa = (Point2D) theConvexHullPoints.get(ia);
            Point2D pb = (Point2D) theConvexHullPoints.get(ib);
            Point2D pc = (Point2D) theConvexHullPoints.get(ic);
            double area = MiscGeometry.calcTriangleArea(pa, pb, pc);
            LOG_.debug("[1t]\t\t\t Area = " + area);
            if (area > retTrigArea) {
                retRipc     = ic;
                retTrigArea = area;
            }
            else {
                LOG_.debug("[1t]\t\t\t break");
                break;
            }
        }

        return new Object[] {new Double(retTrigArea), new Integer(retRipc)};
    }


    /**
     * Auxiliary method for the calculations of the rooted quadrilateral.
     * Used during calculations for the largest quadrilateral
     * 
     * @param theConvexHullPoints list of the convex hull points
     * @param ripa index of the first point of the rooted quadrilateral
     * @param ripb index of the second point of the rooted quadrilateral
     * @param ripc index of the third point of the rooted quadrilateral
     * @param ripd index of the fourth point of the rooted quadrilateral
     *
     * @return array 
     */
    protected static Object[] compRootedQuad(List theConvexHullPoints,
        int ripa, int ripb, int ripc, int ripd) {

        double retQuadArea = 0;
        int    retRipd     = ripd;

        // Calcola un rooted triangle        
        Point2D pa = (Point2D) theConvexHullPoints.get(ripa);
        Point2D pb = (Point2D) theConvexHullPoints.get(ripb);
        Point2D pc = (Point2D) theConvexHullPoints.get(ripc);
        for (int id = ripd; id <= /* ATT < */ theConvexHullPoints.size() - 1; id++) {
            Point2D pd = (Point2D) theConvexHullPoints.get(id);
            double area = MiscGeometry.calcQuadArea(pa, pb, pc, pd);
            if (area > retQuadArea) {
                retRipd = id;
                retQuadArea = area;
            }
            else {
                break;
            }
        }

        return new Object[] {new Double(retQuadArea), new Integer(retRipd)};
    }

}

package it.uniroma1.dipinfo.sketch.geom2d;

import it.uniroma1.dipinfo.sketch.gui.ResultPanel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Wrapper class for the extremal polygons calculations
 */
public class ExtremalPolygonsWrapper {

    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(ExtremalPolygonsWrapper.class.getName());
    private List extremalPolyPoints_;
    private double extremalPolyPerim_ = -1;
    private double extremalPolyArea_ = -1;

    public ExtremalPolygonsWrapper() {
    }

    public List calcLargestTriangle(List theConvexHullPoints) {
        extremalPolyPoints_ = LargestTriangle.calcLargestTriangle(theConvexHullPoints);
        //System.out.println("Largest = " + extremalPolyPoints_);
        extremalPolyPerim_ = extremalPolyArea_ = -1;
        return getExtremalPolyPoints_();
    }

    public List calcLargestQuadrilateral(List theConvexHullPoints) {
        extremalPolyPoints_ = LargestQuadrilateral.calcLargestQuad(theConvexHullPoints);
        //System.out.println("Largest = " + extremalPolyPoints_);
        extremalPolyPerim_ = extremalPolyArea_ = -1;
        return getExtremalPolyPoints_();
    }

    public List calcEnclosingRectangle(List theConvexHullPoints) {
        extremalPolyPoints_ = EnclosingRectangle.calcEnclosingRect(theConvexHullPoints);
        //System.out.println("Enclosing = " + extremalPolyPoints_);
        extremalPolyPerim_ = extremalPolyArea_ = -1;
        return getExtremalPolyPoints_();
    }

    public double getQuadrilateralPerimeter() {
        if (extremalPolyPerim_ == -1) {
            extremalPolyPerim_ = MiscGeometry.calcPerimeter(getExtremalPolyPoints_());
        }
        return (extremalPolyPerim_ != 0 ? extremalPolyPerim_ : 0.00000000001);
    }

    public double getQuadrilateralArea() {
        if (extremalPolyArea_ == -1) {
            System.out.println("quadarea, size = " + getExtremalPolyPoints_().size());
            extremalPolyArea_ = MiscGeometry.calcQuadArea(
                    (Point2D) getExtremalPolyPoints_().get(0), (Point2D) getExtremalPolyPoints_().get(1),
                    (Point2D) getExtremalPolyPoints_().get(2), (Point2D) getExtremalPolyPoints_().get(3));
        }
        return (extremalPolyArea_ != 0 ? extremalPolyArea_ : 0.00000000001);
    }

    public Dimension2D getDimension() {

        Dimension2D retDim = new Dimension2D() {

            private double width_;
            private double height_;

            public double getHeight() {
                return height_;
            }

            public double getWidth() {
                return width_;
            }

            public void setSize(double width, double height) {
                width_ = width;
                height_ = height;
            }

            @Override
            public void setSize(Dimension2D d) {
                setSize(d.getWidth(), d.getHeight());
            }
        };

        Point2D p1 = (Point2D) getExtremalPolyPoints_().get(0);
        Point2D p2 = (Point2D) getExtremalPolyPoints_().get(1);
        if (p1.equals(p2)) {
            // Caso banale 1: un solo punto
            LOG_.info("[Enclosing] Area di 1 solo punto");
            retDim.setSize(0, 0);
        } else if (p1.equals(getExtremalPolyPoints_().get(3))
                && p2.equals(getExtremalPolyPoints_().get(2))) {
            LOG_.info("[Enclosing] Area di 2 soli punti");
            retDim.setSize(p1.distance(p2), 0);
        } else {
            Point2D top = null;
            Point2D bottom = null;
            Point2D left = null;
            Point2D right = null;
            for (int i = 0; i < 4; i++) {
                Point2D p = (Point2D) getExtremalPolyPoints_().get(i);
                if (top == null || top.getY() < p.getY()) {
                    top = p;
                }
                if (bottom == null || bottom.getY() > p.getY()) {
                    bottom = p;
                }
                if (left == null || left.getX() > p.getX()) {
                    left = p;
                }
                if (right == null || right.getX() < p.getX()) {
                    right = p;
                }
            }

            LOG_.debug("top=" + top);
            LOG_.debug("bottom=" + bottom);
            LOG_.debug("left=" + left);
            LOG_.debug("right=" + right);
            LOG_.debug("top.distance(right)   =" + top.distance(right));
            LOG_.debug("bottom.distance(left) =" + bottom.distance(left));
            LOG_.debug("right.distance(bottom)=" + right.distance(bottom));
            LOG_.debug("top.distance(left)    =" + top.distance(left));

            double topToRight = top.distance(right);
            double bottomToLeft = bottom.distance(left);
            double rightToBottom = right.distance(bottom);
            double topToLeft = top.distance(left);

            retDim.setSize(
                    (topToRight != 0 ? topToRight : bottomToLeft),
                    (rightToBottom != 0 ? rightToBottom : topToLeft));
        }
        return retDim;
    }

    public void drawPerimeter(ResultPanel thePanel, Color thePerimeterColor) {

        Graphics2D g = thePanel.getDrawableGraphics();

        Iterator it = getExtremalPolyPoints_().iterator();
        Point2D firstP = (Point2D) it.next();
        Point2D prev = firstP;
        Point2D next = null;
        Color origColor = g.getColor();
        g.setColor(thePerimeterColor);
        for (; it.hasNext();) {
            next = (Point2D) it.next();
            g.drawLine(
                    (int) Math.round(prev.getX()), (int) Math.round(prev.getY()),
                    (int) Math.round(next.getX()), (int) Math.round(next.getY()));
            prev = next;
        }

        g.drawLine(
                (int) Math.round(prev.getX()), (int) Math.round(prev.getY()),
                (int) Math.round(firstP.getX()), (int) Math.round(firstP.getY()));

        g.setColor(origColor);
    }

    public void drawPoints(ResultPanel thePanel, Color thePointsColor,
            int thePointsWidth, int thePointsHeight) {

        Graphics2D g = thePanel.getDrawableGraphics();

        Color origColor = g.getColor();
        g.setColor(thePointsColor);

        for (Iterator it = getExtremalPolyPoints_().iterator(); it.hasNext();) {
            Point2D p = (Point2D) it.next();
            g.fillOval(
                    (int) Math.round(p.getX() - thePointsWidth / 2),
                    (int) Math.round(p.getY() - thePointsHeight / 2),
                    thePointsWidth, thePointsHeight);
        }
        g.setColor(origColor);
    }

    /**
     * @return the extremalPolyPoints_
     */
    public List getExtremalPolyPoints_() {
        return extremalPolyPoints_;
    }
}

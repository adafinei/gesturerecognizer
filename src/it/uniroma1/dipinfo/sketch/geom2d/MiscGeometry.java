package it.uniroma1.dipinfo.sketch.geom2d;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Miscellaneous geometry functions
 */
public class MiscGeometry {

    /** Logger per console */
    private static final Logger LOG_ = Logger.getLogger(MiscGeometry.class.getName());

    private MiscGeometry() {
    }

    /**
     * Calculates the area of a triangle through its points' coordinates,
     * using the formula for a polygon area.
     *
     * @param p1 point 1 of the triangle
     * @param p2 point 2 of the triangle
     * @param p3 point 3 of the triangle
     *
     * @return area of the triangle made by the 3 input points
     */
    public static double calcTriangleArea(Point2D p1, Point2D p2, Point2D p3) {

        double area = p1.getX() * p2.getY() - p2.getX() * p1.getY();
        area += p2.getX() * p3.getY() - p3.getX() * p2.getY();
        area += p3.getX() * p1.getY() - p1.getX() * p3.getY();

        return Math.abs(area / 2);
    }

    /**
     * Calculates the perimeter of a figure, given its points
     *
     * @param points the points of the figure
     *
     * @return the perimeter of the figure
     */
    public static double calcPerimeter(List points) {

        double retPerim = 0.0;

        if (points.size() > 1) {
            Iterator it = points.iterator();
            Point2D prev = (Point2D) it.next();
            while (it.hasNext()) {
                Point2D p = (Point2D) it.next();
                retPerim += Point2D.distance(prev.getX(), prev.getY(), p.getX(), p.getY());
                prev = p;
            }
            Point2D first = (Point2D) points.get(0);
            retPerim += Point2D.distance(prev.getX(), prev.getY(), first.getX(), first.getY());
        }

        return retPerim;
    }

    /**
     * Calculates the area of a square through its points' coordinates,
     * using the formula for a polygon area.
     *
     * @param p1 point 1 of the square
     * @param p2 point 2 of the square
     * @param p3 point 3 of the square
     * @param p4 point 4 of the square
     *
     * @return area of the square given by the four input points
     */
    public static double calcQuadArea(Point2D p1, Point2D p2, Point2D p3, Point2D p4) {

        double area = p1.getX() * p2.getY() - p2.getX() * p1.getY();
        area += p2.getX() * p3.getY() - p3.getX() * p2.getY();
        area += p3.getX() * p4.getY() - p4.getX() * p3.getY();
        area += p4.getX() * p1.getY() - p1.getX() * p4.getY();

        return Math.abs(area / 2);
    }

    /**
     * Calculates a line's closest point to another point
     *
     * @param theLine line from which to find the distance
     * @param thePoint pivot point
     *
     * @return closest point to the pivot on the line
     */
    public static Point2D closest(Line2D theLine, Point2D thePoint) {

        int d = (int) Math.round(theLine.getX2() - theLine.getX1());

        if (d == 0) {
            return new Point2D.Double(theLine.getX1(), thePoint.getY());
        }

        if (theLine.getP1().equals(thePoint)) {
            return thePoint;
        }

        if (theLine.getP2().equals(thePoint)) {
            return thePoint;
        }

        double m = (theLine.getY2() - theLine.getY1()) / d;

        if (m == 0) {
            return new Point2D.Double(thePoint.getX(), theLine.getY1());
        }

        double b1 = theLine.getY2() - m * theLine.getX2();
        double b2 = thePoint.getY() + 1 / m * thePoint.getX();
        double x = (b2 - b1) / (m + 1 / m);
        double y = m * x + b1;

        return new Point2D.Double(x, y);
    }

    public static double angle(Line2D a, Line2D b) {

        Double cr = cross(a, b);
        Double d = dot(a, b);

        return Math.atan2(cr, d);
    }

    public static double cross(Line2D a, Line2D b) {
        double dx1 = a.getX2() - a.getX1();
        double dx2 = b.getX2() - b.getX1();
        double dy1 = a.getY2() - a.getY1();
        double dy2 = b.getY2() - b.getY1();

        return dx1 * dy2 - dy1 * dx2;
    }

    public static double dot(Line2D a, Line2D b) {
        double dx1 = a.getX2() - a.getX1();
        double dx2 = b.getX2() - b.getX1();
        double dy1 = a.getY2() - a.getY1();
        double dy2 = b.getY2() - b.getY1();
        return dx1 * dx2 + dy1 * dy2;
    }
}

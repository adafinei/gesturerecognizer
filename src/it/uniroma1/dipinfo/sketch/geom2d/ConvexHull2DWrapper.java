package it.uniroma1.dipinfo.sketch.geom2d;

import it.uniroma1.dipinfo.sketch.gui.DrawPanel;
import it.uniroma1.dipinfo.sketch.gui.ResultPanel;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Wrapper to the ConvexHull2D class which is a porting to Java
 * of C algorithms
 */
public class ConvexHull2DWrapper {

    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(ConvexHull2DWrapper.class.getName());
    private ConvexHull2D cHull_;
    private cVertexList vertexOriginalList_;
    private cVertexList vertexInputList_;
    private cVertexList vertexHullList_;
    private double convexHullArea_ = -1;
    private double convexHullPerim_ = -1;

    /**
     * Creates a new instance of ConvexHull2DWrapper
     */
    public ConvexHull2DWrapper() {
    }

    /**
     * Sets the list of points
     * @param thePoints
     */
    public void setPoints(Collection thePoints) {

        vertexOriginalList_ = new cVertexList();

        for (Iterator it = thePoints.iterator(); it.hasNext();) {
            Point2D p = (Point2D) it.next();
            vertexOriginalList_.SetVertex(
                    (int) Math.round(p.getX()),
                    (int) Math.round(p.getY()));
        }

        convexHullArea_ = -1;
        convexHullPerim_ = -1;
    }

    /**
     * Launches calculations for the convex hull
     */
    public void calcConvexHull() {
        vertexInputList_ = new cVertexList();
        vertexOriginalList_.ListCopy(vertexInputList_);
        cHull_ = new ConvexHull2D(vertexInputList_);
        vertexHullList_ = cHull_.RunHull();
    }

    /**
     * Gets the points of the Convex Hull
     * @return a list containing the Convex Hull points
     */
    public List getConvexHullPoints() {

        List retConvexHullPoints = new ArrayList(vertexHullList_.n);

        final int vTotal = vertexHullList_.n;
        for (int i = 0; i < vTotal; i++) {
            cVertex vCurr = vertexHullList_.GetElement(i);
            retConvexHullPoints.add(new Point2D.Double(vCurr.v.x, vCurr.v.y));
        }

        return retConvexHullPoints;
    }

    /**
     * Calculates the area of the convex hull
     * @return the value of the area of the convex hull
     */
    public double calcConvexHullArea() {
        if (convexHullArea_ == -1) {
            convexHullArea_ = 0;
            final int vTotal = vertexHullList_.n;
            for (int i = 0; i < vTotal; i++) {
                cVertex vCurr = vertexHullList_.GetElement(i);
                int xCurr = vCurr.v.x;
                int yCurr = vCurr.v.y;
                cVertex vNext = vertexHullList_.GetElement((i + 1) % vTotal);
                int xNext = vNext.v.x;
                int yNext = vNext.v.y;
                convexHullArea_ += xCurr * yNext - yCurr * xNext;
                if (LOG_.isDebugEnabled()) {
                    LOG_.debug("Area tra (" + vCurr.v.x + "," + vCurr.v.y + ") e ("
                            + vNext.v.x + "," + vNext.v.y + ")");
                }
            }
            convexHullArea_ = Math.abs(convexHullArea_ / 2);
        }

        return (convexHullArea_ != 0 ? convexHullArea_ : 0.00000000001);
    }

    /**
     * Calculates the area of the convex hull
     * @return the value of the area of the convex hull
     */
    public double calcConvexHullArea2() {
        return vertexHullList_.AreaPoly2() / 2;
    }

    /**
     * Calculates the perimeter of the convex hull
     * @return the value of the perimeter of the convex hull
     */
    public double calcConvexHullPerimeter() {
        if (convexHullPerim_ == -1) {
            convexHullPerim_ = 0;
            final int vTotal = vertexHullList_.n;
            for (int i = 0; i < vTotal; i++) {
                cVertex vCurr = vertexHullList_.GetElement(i);
                int xCurr = vCurr.v.x;
                int yCurr = vCurr.v.y;
                cVertex vNext = vertexHullList_.GetElement((i + 1) % vTotal);
                int xNext = vNext.v.x;
                int yNext = vNext.v.y;
                convexHullPerim_ += Point2D.distance(xCurr, yCurr, xNext, yNext);
                //System.out.println("Distanza tra (" + vCurr.v.x + "," + vCurr.v.y + " e (" +
                //vNext.v.x + "," + vNext.v.y + ")");
            }
        }

        return convexHullPerim_;
    }

    /**
     * Draws the user's stroke
     */
    public void drawInputPoints(ResultPanel thePanel) {
        vertexOriginalList_.DrawPoints(thePanel.getDrawableGraphics(), 10, 10);
        vertexOriginalList_.PrintVertices();
    }

    /**
     * Draws the hull points
     * 
     * @param thePanel the instance of the ResultPanel class on which to draw
     * the points
     */
    public void drawHullPoints(ResultPanel thePanel) {
        vertexInputList_.ClearVertexList();
        vertexHullList_.DrawPoints(thePanel.getDrawableGraphics(), 10, 10);
        cHull_.DrawHull(thePanel.getDrawableGraphics(), 10, 10);
    }
}

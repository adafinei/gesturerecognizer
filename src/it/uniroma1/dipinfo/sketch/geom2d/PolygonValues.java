
package it.uniroma1.dipinfo.sketch.geom2d;

import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * @author Alex Dafinei
 * This class acts as a container for all the hull values,
 * and other calculations useful for features
 */
public class PolygonValues {

    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(PolygonValues.class.getName());
    private ConvexHull2DWrapper convexHull_;
    private ExtremalPolygonsWrapper largestQuad_;
    private ExtremalPolygonsWrapper largestTriangle_;
    private ExtremalPolygonsWrapper enclosingRectangle_;
    private Dimension2D erDimension_;
    private List<Point2D> pointsList_;
    private double perimeter_;


    /**
     * Calculates the special polygons used for the featurs calculations.
     *
     * @param thePoints points of the figure traced by the user
     */
    public PolygonValues(List<Point2D> thePoints) {
        this.pointsList_=thePoints;

        // Calcolo convex hull
        convexHull_ = new ConvexHull2DWrapper();
        convexHull_.setPoints(thePoints);
        LOG_.debug("Starting convex hull calculations...");
        convexHull_.calcConvexHull();
        LOG_.debug("Convex Hull calculations done, starting Polygon calculations...");

        // Calcolo largest quadrilateral
        largestQuad_ = new ExtremalPolygonsWrapper();
        largestQuad_.calcLargestQuadrilateral(convexHull_.getConvexHullPoints());

        // Calcolo largest triangle
        largestTriangle_ = new ExtremalPolygonsWrapper();
        largestTriangle_.calcLargestTriangle(convexHull_.getConvexHullPoints());

        // Calcolo enclosing rectangle
        enclosingRectangle_ = new ExtremalPolygonsWrapper();
        enclosingRectangle_.calcEnclosingRectangle(convexHull_.getConvexHullPoints());
        erDimension_ = enclosingRectangle_.getDimension();

        //calcolo del perimetro
        perimeter_=MiscGeometry.calcPerimeter(thePoints);
        LOG_.debug("Polygon calculations done...");
    }

    /**
     * Getter method
     *
     * @return the convexHull of the figure
     */
    public ConvexHull2DWrapper getConvexHull() {
        return convexHull_;
    }

    /**
     * Getter method
     *
     * @return the largest quadrilateral enclosed in the figure
     */
    public ExtremalPolygonsWrapper getLargestQuad() {
        return largestQuad_;
    }

    /**
     * Getter method
     *
     * @return the largestTriangle enclosed in the firugre
     */
    public ExtremalPolygonsWrapper getLargestTriangle() {
        return largestTriangle_;
    }

    /**
     * Getter method
     *
     * @return the enclosing rectangle of the figure
     */
    public ExtremalPolygonsWrapper getEnclosingRectangle() {
        return enclosingRectangle_;
    }

    /**
     * Getter method
     *
     * @return utility method, it returns the enclosing rectangle dimension
     */
    public Dimension2D getErDimension() {
        return erDimension_;
    }

    /**
     * Getter method
     *
     * @return the perimeter of the figure
     */
    public double getPerimeter() {
        return perimeter_;
    }

    /**
     * Getter method
     *
     * @return the list of points composing the figure
     */
    public List<Point2D> getPointsList() {
        return pointsList_;
    }

}

package it.uniroma1.dipinfo.sketch.geom2d;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import org.apache.log4j.Logger;


/**
 * Panel on which to draw strokes - may be used for debug purposes
 */
public class DrawablePanel extends JPanel {

    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(DrawablePanel.class.getName());
    private final BufferedImage buffOffscreen_;
    private final Graphics2D g2dOffscreen_;
    private final int width_;
    private final int height_;

    /** Creates a new instance of DrawablePanel */
    public DrawablePanel(int theWidth, int theHeight) {
        super();

        buffOffscreen_ = new BufferedImage(theWidth, theHeight, BufferedImage.TYPE_INT_ARGB);
        g2dOffscreen_ = buffOffscreen_.createGraphics();

        width_ = theWidth;
        height_ = theHeight;
    }

    public Dimension getDrawableDimension() {
        return new Dimension(width_, height_);
    }

    public Graphics2D getDrawableGraphics() {
        return g2dOffscreen_;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(buffOffscreen_, 0, 0, this);
    }

    @Override
    protected void finalize() {
        g2dOffscreen_.dispose();
        LOG_.info("Dispose del contesto grafico " + this);
    }
}

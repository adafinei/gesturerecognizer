package it.uniroma1.dipinfo.sketch.geom2d;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Calculates the list of points of the largest enclosed triangle for
 * a figure, given its convex hull points
 */
public class LargestTriangle extends AbstractLargestPolygon {

    /** Logger per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(LargestTriangle.class.getName());

    private LargestTriangle() {
    }

    /**
     * Calculates the list of points of the largest enclosed triangle for
     * a figure, given its convex hull points
     *
     * @param theConvexHullPoints List of Convex Hull points
     *
     * @return list of the largest triangle points
     */
    public static List calcLargestTriangle(List theConvexHullPoints) {

        List retLargestTrianglePoints = null;

        // Controllo caso banale
        if (theConvexHullPoints.size() <= 3) {

            retLargestTrianglePoints = new ArrayList();
            for (int i = 0; i < theConvexHullPoints.size(); i++) {
                Point2D p = (Point2D) theConvexHullPoints.get(i);
                retLargestTrianglePoints.add(p);
            }
            Point2D p0 = (Point2D) theConvexHullPoints.get(0);
            for (int i = theConvexHullPoints.size(); i < 4; i++) {
                retLargestTrianglePoints.add(p0);
            }
        } else {

            // Calcolo di uno tra i rooted triangle

            // Indici dei punti del rooted triangle
            int ripa = 0;
            int ripb = 0;
            int ripc = 0;

            int ia = 0;
            double triArea = 0;

            for (int ib = 1; ib <= theConvexHullPoints.size() - 2; ib++) {
                int ic = (ib >= 2 ? ib + 1 : 2);
                Object[] rootedTriInfo = compRootedTri(theConvexHullPoints, ia, ib, ic);
                double area = ((Double) rootedTriInfo[0]).doubleValue();
                ic = ((Integer) rootedTriInfo[1]).intValue();
                if (area > triArea) {
                    triArea = area;
                    ripa = ia;
                    ripb = ib;
                    ripc = ic;
                }
            } // ripa, ripb and ripc are the indexes of the points of the rooted triangle

            // Indici per i valori finali
            int fipa = 0;
            int fipb = 0;
            int fipc = 0;

            // computes other triangles and choose the largest one
            double finalArea = triArea;

            // indexes of the final points
            int pf0 = ripa;
            int pf1 = ripb;
            int pf2 = ripc;

            for (ia = ripa + 1; ia <= ripb; ia++) {
                triArea = 0;
                int ib0 = (ia == ripb ? ripb + 1 : ripb);
                double area = 0;
                for (int ib = ib0; ib <= ripc; ib++) {
                    int ic = (ib == ripc ? ripc + 1 : ripc);
                    Object[] rootedTriInfo = compRootedTri(theConvexHullPoints, ia, ib, ic);
                    area = ((Double) rootedTriInfo[0]).doubleValue();
                    ic = ((Integer) rootedTriInfo[1]).intValue();
                    if (area > triArea) {
                        triArea = area;
                        fipa = ia;
                        fipb = ib;
                        fipc = ic;
                    }
                }
                if (triArea > finalArea) {
                    finalArea = triArea;
                    pf0 = fipa;
                    pf1 = fipb;
                    pf2 = fipc;
                }
            }

            // Aggiunge i punti alla lista punti ritornata (largest quadrilateral)
            retLargestTrianglePoints = new ArrayList();
            retLargestTrianglePoints.add(theConvexHullPoints.get(pf0));
            retLargestTrianglePoints.add(theConvexHullPoints.get(pf1));
            retLargestTrianglePoints.add(theConvexHullPoints.get(pf2));
            retLargestTrianglePoints.add(theConvexHullPoints.get(pf0));
        }

        return retLargestTrianglePoints;
    }
}

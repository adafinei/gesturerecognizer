package it.uniroma1.dipinfo.sketch.geom2d;

/**
 * Class cPointd  -- point with double coordinates
 */
class cPointd {

    double x_;
    double y_;

    cPointd() {
        x_ = y_ = 0;
    }

    cPointd(int x, int y) {
        x_ = x;
        y_ = y;
    }

    /**
     * Prints point to the console
     */
    public void PrintPoint() {
        System.out.println(" (" + x_ + "," + y_ + ")");
    }
}

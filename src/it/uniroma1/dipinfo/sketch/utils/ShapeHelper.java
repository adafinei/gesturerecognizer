package it.uniroma1.dipinfo.sketch.utils;

import java.awt.Shape;
import java.awt.geom.PathIterator;

import org.apache.log4j.Logger;


public class ShapeHelper {
    
    /** Loggers per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(ShapeHelper.class.getName());


    private ShapeHelper() {
    }


    public static void dumpShapePath(Shape theShape) {

        LOG_.debug("Shape '" + theShape + "' = '" + theShape.getBounds2D() + "'");

        PathIterator pathIte = theShape.getPathIterator(null);
        double[] pathData = new double[6];
        int i = 1;
        while (!pathIte.isDone()) {
            int type = pathIte.currentSegment(pathData);
            LOG_.debug("Path #" + (i++) + ":");
            String strType = null;
            switch (type) {
                case PathIterator.SEG_MOVETO:
                    strType = "SEG_MOVETO";
                    break;
                case PathIterator.SEG_LINETO:
                    strType = "SEG_LINETO";
                    break;
                case PathIterator.SEG_QUADTO:
                    strType = "SEG_QUADTO";
                    break;
                case PathIterator.SEG_CUBICTO:
                    strType = "SEG_CUBICTO";
                    break;
                case PathIterator.SEG_CLOSE:
                    strType = "SEG_CLOSE";
                    break;
                default:
                    strType = "????????";
            }
            LOG_.debug("\tType =" + strType);
            LOG_.debug("\tData =" + pathData[0] + "," + pathData[1] + "," + pathData[2] + "," + pathData[3] + "," + pathData[4] + "," + pathData[5]);
            
            pathIte.next();
        }        
    }

}

package it.uniroma1.dipinfo.sketch.utils;


import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;


/**
 * This class locates files/folders placed outside a JAR
 * It is used to retrieve SVG drawings for beautification purposes
 * and to access the external Derby database; this class is useful in order
 * to allow relative path access from classes packed inside a JAR file to files
 * placed outside the JAR.
 *
 * @author Alex Dafinei
 */
public class ExternalFileLocator {


    /**
     * Retrieves the absolute path of a file/folder given a path relative to the
     * executable JAR file containing the application.
     *
     * @param relativePath the path of a file relative to the JAR file containing
     * the application
     *
     * @return an absolute path on the system under the form of a URI
     */
    public String getAbsolutePath(String relativePath) {
        String url = this.getClass().getResource("/" + this.getClass().getName().replaceAll("\\.", "/") + ".class").toString();
        url = url.substring(4).replaceFirst("/[^/]+\\.jar!.*$", "/");
        url+=relativePath;
        System.out.println("[DEBUG]: URL esterno al progetto:"+url);
        PanelAccessor.getOutputText().append("Accessing external file/folder:"+url+"\n");
        return url;
    }


    /**
     * Retrieves the absolute path of a file/folder given a path relative to the
     * executable JAR file containing the application.
     *
     * @param relativePath the path of a file relative to the JAR file containing
     * the application
     *
     * @return an absolute path on the system under the form of a Windows path
     * e.g. C:\Program Files\
     */
    public String getAbsolutePathWin(String relativePath) {
        String url = this.getClass().getResource("/" + this.getClass().getName().replaceAll("\\.", "/") + ".class").toString();
        url = url.substring(4).replaceFirst("/[^/]+\\.jar!.*$", "/");
        url+=relativePath;

        if(url.startsWith("file:/")){
            url=url.substring(6);
            url=url.replace("/", "\\");
        }
        System.out.println("[DEBUG]: URL esterno al progetto:"+url);
        PanelAccessor.getOutputText().append("Accessing external file/folder:"+url+"\n");
        return url;
    }
}

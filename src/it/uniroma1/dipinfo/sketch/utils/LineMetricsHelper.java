package it.uniroma1.dipinfo.sketch.utils;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Helper class for simple geometry calculations, and constraint
 * verification. Constraint verification is not fully implemented in
 * FcBD.
 */
public class LineMetricsHelper {

    /** Loggers per statements di debug */
    private static final Logger LOG_ = Logger.getLogger(LineMetricsHelper.class.getName());

    /**
     * Creates a new instance of LineMetricsHelper
     */
    private LineMetricsHelper() {
    }

    public static class LinesMetrics {

        /** length of the first line segment */
        public final double line1_len;
        /** length of the second line segment */
        public final double line2_len;

        /**
         * Sets the lines
         * @param theLine1Len first line
         * @param theLine2Len second line
         */
        LinesMetrics(double theLine1Len, double theLine2Len) {
            line1_len = theLine1Len;
            line2_len = theLine2Len;
        }
    }

    /**
     * Returns true if the lines are concident
     * @param theFirstLine
     * @param theSecondLine
     * @return true if the lines are concident
     */
    public static Point2D isCoincident(Line2D theFirstLine, Line2D theSecondLine) {
        Point2D line1Start = theFirstLine.getP1();
        Point2D line1End = theFirstLine.getP2();
        Point2D line2Start = theSecondLine.getP1();
        Point2D line2End = theSecondLine.getP2();
        return (line1Start.equals(line2Start) || line1Start.equals(line2End)
                ? line1Start
                : (line1End.equals(line2Start) || line1End.equals(line2End) ? line1End : null));
    }

    /**
     * Returns true if a line is concident with an Ellipse
     * 
     * @param theLine
     * @param theEllipse
     *
     * @return true if a line is concident with an Ellipse
     */
    public static Point2D isCoincident(Line2D theLine, Ellipse2D theEllipse) {
        Point2D lineStart = theLine.getP1();
        Point2D lineEnd = theLine.getP2();
        Rectangle2D pointStart = new Rectangle2D.Double(
                lineStart.getX() - 1, lineStart.getY() - 1, 3, 3);
        Rectangle2D pointEnd = new Rectangle2D.Double(
                lineEnd.getX() - 1, lineEnd.getY() - 1, 3, 3);
        return (theEllipse.intersects(pointStart) && !theEllipse.contains(lineStart)
                ? lineStart
                : (theEllipse.intersects(pointEnd) && !theEllipse.contains(lineEnd) ? lineEnd : null));
    }

    /**
     * Returns the length of two line segments in input
     * @param theFirstPoints  points of the first line
     * @param theSecondPoints points of the second line
     *
     * @return instance of LineMetrics containing the lengths
     */
    public static LinesMetrics calcLinesMetric(List theFirstPoints, List theSecondPoints) {

        LinesMetrics retMetrics = null;

        if (LOG_.isDebugEnabled()) {
            LOG_.debug("\tfirst=" + theFirstPoints);
            LOG_.debug("\tsecond=" + theSecondPoints);
        }

        if (theFirstPoints.size() == 2 && theSecondPoints.size() == 2) {

            // Calcola la lunghezza delle due linee (arrotondando alla 5a cifra decimale)
            Point2D firstStart = (Point2D) theFirstPoints.get(0);
            double firstLength = Math.round(
                    firstStart.distance((Point2D) theFirstPoints.get(1)) * 10000.0) / 10000.0;
            Point2D secondStart = (Point2D) theSecondPoints.get(0);
            double secondLength = Math.round(
                    secondStart.distance((Point2D) theSecondPoints.get(1)) * 10000.0) / 10000.0;

            if (LOG_.isDebugEnabled()) {
                LOG_.debug("\tfirst len = " + firstLength);
                LOG_.debug("\tsecond len = " + secondLength);
            }

            retMetrics = new LinesMetrics(firstLength, secondLength);
        }

        return retMetrics;
    }

    /**
     * Calculates distance between two lines
     *
     * @param theFirstPoints points of the first line
     * @param theSecondPoints points of the second line
     * @param theBoundingHeight threshold for distance
     * @param theBoundingWidth threshold for distance
     * @return true or false depending if lines are near each one
     */
    public static boolean calcLinesDistance(List theFirstPoints, List theSecondPoints,
            double theBoundingHeight, double theBoundingWidth) {

        boolean retAreLinesNear = false;

        if (LOG_.isDebugEnabled()) {
            LOG_.debug("\tfirst=" + theFirstPoints);
            LOG_.debug("\tsecond=" + theSecondPoints);
        }

        if (theFirstPoints.size() == 2 && theSecondPoints.size() == 2) {

            // Calcola la distanza delle due linee
            Point2D firstStart = (Point2D) theFirstPoints.get(0);
            Point2D firstEnd = (Point2D) theFirstPoints.get(1);
            Point2D secondStart = (Point2D) theSecondPoints.get(0);
            Point2D secondEnd = (Point2D) theSecondPoints.get(1);

            // ***** Intorno rettangolare di altezza indicata ******************
            Point2D[] perpPstart = calcPerpendicularSegment(theFirstPoints, firstStart, theBoundingHeight, theBoundingWidth);
            Point2D[] perpPend = calcPerpendicularSegment(theFirstPoints, firstEnd, theBoundingHeight, theBoundingWidth);

            // Shape rappresentante l'intorno rettangolare individuato dai 4 punti
            GeneralPath gpFirstBound = new GeneralPath();
            Line2D[] boundLines = new Line2D[4];
            boundLines[0] = new Line2D.Double(perpPstart[0], perpPstart[1]);
            boundLines[1] = new Line2D.Double(perpPstart[1], perpPend[1]);
            boundLines[2] = new Line2D.Double(perpPend[1], perpPend[0]);
            boundLines[3] = new Line2D.Double(perpPend[0], perpPstart[0]);
            gpFirstBound.append(boundLines[0], true);
            gpFirstBound.append(boundLines[1], true);
            gpFirstBound.append(boundLines[2], true);
            gpFirstBound.append(boundLines[3], true);
            //gpFirstBound.closePath();
            Shape firstBound = gpFirstBound.createTransformedShape(null);

            // Assert area ottenuta rispetto a quella attesa (base * altezza rettangolo)
            double obtainedArea = perpPend[0].distance(perpPstart[0]) * perpPend[0].distance(perpPend[1]);
            double expectedArea = (theBoundingWidth + firstEnd.distance(firstStart)) * theBoundingHeight;
            LOG_.debug("first bounding box = " + firstBound);
            ShapeHelper.dumpShapePath(firstBound);
            LOG_.debug("obtainedArea = " + obtainedArea);
            LOG_.debug("expectedArea = " + expectedArea);
            if (Math.round(obtainedArea * 1000) != Math.round(expectedArea * 1000)) {
                throw new RuntimeException("Assert su area calcolata [" + obtainedArea + "," + expectedArea + "]");
            }

            // Controlla se c'e' contenimento della seconda linea nell'intorno rettangolare
            // (quindi distanza inferiore della tolleranza in input)
            LOG_.debug("first line bounding box contains second line start ? " + firstBound.contains(secondStart));
            LOG_.debug("first line bounding box contains second line end   ? " + firstBound.contains(secondEnd));
            retAreLinesNear = firstBound.contains(secondStart) || firstBound.contains(secondEnd);

            // ...altrimenti controlla se la seconda linea interseca l'intorno rettangolare
            if (!retAreLinesNear) {
                boolean check = false;
                Line2D secondLine = new Line2D.Double(secondStart, secondEnd);
                for (int i = 0; i < boundLines.length; i++) {
                    LOG_.debug("line bounding box [" + boundLines[i].getBounds2D() + "] intersects second line ? " + boundLines[i].intersectsLine(secondLine));
                    check = boundLines[i].intersectsLine(secondLine);
                    if (check) {
                        retAreLinesNear = true;
                        break;
                    }
                }
            }
        }

        return retAreLinesNear;
    }

    /**
     * Calculates the distance between an ellipse and a line
     *
     * @param theFirstPoints
     * @param theSecondPoints
     * @param theDistance
     * @return true if the distance is small
     */

    public static boolean calcEllipseLineDistance(List theFirstPoints, List theSecondPoints,
            double theDistance) {

        boolean retIsLineNear = false;

        // Recupera centro e raggio del cerchio
        Point2D firstCenter = (Point2D) theFirstPoints.get(0);
        double firstRadius = ((Double) theFirstPoints.get(1)).doubleValue();

        // Recupera gli estremi del segmento
        Point2D secondStart = (Point2D) theSecondPoints.get(0);
        Point2D secondEnd = (Point2D) theSecondPoints.get(1);

        // Il segmento e' vicino al cerchio se almeno uno dei suoi estremi
        // e' a distanza inferiore della soglia data (appartiene all'intorno
        // circolare di raggio dato)
        retIsLineNear =
                firstCenter.distance(secondStart) <= theDistance
                || firstCenter.distance(secondEnd) <= theDistance;

        return retIsLineNear;
    }

    /**
     * Checks if two shapes are near by expanding the first shape's bounding
     * box and see if the bounding boxes intersect
     *
     * @param theFirst      first shape
     * @param theSecond     second shape
     * @param theDistance   threshold for first box expansion
     *
     * @return <CODE>true</CODE> if the bounding boxes intersect
     * 
     */
    public static boolean calcShapesDistance(Shape theFirst, Shape theSecond,
            double theDistance) {

        boolean retAreShapesNear = false;

        // Determina i bbox delle due shapes
        Rectangle2D firstBbox = theFirst.getBounds2D();
        Rectangle2D secondBbox = theSecond.getBounds2D();
        Rectangle2D firstNearBox = getNearBox(firstBbox, theDistance);
        Rectangle2D secondNearBox = getNearBox(secondBbox, theDistance);

        retAreShapesNear =
                firstNearBox.intersects(secondNearBox)
                && !firstNearBox.contains(secondNearBox)
                && !secondNearBox.contains(firstNearBox);

        if (LOG_.isDebugEnabled()) {
            LOG_.debug("first  shape bounding box = " + firstBbox);
            LOG_.debug("first  shape near     box = " + firstNearBox);
            LOG_.debug("second shape bounding box = " + secondBbox);
            LOG_.debug("second shape near     box = " + secondNearBox);
            LOG_.debug("intersects second shape ? " + retAreShapesNear);
        }

        return retAreShapesNear;
    }

    private static Rectangle2D getNearBox(Rectangle2D theRect, double theDistance) {
        return new Rectangle2D.Double(
                theRect.getX() - theDistance / 2, theRect.getY() - theDistance / 2,
                theRect.getWidth() + theDistance, theRect.getHeight() + theDistance);
    }

    /**
     * Calculates the intersection point of two line segments
     */
    public static Point2D calcLineLineIntersection(List theFirstPoints, List theSecondPoints) {

        Point2D retPoint = null;

        Point2D firstStart = (Point2D) theFirstPoints.get(0);
        Point2D firstEnd = (Point2D) theFirstPoints.get(1);
        Point2D secondStart = (Point2D) theSecondPoints.get(0);
        Point2D secondEnd = (Point2D) theSecondPoints.get(1);
        if (LOG_.isDebugEnabled()) {
            LOG_.debug("\tfirst line  =" + firstStart + "," + firstEnd);
            LOG_.debug("\tsecond line =" + secondStart + "," + secondEnd);
        }

        double bx = firstEnd.getX() - firstStart.getX();
        double by = firstEnd.getY() - firstStart.getY();
        double dx = secondEnd.getX() - secondStart.getX();
        double dy = secondEnd.getY() - secondStart.getY();

        double b_dot_d_perp = bx * dy - by * dx;

        if (b_dot_d_perp != 0) {

            double cx = secondStart.getX() - firstStart.getX();
            double cy = secondStart.getY() - firstStart.getY();

            double t = (cx * dy - cy * dx) / b_dot_d_perp;

            retPoint = new Point2D.Double(
                    firstStart.getX() + t * bx, firstStart.getY() + t * by);

        } else {
            LOG_.warn("PARALLEL");
        }

        return retPoint;
    }

    /**
     * Calculates the intersection point between two lines
     */
    public static Point2D calcLineLineCommonIntersection(List theFirstPoints, List theSecondPoints, double[] theResults) {

        Point2D retP = calcLineLineIntersection(theFirstPoints, theSecondPoints);
        if (retP != null) {
            // Determina se il punto di intersezione e' un punto delle due linee (ad es. la prima)
            Line2D line1 = new Line2D.Double((Point2D) theFirstPoints.get(0), (Point2D) theFirstPoints.get(1));
            Line2D line2 = new Line2D.Double((Point2D) theSecondPoints.get(0), (Point2D) theSecondPoints.get(1));

            // Se le linee si intersecano oppure uno degli estremi di una giace sull'altra
            if (theResults != null
                    && (line1.intersectsLine(line2)
                    || Math.round(line1.ptSegDist(line2.getP1())) == 0
                    || Math.round(line1.ptSegDist(line2.getP2())) == 0
                    || Math.round(line2.ptSegDist(line1.getP1())) == 0
                    || Math.round(line2.ptSegDist(line1.getP2())) == 0)) {

                LOG_.debug("intersect p = " + retP);
                Point2D line1Start = line1.getP1();
                Point2D line2Start = line2.getP1();

                // Calcola le percentuali di intersezione relativamente alle due linee
                theResults[0] = (line1Start.distance(retP) / line1Start.distance(line1.getP2())) * 100;
                theResults[1] = (line2Start.distance(retP) / line2Start.distance(line2.getP2())) * 100;

                // Corregge eventuali errori di arrotondamento su punti 'al limite'
                if (theResults[0] > 100) {
                    theResults[0] = 100;
                }
                if (theResults[1] > 100) {
                    theResults[1] = 100;
                }
                LOG_.debug("line 1 [" + line1.toString() + "]: dist = " + line1Start.distance(retP) + " (" + theResults[0] + "%)");
                LOG_.debug("line 2 [" + line2.toString() + "]: dist = " + line2Start.distance(retP) + " (" + theResults[1] + "%)");
            }
        }

        return retP;
    }

    /**
     * Calculates the angle between two line segments in rad. Convenience method
     * it calls:
     * <CODE>calcLinesRadAngle(List, List, Point2D)</CODE> for calculation
     *
     * @param theFirstLine points of the first line
     * @param theSecondLine points of the second line
     *
     * @return angle in rad between the lines
     */
    public static double calcLinesRadAngle(Line2D theFirstLine, Line2D theSecondLine, Point2D theCommonPoint) {
        return calcLinesRadAngle(
                theFirstLine.getP1(), theFirstLine.getP2(),
                theSecondLine.getP1(), theSecondLine.getP2(),
                theCommonPoint);
    }

    public static double calcLinesRadAngle(Point2D theFirstLineP1, Point2D theFirstLineP2,
            Point2D theSecondLineP1, Point2D theSecondLineP2, Point2D theCommonPoint) {

        List firstPoints = new ArrayList();
        firstPoints.add(theFirstLineP1);
        firstPoints.add(theFirstLineP2);
        List secondPoints = new ArrayList();
        secondPoints.add(theSecondLineP1);
        secondPoints.add(theSecondLineP2);
        return calcLinesRadAngle(firstPoints, secondPoints, theCommonPoint);
    }

    /**
     * Calculates the internal angle in rads between two line segments
     *
     * @param theFirstPoints the points of the line
     * @param theSecondPoints the points of the second line
     *
     * @return angle in rad between the line segments
     */
    public static double calcLinesRadAngle(List theFirstPoints, List theSecondPoints, Point2D theCommonPoint) {

        double retRadAngle = 0;
        // Angolo linea iesima con l'asse delle ascisse
        double radAngle1 = calcAngle(theFirstPoints, theCommonPoint);
        double radAngle2 = calcAngle(theSecondPoints, theCommonPoint);
        LOG_.debug("radAngle1 = " + radAngle1);
        LOG_.debug("radAngle2 = " + radAngle2);

        // differenza tra i due angoli ===> uno dei due possibili angoli tra le linee
        retRadAngle = Math.abs(radAngle1 - radAngle2);
        LOG_.debug("angle (rad) = " + retRadAngle);

        double degAngle = Math.toDegrees(retRadAngle);
        LOG_.debug("angle (deg) = " + degAngle);
        return retRadAngle;
    }

    /**
     * Calculates the slope of line segment
     *
     * @param thePoints points of the line segment
     *
     * @return slope of the segment
     */
    private static double calcSlope(List thePoints) {
        Point2D start = (Point2D) thePoints.get(0);
        Point2D end = (Point2D) thePoints.get(1);
        double slope = (end.getY() - start.getY()) / (end.getX() - start.getX());
        LOG_.debug("Line (" + start + "," + end + "), Slope = " + slope);
        return slope;
    }

    /**
     * Calculates the angle between a line and the X axis
     *
     * @param thePoints points identifying the line
     * @param theOrigin origin
     *
     * @return angolo tra segmento e ascissa
     */
    private static double calcAngle(List thePoints, Point2D theOrigin) {
        double retRadAngle = 0;

        Point2D first = (Point2D) thePoints.get(0);
        Point2D second = (Point2D) thePoints.get(1);

        // Se origine e' null ==> prende come origine un punto qualsiasi
        if (theOrigin == null) {
            theOrigin = first;
        }

        // Sceglie tra i due il punto finale in base all'origine
        Point2D endPoint = (theOrigin.equals(first) ? second : first);
        LOG_.debug("Uso (origine, finale) = (" + theOrigin + "," + endPoint + ")");

        double y = theOrigin.getY() - endPoint.getY();
        double x = endPoint.getX() - theOrigin.getX();
        retRadAngle = Math.atan2(y, x);
        if (retRadAngle < 0) {
            retRadAngle += 2 * Math.PI;
        }

        double degAngle = Math.toDegrees(retRadAngle);
        LOG_.debug("Line (" + first + "," + second + "), angle = " + retRadAngle + " (" + degAngle + " deg)");
        return retRadAngle;
    }

    /**
     * Calculates two points of a segment perpendicular to the input segment.
     * The segment whic unites the two points passes through a point which is
     * at a distance equal to the offset theRefPointOffs
     * The returned points are at opposite sides from the input segment
     *
     * @param theLinePoints list of points of the input line segment
     * @param theRefPoint   reference point (one of the extremals of the segment)
     * @param theSegmentLength   length of the new segment
     * @param theRefPointOffs    distance from the reference point
     *
     * @return two points representing the calculated segment.
     */
    private static Point2D[] calcPerpendicularSegment(List theLinePoints,
            Point2D theRefPoint,
            double theSegmentLength, double theRefPointOffs) {

        Point2D[] retPoints = new Point2D.Double[2];

        double lineRadAngle = calcAngle(theLinePoints, theRefPoint);

        Point2D refPointOffsetted = new Point2D.Double(
                theRefPoint.getX() - (theRefPointOffs / 2) * Math.cos(lineRadAngle),
                theRefPoint.getY() + (theRefPointOffs / 2) * Math.sin(lineRadAngle));
        if (LOG_.isDebugEnabled()) {
            LOG_.debug("ref point = " + theRefPoint);
            LOG_.debug("ref point offsetted = " + refPointOffsetted);
        }
        theRefPoint = refPointOffsetted;

        Point2D p1 = null;
        Point2D p2 = null;

        // Casi particolari:
        // Angolo 0 ovvero segmento in input orizzontale
        if (Math.round(lineRadAngle * 1000) == 0) {
            LOG_.info("Linea orizzontale");
            p1 = new Point2D.Double(
                    theRefPoint.getX(),
                    theRefPoint.getY() - (theSegmentLength / 2));
            p2 = new Point2D.Double(
                    theRefPoint.getX(),
                    theRefPoint.getY() + (theSegmentLength / 2));
        } else {
            p1 = new Point2D.Double(
                    theRefPoint.getX() + (theSegmentLength / 2) * Math.cos(lineRadAngle + Math.PI / 2),
                    theRefPoint.getY() - (theSegmentLength / 2) * Math.sin(lineRadAngle + Math.PI / 2));
            p2 = new Point2D.Double(
                    theRefPoint.getX() + (theSegmentLength / 2) * Math.cos(lineRadAngle - Math.PI / 2),
                    theRefPoint.getY() - (theSegmentLength / 2) * Math.sin(lineRadAngle - Math.PI / 2));
        }

        // Approssima (arrotonda alla 10a cifra decimale) le coordinate dei due punti trovati
        p1 = new Point2D.Double(
                Math.round(p1.getX() * 1000000000.0) / 1000000000.0, Math.round(p1.getY() * 1000000000.0) / 1000000000.0);
        p2 = new Point2D.Double(
                Math.round(p2.getX() * 1000000000.0) / 1000000000.0, Math.round(p2.getY() * 1000000000.0) / 1000000000.0);

        // Ordina i punti p1, p2 in base all'ascissa o, se coincide, in base all'ordinata
        // Primo punto: quello piu' a sinistra o piu' in alto
        retPoints[0] = (p1.getX() < theRefPoint.getX()
                ? p1
                : (p1.getX() > theRefPoint.getX()
                ? p2
                : (p1.getY() < theRefPoint.getY() ? p1 : p2)));
        // Secondo punto: quello piu' a destra o piu' in basso
        retPoints[1] = (p1.getX() > theRefPoint.getX()
                ? p1
                : (p1.getX() < theRefPoint.getX()
                ? p2
                : (p1.getY() > theRefPoint.getY() ? p1 : p2)));

        // Debug
        double distRetPoint1 = retPoints[0].distance(theRefPoint);
        double distRetPoint2 = retPoints[1].distance(theRefPoint);

        LOG_.debug("retPoint1 = " + retPoints[0] + " pRef.distance=" + distRetPoint1);
        LOG_.debug("retPoint2 = " + retPoints[1] + " pRef.distance=" + distRetPoint2);

        // Assert sui punti trovati: devono essere alla distanza richiesta (con arrotondamento)
        if (Math.round(distRetPoint1 * 100000) != theSegmentLength * 100000 / 2 || Math.round(distRetPoint2 * 100000) != theSegmentLength * 100000 / 2) {
            throw new RuntimeException("Errore: assert distanza punto calcolato errata! ["
                    + (Math.round(distRetPoint1 * 100000) != theSegmentLength * 100000 / 2 ? "pSol1" : "pSol2") + "]");
        }

        return retPoints;
    }
}

package it.uniroma1.dipinfo.sketch.gui;

import it.uniroma1.dipinfo.sketch.gui.strokeproducer.StrokeProducer;
import it.uniroma1.dipinfo.sketch.recognizer.RecognizerTimerTask;
import it.uniroma1.dipinfo.sketch.structures.Stroke;
import it.uniroma1.dipinfo.sketch.structures.TimeInterval;
import it.uniroma1.dipinfo.sketch.config.Config;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.geom.Point2D;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JPanel;


/**
 * The class underlying the panel used by the user to trace a symbol
 * 
 * @author Alex Dafinei
 */
public class DrawPanel extends JPanel
        implements MouseListener, MouseMotionListener, StrokeProducer {

    /**Default dimension of the panel */
    private final static int IMAGE_HEIGHT = 500;
    /**Default dimension of the panel */
    private final static int IMAGE_WIDTH = 500;
    /**Time out value */
    private final static int TASK_TIMEOUT=Config.getInstance().getTASK_TIMEOUT();
    /**Threshold for minimum number of points in a stroke*/
    private final static int MIN_POINTS_PER_STROKE=Config.getInstance().getMIN_POINTS_PER_STROKE();
    /**Image for the graphics on the DrawPanel */
    private BufferedImage bufferedImage_ = new BufferedImage(IMAGE_WIDTH,
            IMAGE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
    /**Graphics on the DrawPanel */
    private Graphics2D g2d_ = bufferedImage_.createGraphics();
    /**Actual coordinates of the point */
    private int xNew_, yNew_;
    /**Previous coordinates of the point */
    private int xOld_, yOld_;
    /** Last stroke drawn by the user */
    private Stroke currStroke_;
    /** First point of the stroke the user is currently drawing */
    private Point2D firstPoint_;
    /** Last point of the stroke the user is currently drawing */
    private Point2D lastPoint_;
    /** List of points which are currently drawn by the user */
    private List points_ = new ArrayList();
    /** Timing information for the drawing session */
    private long sessionStartMillis_ = System.currentTimeMillis();
    /** Time of the first point drawing */
    private long startMillis_;
    /** Time of the last point drawing */
    private long endMillis_;
    /**List of strokes */
    private List<Stroke> strokeList_ = new LinkedList<Stroke>();
    /** Timer for starting recognition in case of multistroke symbols  */
    private Timer timer = null;
    TimerTask task = null;

    /**
     * Constructor
     */
    public DrawPanel() {
        setBackground(Color.WHITE);          // Settaggio Sfondo.   
        addMouseMotionListener(this);        // Pone in ascolto un "thread di movimento del Mouse" sul JPanel.
        addMouseListener(this);              // Pone in ascolto un "thread di azione del Mouse" sul JPanel.
        g2d_.setColor(Color.BLUE); // Consente la "Colorazione BLUE" al passaggio del Mouse il tasto premuto.
    }

    /**
     * Event listener (not used)
     *
     * @param e
     */
    public void mouseClicked(MouseEvent e) { /* No Actions */ }

    /**
     * Event listener (not used)
     *
     * @param e
     */
    public void mouseEntered(MouseEvent e) { /* No Actions */ }

    /**
     * Event listener (not used)
     *
     * @param e
     */
    public void mouseExited(MouseEvent e) { /* No Actions */ }

    /**
     * Event handler for the mousePressed event
     * 
     * @param e the mousePressed event
     */
    public void mousePressed(MouseEvent e) {

        if (e.getButton() != MouseEvent.BUTTON1) {
            return;
        }

        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        task = new RecognizerTimerTask(this);

        xNew_ = e.getX();
        yNew_ = e.getY();
        xOld_ = xNew_;
        yOld_ = yNew_;

        // New stroke starting - considering only its first point
        points_.clear();

        // First pixel and timing information
        firstPoint_ = new Point2D.Float(xOld_, yOld_);
        startMillis_ = System.currentTimeMillis() - sessionStartMillis_;
        repaint();
    }

    /**
     * Event handler for the mouseReleased event
     *
     * @param e the mouseReleased event
     */
    public void mouseReleased(MouseEvent e) {


        timer.schedule(task, TASK_TIMEOUT);

        xNew_ = e.getX();
        yNew_ = e.getY();
        xOld_ = xNew_;
        yOld_ = yNew_;

        // Memorizza l'ultimo pixel e le sue informazioni sul tempo
        lastPoint_ = new Point2D.Float(xOld_, yOld_);
        endMillis_ = System.currentTimeMillis() - sessionStartMillis_;

        // Crea un nuovo stroke, corrispondente a quanto disegnato
        currStroke_ = new Stroke(firstPoint_, lastPoint_, points_,
                new TimeInterval(startMillis_, endMillis_));

        //aggiorna la lista di strokes
        if (currStroke_.getPointsList().size() > MIN_POINTS_PER_STROKE) {
            strokeList_.add(currStroke_);
        } else {
            currStroke_ = null;
        }
    }

    /**
     * Defines actions for the mouse dragged event; this is the moment
     * when the user traces a symbol
     *
     * @param e the mouseDragged event
     */
    public void mouseDragged(MouseEvent e) {

        xNew_ = e.getX();
        yNew_ = e.getY();

        // Aggiunge alla lista di punti il nuovo punto su cui e' andato il mouse
        points_.add(new Point2D.Double(xNew_, yNew_));
        repaint();
    }

    /**
     * mouseMoved event handler - nothing happens
     * 
     * @param e
     */
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void paint(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;
        g2.setBackground(Color.WHITE);
        getSketchGraphics().drawLine(xOld_, yOld_, xNew_, yNew_);
        xOld_ = xNew_;
        yOld_ = yNew_;

        g2.drawImage(bufferedImage_, 0, 0, Color.WHITE, this);

    }

    /**
     * Cleans the DrawPanel
     */
    public void cleanScreen() {
        // Pulisce il pannello
        Graphics2D g = bufferedImage_.createGraphics();
        g.setBackground(Color.WHITE);
        g.clearRect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
        g.dispose();
        xOld_ = yOld_ = xNew_ = yNew_ = Integer.MAX_VALUE;
    }

    /**
     * Getter method
     *
     * @return the current stroke
     */
    public Stroke getCurrentStroke() {
        return currStroke_;
    }

    /**
     * Getter method
     *
     * @return the sketchGraphics
     */
    public Graphics2D getSketchGraphics() {
        return g2d_;
    }

    /**
     * Getter method
     *
     * @return the stroke list
     */
    public List<Stroke> getStrokeList_() {
        return strokeList_;
    }

    /**
     * Setter method
     *
     * @param strokeList_ the list of strokes to set
     */
    public void setStrokeList_(List<Stroke> strokeList_) {
        this.strokeList_ = strokeList_;
    }

    /**
     * Getter method Returns the list of strokes
     *
     * @return the list of strokes traced by the user
     */
    public List<Stroke> getStrokes() {
        return this.strokeList_;
    }

    /**
     * Clears and reinitialises the strokes acquired.
     */
    public void clearStrokes() {
        this.strokeList_ = null;
        this.strokeList_ = new LinkedList<Stroke>();
    }
}

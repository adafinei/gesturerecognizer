/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma1.dipinfo.sketch.gui;

import it.uniroma1.dipinfo.sketch.structures.Stroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 * This class holds the logic behind the feddback panel for displaying results
 * or feedback to the user.
 *
 * @author Alex Dafinei
 */
public class ResultPanel extends JPanel {

    /** Logger per statements di debug */
    //private static final Logger LOG_ = Logger.getLogger(DrawablePanel.class.getName());
    
    /**Dimension of the drawing area */
    private final static int IMAGE_WIDTH = 500;
    private final static int IMAGE_HEIGHT = 500;

    private BufferedImage bufferedImage_=new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
    
    /** Constructor */
    public ResultPanel() {
        setBackground(Color.white); // Imposta "Bianco" il colore dello Sfondo del JPanel.
        
    }

    @Override
    public void paint( Graphics g ) {
       Graphics2D g2 = (Graphics2D) g;
       g2.setBackground(Color.WHITE);
       g2.drawImage(bufferedImage_, 0, 0, Color.WHITE, this);
    }

    /** 
     * Returns the graphic context to draw on
     * 
     * @return Graphics2D g2d the graphic context to draw on
     */
    public Graphics2D getDrawableGraphics() {
       Graphics2D g2d=bufferedImage_.createGraphics();
       return g2d;
    }

    /**
     * Draws a stroke on this panel
     *
     * @param theStroke the stroke to draw
     * @param theStrokeColor the color used for drawing
     */
    public void drawStroke(Stroke theStroke, Color theStrokeColor) {
        Graphics2D g = bufferedImage_.createGraphics();
        g.setColor(theStrokeColor);
        theStroke.draw(g);
        g.dispose();
        repaint();
    }

    /**
     * Draws freely points on this panel, connecting them by lines
     *
     * @param  a first point of line
     * @param  b second point of line
     * @param  pointColor color of the drawing
     */
    public void drawFreePoints(Point2D a, Point2D b, Color pointColor) {
        Graphics2D g = bufferedImage_.createGraphics();
        g.setColor(pointColor);
        g.drawLine((int)a.getX(),(int)a.getY(),(int)b.getX(),(int)b.getY());
        g.dispose();
        repaint();
    }


    /**
     * Writes/draws a message over the panel
     *
     * @param str the message
     * @param color color of the message
     * @param x x position of the message
     * @param y y position of the message
     */
    public void drawMessage(String str, Color color, int x, int y) {
        System.out.println("str = " + str);
        Graphics2D g = bufferedImage_.createGraphics();
        g.setColor(color);
        g.drawString(str ,x ,y);
        g.dispose();
        repaint();
    }

    /**
     * Cleans the panel
     */
    public void cleanScreen() {
        // Pulisce il pannello
        Graphics2D g = bufferedImage_.createGraphics();
        g.setBackground(Color.WHITE);
        g.clearRect(0, 0, IMAGE_WIDTH , IMAGE_HEIGHT);
        g.dispose();
    }
}

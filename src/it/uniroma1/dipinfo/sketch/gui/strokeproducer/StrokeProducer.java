package it.uniroma1.dipinfo.sketch.gui.strokeproducer;

import it.uniroma1.dipinfo.sketch.structures.Stroke;
import java.util.List;


/**
 * Interface implemented by the DrawPanel and the ImageProcessor
 */
public interface StrokeProducer {

    /*
     * Gets the list of strokes
     */
    public List<Stroke> getStrokes();

    /*
     * clears the list of strokes
     */
    public void clearStrokes();
   

}

package it.uniroma1.dipinfo.sketch.gui;

import it.uniroma1.dipinfo.sketch.tracking.JImagePanel;
import it.uniroma1.dipinfo.sketch.tracking.JSelectionImagePanel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * This is a utility class which holds references to all the panels in the GUI
 *
 * @author Alex Dafinei
 */
public class PanelAccessor {

    private static DrawPanel drawPanel_;
    private static ResultPanel resultPanel_;
    private static JTextArea outputText_;
    private static SVGPanel svgPanel_;
    private static JSelectionImagePanel liveImage_;
    private static JImagePanel processedImage_;

    

    /**
     * Creates a new Instance of PanelAccessor
     *
     * @param dp the draw panel an instance of DrawPanel
     * @param rp the result/debug panel an instance of ResultPanel
     * @param svg the svg panel an instance of SVGPanel
     * @param jta the debug panel an instance of JTextArea
     */
     
    PanelAccessor(JPanel dp, JPanel rp, SVGPanel svg, JTextArea jta) {
        drawPanel_ = (DrawPanel) dp;
        resultPanel_ = (ResultPanel) rp;
        outputText_ = jta;
        svgPanel_ = (SVGPanel) svg;
    }

    /**
     * Creates a new Instance of PanelAccessor with video panels
     *
     * @param dp the draw panel an instance of DrawPanel
     * @param rp the result/debug panel an instance of ResultPanel
     * @param svg the svg panel an instance of SVGPanel
     * @param jta the debug panel an instance of JTextArea
     * @param liveImage the panel containing the live image, an instance of JSelectionImagePanel
     * @param processedImage an instance of ImagePanel
     */

    PanelAccessor(JPanel dp, JPanel rp, SVGPanel svg, JTextArea jta, JPanel liveImage, JPanel processedImage) {
        drawPanel_ = (DrawPanel) dp;
        resultPanel_ = (ResultPanel) rp;
        outputText_ = jta;
        svgPanel_ = (SVGPanel) svg;
        liveImage_ = (JSelectionImagePanel) liveImage;
        processedImage_ = (JImagePanel) processedImage;
    }

    /**
     * Accessor method
     *
     * @return drawPanel, a reference to the panel used to trace symbols
     */
    public static DrawPanel getDrawPanel() {
        return drawPanel_;
    }

    /**
     * Accessor method
     *
     * @return resultPanel, a reference to a feedback panel used to display filter
     * results, feedback on video mode tracing, special polygons (if enabled.)
     */
    public static ResultPanel getResultPanel() {
        return resultPanel_;
    }

    /**
     * Accessor method
     *
     * @return a JTextArea object, used for displaying on screen various informations
     */
    public static JTextArea getOutputText() {
        return outputText_;
    }

    /**
     * Accessor method
     *
     * @return the reference to the SVG panel for displaying recognized symbols
     */
    public static SVGPanel getSvgPanel() {
        return svgPanel_;
    }

    /**
     * Accessor method
     *
     * @return the live image, frames captured by the webcam
     */
    public static JSelectionImagePanel getLiveImage() {
        return liveImage_;
    }

    /**
     * Accessor method
     *
     * @param aLiveImage_ the live image captured by the webcam to set
     */
    public static void setLiveImage(JPanel aLiveImage_) {
        liveImage_ = (JSelectionImagePanel)aLiveImage_;
    }

    /**
     * Accessor method
     *
     * @return the processed Image
     */
    public static JImagePanel getProcessedImage() {
        return processedImage_;
    }

    /**
     * Accessor method
     *
     * @param aProcessedImage_ the processed Image to set
     */
    public static void setProcessedImage(JPanel aProcessedImage_) {
        processedImage_ = (JImagePanel)aProcessedImage_;
    }

    /**
     * Accessor method
     *
     * @param aDrawPanel_ the panel on which the user draws to set
     */
    public static void setDrawPanel(JPanel aDrawPanel_) {
        drawPanel_ = (DrawPanel)aDrawPanel_;
    }

    /**
     * Accessor method
     *
     * @param aResultPanel_ the result Panel for displaying various results to set
     */
    public static void setResultPanel(JPanel aResultPanel_) {
        resultPanel_ = (ResultPanel)aResultPanel_;
    }

    /**
     * Accessor method
     *
     * @param aOutputText_ the console for displaying information to set
     */
    public static void setOutputText(JTextArea aOutputText_) {
        outputText_ = aOutputText_;
    }

    /**
     * Accessor method
     *
     * @param aSvgPanel_ a panel to display SVG results of the
     * recognized symbol to set
     */
    public static void setSvgPanel(SVGPanel aSvgPanel_) {
        svgPanel_ = aSvgPanel_;
    }
}

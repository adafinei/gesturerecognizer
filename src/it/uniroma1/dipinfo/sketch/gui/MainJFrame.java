/*
 * MainJFrame.java
 *
 * Created on 6-ott-2010, 16.33.02
 */
package it.uniroma1.dipinfo.sketch.gui;

import it.uniroma1.dipinfo.sketch.tracking.CameraController;
import it.uniroma1.dipinfo.sketch.tracking.ImageProcessor;
import it.uniroma1.dipinfo.sketch.tracking.ImageUpdateListener;
import it.uniroma1.dipinfo.sketch.tracking.JImagePanel;
import it.uniroma1.dipinfo.sketch.tracking.JSelectionImagePanel;
import it.uniroma1.dipinfo.sketch.tracking.SelectionEvent;
import it.uniroma1.dipinfo.sketch.tracking.SelectionListener;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * This class is the main GUI JFrame, it holds references to
 * all the other GUI children
 *
 * @author Alex Dafinei
 */
public class MainJFrame extends javax.swing.JFrame implements WindowListener, ChangeListener, SelectionListener, ImageUpdateListener {

    /**The SVG panel for beatified symbols */
    private SVGPanel svgPanel_ = new SVGPanel();
    //Tracker
    private static ImageProcessor imp_ = new ImageProcessor();
    private static CameraController cameraController_;

    /** Creates new form MainJFrame */
    public MainJFrame() {
        initComponents();
        inputTabPane.addChangeListener(changeListenerInputPane);
        videoTabPane.addChangeListener(changeListenerPickPane);


    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        logOutputFrame = new javax.swing.JInternalFrame();
        jScrollPane1 = new javax.swing.JScrollPane();
        outputText = new javax.swing.JTextArea();
        resultTabPane = new javax.swing.JTabbedPane();
        resultPanel = new it.uniroma1.dipinfo.sketch.gui.ResultPanel();
        inputTabPane = new javax.swing.JTabbedPane();
        drawPanel_ = new it.uniroma1.dipinfo.sketch.gui.DrawPanel();
        videoTabPane = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        liveImage = new it.uniroma1.dipinfo.sketch.tracking.JSelectionImagePanel();
        redHiSlider = new javax.swing.JSlider();
        greenHiSlider = new javax.swing.JSlider();
        blueHiSlider = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        processedImage = new it.uniroma1.dipinfo.sketch.tracking.JImagePanel();
        redLoSlider = new javax.swing.JSlider();
        greenLoSlider = new javax.swing.JSlider();
        blueLoSlider = new javax.swing.JSlider();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        optionsMenu = new javax.swing.JMenu();
        vInvert = new javax.swing.JCheckBoxMenuItem();
        mirrorEffect = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("FcBD  - ver 1.0");

        jDesktopPane1.setDesktopManager(new SketchDesktopManager());
        PanelAccessor.setDrawPanel(drawPanel_);
        PanelAccessor.setResultPanel(resultPanel);
        PanelAccessor.setSvgPanel(svgPanel_);
        PanelAccessor.setOutputText(outputText);
        PanelAccessor.setLiveImage(liveImage);
        PanelAccessor.setProcessedImage(processedImage);

        logOutputFrame.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        try {
            logOutputFrame.setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        logOutputFrame.setVisible(true);

        outputText.setColumns(200);
        outputText.setEditable(false);
        outputText.setRows(5);
        jScrollPane1.setViewportView(outputText);
        outputText.getAccessibleContext().setAccessibleParent(logOutputFrame);

        javax.swing.GroupLayout logOutputFrameLayout = new javax.swing.GroupLayout(logOutputFrame.getContentPane());
        logOutputFrame.getContentPane().setLayout(logOutputFrameLayout);
        logOutputFrameLayout.setHorizontalGroup(
            logOutputFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(logOutputFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1008, Short.MAX_VALUE)
                .addContainerGap())
        );
        logOutputFrameLayout.setVerticalGroup(
            logOutputFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
        );

        logOutputFrame.setBounds(20, 520, 1030, 170);
        jDesktopPane1.add(logOutputFrame, javax.swing.JLayeredPane.DEFAULT_LAYER);

        resultPanel.setFocusCycleRoot(true);
        resultPanel.setPreferredSize(new java.awt.Dimension(500, 500));

        javax.swing.GroupLayout resultPanelLayout = new javax.swing.GroupLayout(resultPanel);
        resultPanel.setLayout(resultPanelLayout);
        resultPanelLayout.setHorizontalGroup(
            resultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 515, Short.MAX_VALUE)
        );
        resultPanelLayout.setVerticalGroup(
            resultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 475, Short.MAX_VALUE)
        );

        resultTabPane.addTab("Filter", resultPanel);
        resultTabPane.addTab("Svg Symbol", svgPanel_);

        resultTabPane.setBounds(530, 10, 520, 500);
        jDesktopPane1.add(resultTabPane, javax.swing.JLayeredPane.DEFAULT_LAYER);

        inputTabPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                inputTabPaneStateChanged(evt);
            }
        });

        javax.swing.GroupLayout drawPanel_Layout = new javax.swing.GroupLayout(drawPanel_);
        drawPanel_.setLayout(drawPanel_Layout);
        drawPanel_Layout.setHorizontalGroup(
            drawPanel_Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 485, Short.MAX_VALUE)
        );
        drawPanel_Layout.setVerticalGroup(
            drawPanel_Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 475, Short.MAX_VALUE)
        );

        inputTabPane.addTab("Mouse Input", drawPanel_);

        videoTabPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                videoTabPaneStateChanged(evt);
            }
        });

        liveImage.setPreferredSize(new java.awt.Dimension(416, 312));

        javax.swing.GroupLayout liveImageLayout = new javax.swing.GroupLayout(liveImage);
        liveImage.setLayout(liveImageLayout);
        liveImageLayout.setHorizontalGroup(
            liveImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 416, Short.MAX_VALUE)
        );
        liveImageLayout.setVerticalGroup(
            liveImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 312, Short.MAX_VALUE)
        );

        jLabel1.setText("Red Max");

        jLabel2.setText("Green Max");

        jLabel3.setText("Blue Max");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(liveImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(redHiSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(greenHiSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(blueHiSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(liveImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(redHiSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(greenHiSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(blueHiSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17))
        );

        videoTabPane.addTab("Color Pick", jPanel1);

        processedImage.setPreferredSize(new java.awt.Dimension(416, 312));

        javax.swing.GroupLayout processedImageLayout = new javax.swing.GroupLayout(processedImage);
        processedImage.setLayout(processedImageLayout);
        processedImageLayout.setHorizontalGroup(
            processedImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 416, Short.MAX_VALUE)
        );
        processedImageLayout.setVerticalGroup(
            processedImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 303, Short.MAX_VALUE)
        );

        jLabel4.setText("Red Min");

        jLabel5.setText("Green Min");

        jLabel6.setText("Blue Min");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(redLoSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(greenLoSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(blueLoSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6))
                    .addComponent(processedImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(processedImage, javax.swing.GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
                .addGap(11, 11, 11)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(redLoSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(16, 16, 16)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(greenLoSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(16, 16, 16)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(blueLoSlider, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                        .addGap(39, 39, 39))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(51, 51, 51))))
        );

        videoTabPane.addTab("Video Feedback", jPanel3);

        inputTabPane.addTab("Video Input", videoTabPane);

        inputTabPane.setBounds(20, 10, 490, 500);
        jDesktopPane1.add(inputTabPane, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Close");
        jMenuItem1.setToolTipText("Exit application");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Clear");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        optionsMenu.setText("Options");

        vInvert.setSelected(it.uniroma1.dipinfo.sketch.config.Config.getInstance().isMIRROR_INVERT());
        vInvert.setText("Vertical Invert");
        vInvert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vInvertActionPerformed(evt);
            }
        });
        optionsMenu.add(vInvert);

        mirrorEffect.setSelected(it.uniroma1.dipinfo.sketch.config.Config.getInstance().isMIRROR_INVERT());
        mirrorEffect.setText("Mirror Effect");
        mirrorEffect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mirrorEffectActionPerformed(evt);
            }
        });
        optionsMenu.add(mirrorEffect);

        jMenuBar1.add(optionsMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1060, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        //Esce dall'applicativo:
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // pulisce gli schermi:
        System.out.println("[DEBUG]: Premuto Edit");
        PanelAccessor.getDrawPanel().cleanScreen();
        PanelAccessor.getOutputText().setText("");
        PanelAccessor.getResultPanel().cleanScreen();
        imp_.initTrajectoryVars();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void inputTabPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_inputTabPaneStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_inputTabPaneStateChanged

    private void videoTabPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_videoTabPaneStateChanged
    }//GEN-LAST:event_videoTabPaneStateChanged

    private void vInvertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vInvertActionPerformed
        // TODO add your handling code heres:
        JCheckBoxMenuItem sourceItem = (JCheckBoxMenuItem) evt.getSource();
        cameraController_.setRotate_(sourceItem.getState());
        System.out.println("[DEBUG]: Premuto su vInvert" + sourceItem.getState());

    }//GEN-LAST:event_vInvertActionPerformed

    private void mirrorEffectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mirrorEffectActionPerformed
        // TODO add your handling code here:
        JCheckBoxMenuItem sourceItem = (JCheckBoxMenuItem) evt.getSource();
        cameraController_.setMirror_(sourceItem.getState());
        System.out.println("[DEBUG]: Premuto su mirrorEffect" + sourceItem.getState());
    }//GEN-LAST:event_mirrorEffectActionPerformed

    /**
     * Main function
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            // ClassNotFoundException, InstantiationException
            // IllegalAccessException, UnsupportedLookAndFeelException
        }

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    MainJFrame app = new MainJFrame();
                    app.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 2 - app.getWidth() / 2,
                            Toolkit.getDefaultToolkit().getScreenSize().height / 2 - app.getHeight() / 2);
                    app.setVisible(true);

                } catch (Exception e) {
                }
            }
        });


    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSlider blueHiSlider;
    private javax.swing.JSlider blueLoSlider;
    private javax.swing.JPanel drawPanel_;
    private javax.swing.JSlider greenHiSlider;
    private javax.swing.JSlider greenLoSlider;
    private javax.swing.JTabbedPane inputTabPane;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel liveImage;
    private javax.swing.JInternalFrame logOutputFrame;
    private javax.swing.JCheckBoxMenuItem mirrorEffect;
    private javax.swing.JMenu optionsMenu;
    private javax.swing.JTextArea outputText;
    private javax.swing.JPanel processedImage;
    private javax.swing.JSlider redHiSlider;
    private javax.swing.JSlider redLoSlider;
    private javax.swing.JPanel resultPanel;
    private javax.swing.JTabbedPane resultTabPane;
    private javax.swing.JCheckBoxMenuItem vInvert;
    private javax.swing.JTabbedPane videoTabPane;
    // End of variables declaration//GEN-END:variables

    /**
     * Called when a selection box is drawn in video input mode.
     * Scan the selected region of the image and determine the lowest and highest RGB values.
     *
     * @param e
     */
    public void selectionPerformed(SelectionEvent e) {

        int r = 0, g = 0, b = 0, minRed = 255, minGreen = 255, minBlue = 255, maxRed = 0, maxGreen = 0, maxBlue = 0;
        BufferedImage image = ((JSelectionImagePanel) (e.getSource())).getImage();
        System.out.println("Selection box drawn (X1=" + e.getX1() + ", Y1=" + e.getY1() + ", X2=" + e.getX2() + ", Y2=" + e.getY2() + ")");
        imp_.setDistX(Math.abs(e.getX2() - e.getX1()));
        imp_.setDistY(Math.abs(e.getY2() - e.getY1()));
        for (int x = Math.min(e.getX1(), e.getX2()); x <= Math.max(e.getX1(), e.getX2()); x++) // scan rows
        {
            for (int y = Math.min(e.getY1(), e.getY2()); y <= Math.max(e.getY1(), e.getY2()); y++) // scan pixels in current row
            {

                Color c = new Color(image.getRGB(x, y));
                r = c.getRed();// get the rgb value for the current pixel
                g = c.getGreen();
                b = c.getBlue();

                if (r < minRed) {
                    minRed = r;
                }
                if (g < minGreen) {
                    minGreen = g;
                }
                if (b < minBlue) {
                    minBlue = b;
                }
                if (r > maxRed) {
                    maxRed = r;
                }
                if (g > maxGreen) {
                    maxGreen = g;
                }
                if (b > maxBlue) {
                    maxBlue = b;
                }
            }
        }

        System.out.println("  Lowest RGB = " + minRed + ", " + minGreen + ", " + minBlue);
        System.out.println("  Highest RGB = " + maxRed + ", " + maxGreen + ", " + maxBlue);
        //sets the sliders values
        redLoSlider.setValue(minRed);
        greenLoSlider.setValue(minGreen);
        blueLoSlider.setValue(minBlue);
        redHiSlider.setValue(maxRed);
        greenHiSlider.setValue(maxGreen);
        blueHiSlider.setValue(maxBlue);

    }

    /**
     * Prepares video components, such as color selection sliders.
     */
    public void prepareVideoComponents() {
        try {
            //setup code for event handlers and other stuff

            PanelAccessor.getLiveImage().addSelectionListener(this);
            PanelAccessor.getLiveImage().setImage(imp_.getLiveImage());
            PanelAccessor.getProcessedImage().setImage(imp_.getProcessedImage());
            if (cameraController_ == null) {
                cameraController_ = new CameraController(imp_);
                cameraController_.start();
            }
            imp_.startTracking();

            //JSlider settings
            redLoSlider.setMinimum(0);
            redLoSlider.setMaximum(255);
            redLoSlider.setValue(0);
            redLoSlider.setMajorTickSpacing(10);
            redLoSlider.setMinorTickSpacing(5);
            redLoSlider.setPaintTicks(true);
            redLoSlider.addChangeListener(this);

            greenLoSlider.setMinimum(0);
            greenLoSlider.setMaximum(255);
            greenLoSlider.setValue(0);
            greenLoSlider.setMajorTickSpacing(10);
            greenLoSlider.setMinorTickSpacing(5);
            greenLoSlider.setPaintTicks(true);
            greenLoSlider.addChangeListener(this);

            blueLoSlider.setMinimum(0);
            blueLoSlider.setMaximum(255);
            blueLoSlider.setValue(0);
            blueLoSlider.setMajorTickSpacing(10);
            blueLoSlider.setMinorTickSpacing(5);
            blueLoSlider.setPaintTicks(true);
            blueLoSlider.addChangeListener(this);

            redHiSlider.setMinimum(0);
            redHiSlider.setMaximum(255);
            redHiSlider.setValue(0);
            redHiSlider.setMajorTickSpacing(10);
            redHiSlider.setMinorTickSpacing(5);
            redHiSlider.setPaintTicks(true);
            redHiSlider.addChangeListener(this);

            greenHiSlider.setMinimum(0);
            greenHiSlider.setMaximum(255);
            greenHiSlider.setValue(0);
            greenHiSlider.setMajorTickSpacing(10);
            greenHiSlider.setMinorTickSpacing(5);
            greenHiSlider.setPaintTicks(true);
            greenHiSlider.addChangeListener(this);

            blueHiSlider.setMinimum(0);
            blueHiSlider.setMaximum(255);
            blueHiSlider.setValue(0);
            blueHiSlider.setMajorTickSpacing(10);
            blueHiSlider.setMinorTickSpacing(5);
            blueHiSlider.setPaintTicks(true);
            blueHiSlider.addChangeListener(this);

            // Add an action listener to the Image Processor
            imp_.addImageUpdateListener(this);

        } catch (Exception ex) {
            Logger.getLogger(MainJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    /**
     * Sets the images acquired from webcam/camera
     */
    public void imagesUpdated() {
        //PanelAccessor.getLiveImage().setImage(imp_.getLiveImage());
        PanelAccessor.getProcessedImage().setImage(imp_.getProcessedImage());

    }

    /**
     * Called when a Slider is adjusted.
     *
     * @param e the event
     */
    public void stateChanged(ChangeEvent e) {
//        System.out.println("[DEBUG]: Lo stato e' cambiato");
        if (e.getSource().equals(redLoSlider)) {
            imp_.setMinRed(redLoSlider.getValue());
        } else if (e.getSource().equals(greenLoSlider)) {
            imp_.setMinGreen(greenLoSlider.getValue());
        } else if (e.getSource().equals(blueLoSlider)) {
            imp_.setMinBlue(blueLoSlider.getValue());
        } else if (e.getSource().equals(redHiSlider)) {
            imp_.setMaxRed(redHiSlider.getValue());
        } else if (e.getSource().equals(greenHiSlider)) {
            imp_.setMaxGreen(greenHiSlider.getValue());
        } else if (e.getSource().equals(blueHiSlider)) {
            imp_.setMaxBlue(blueHiSlider.getValue());
        }
    }

    /**
     * Event handler
     *
     * @param windowevent
     */
    public void windowClosing(WindowEvent windowevent) {
        setVisible(false);
        imp_.shutdown();
    }

    /**
     * Event handler
     *
     * @param e the event
     */
    public void windowActivated(WindowEvent e) {
    }

    /**
     * Event handler
     *
     * @param e the event
     */
    public void windowClosed(WindowEvent e) {
    }

    /**
     * Event handler
     *
     * @param e the event
     */
    public void windowDeactivated(WindowEvent e) {
    }

    /**
     * Event handler
     *
     * @param e the event
     */
    public void windowDeiconified(WindowEvent e) {
    }

    /**
     * Event handler
     *
     * @param e the event
     */
    public void windowIconified(WindowEvent e) {
    }

    /**
     * Event handler
     *
     * @param e the event
     */
    public void windowOpened(WindowEvent e) {
    }
    
    /**
     * Listener for selection between Mouse and Webcam
     */
    ChangeListener changeListenerInputPane = new ChangeListener() {

        public void stateChanged(ChangeEvent changeEvent) {
            JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
            int index = sourceTabbedPane.getSelectedIndex();
            System.out.println("[DEBUG]: Tab changed to: " + sourceTabbedPane.getTitleAt(index) + " " + index);
            if (index == 0) {
                //spegne la webcam
                System.out.println("[DEBUG]: Disabling video input...");
                imp_.shutdown();
                //cameraController_.shutdown(); disabilitato va in eccezione la dll

            } else if (index == 1) {
                try {
                    System.out.println("[DEBUG]: Preparing video components...");
                    prepareVideoComponents();
                } catch (Exception ex) {
                    PanelAccessor.getOutputText().append("ERROR: Webcam initialization failed! \n");
                }
            }
        }
    };
    ChangeListener changeListenerPickPane = new ChangeListener() {

        public void stateChanged(ChangeEvent changeEvent) {
            JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
            int index = sourceTabbedPane.getSelectedIndex();
            System.out.println("[DEBUG]: Tab changed to: " + sourceTabbedPane.getTitleAt(index) + " " + index);
            JImagePanel j = (JImagePanel) liveImage;
            if (index == 0) {
                j.setDraw_(true);
            } else if (index == 1) {
                j.setDraw_(false);
            }
        }
    };
}

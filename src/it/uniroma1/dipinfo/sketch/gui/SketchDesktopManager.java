package it.uniroma1.dipinfo.sketch.gui;

import javax.swing.DefaultDesktopManager;
import javax.swing.JComponent;

/**
 * This class is here in order to prevent the user from dragging
 * InternalJFrames on the layout panel
 *
 * @author Alex Dafinei
 */
public class SketchDesktopManager extends DefaultDesktopManager {

    @Override
    public void dragFrame(JComponent f, int newX, int newY) {

}


    @Override
public void beginDraggingFrame(JComponent f) {

}


    @Override
public void endDraggingFrame(JComponent f) {

}

}

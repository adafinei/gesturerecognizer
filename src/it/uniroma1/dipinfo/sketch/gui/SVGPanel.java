/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package it.uniroma1.dipinfo.sketch.gui;

import it.uniroma1.dipinfo.sketch.utils.ExternalFileLocator;
import it.uniroma1.dipinfo.sketch.config.Config;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JPanel;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.GVTTreeRendererAdapter;
import org.apache.batik.swing.gvt.GVTTreeRendererEvent;
import org.apache.batik.swing.svg.GVTTreeBuilderAdapter;
import org.apache.batik.swing.svg.GVTTreeBuilderEvent;
import org.apache.batik.swing.svg.SVGDocumentLoaderAdapter;
import org.apache.batik.swing.svg.SVGDocumentLoaderEvent;
import org.w3c.dom.svg.SVGDocument;

/**
 * This class will display an SVG recognized symbol
 *
 * @author Alex Dafinei
 */
public class SVGPanel extends JPanel {

    /**Canvas for SVG drawing */
    protected JSVGCanvas svgCanvas_=new JSVGCanvas();
    private String absLibPath_=Config.getInstance().getSVG_ABS_PATH_LIBRARY_FOLDER();
    private String relLibPath_=Config.getInstance().getSVG_LIBRARY_FOLDER();
    

    /**
     * Constructor
     */
    public SVGPanel(){
        setBackground(Color.white); // Imposta "Bianco" il colore dello Sfondo del JPanel.
        add(svgCanvas_,BorderLayout.NORTH);
    }

    /**
     * Starts the drawing of a recognized symbol
     *
     * @param uri name of the symbol in library
     * @param width width of the symbol: actually not used
     * @param height height of the symbol: actually not used
     * @param x x coordinate where to start the symbol
     * @param y y coordinate where to start the symbol
     */
    public void drawBeautifiedSymbol(String uri, double width, double height, double x, double y){
        //controllo per le linee
        if(uri.startsWith("LINE_W")||uri.startsWith("LINE_E"))
            uri="HORIZONTAL_LINE";

        if(uri.startsWith("LINE_N")||uri.startsWith("LINE_S"))
            uri="VERTICAL_LINE";

        //crea gli event listener per l'SVG
        svgCanvas_.addSVGDocumentLoaderListener(new SVGDocumentLoaderAdapter() {
            @Override
            public void documentLoadingStarted(SVGDocumentLoaderEvent e) {
                //System.out.println("[DEBUG]: SVGPanel Document Loading...");
            }
            @Override
            public void documentLoadingCompleted(SVGDocumentLoaderEvent e) {
                //System.out.println("[DEBUG]: SVGPanel Document Loaded.Resizing...");
                SVGDocument  doc = e.getSVGDocument();
                //eventuale logica per la manipolazione va inserita qui
            }
        });

        svgCanvas_.addGVTTreeBuilderListener(new GVTTreeBuilderAdapter() {
            @Override
            public void gvtBuildStarted(GVTTreeBuilderEvent e) {
                //System.out.println("[DEBUG]: SVGPanel Build Started...");
            }
            @Override
            public void gvtBuildCompleted(GVTTreeBuilderEvent e) {
                //System.out.println("[DEBUG]: SVGPanel Build Done.");
            }
        });

        svgCanvas_.addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
            @Override
            public void gvtRenderingPrepare(GVTTreeRendererEvent e) {
            //System.out.println("[DEBUG]: SVGPanel Rendering Started...");
            }
            @Override
            public void gvtRenderingCompleted(GVTTreeRendererEvent e) {
            //System.out.println("[DEBUG]: SVGPanel Rendering Completed...");
            }
        });
        svgCanvas_.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
        
        String symbol_uri="";

        if(Config.getInstance().isRUNNING_IN_IDE()){
            symbol_uri=absLibPath_+uri+".svg";
        }else{
            ExternalFileLocator efl=new ExternalFileLocator();
            symbol_uri=efl.getAbsolutePath(relLibPath_)+uri+".svg";
        }
        PanelAccessor.getOutputText().append("\n Drawing symbol at path: "+symbol_uri);
        svgCanvas_.setURI(symbol_uri);
    }
    /**
     * Accessor method
     *
     * @return the SVG Canvas
     */
    public JSVGCanvas getSvgCanvas() {
        return svgCanvas_;
    }
    /**
     * Accessor method
     *
     * @param svgCanvas the SVG Canvas to set
     */
    public void setSvgCanvas(JSVGCanvas svgCanvas) {
        this.svgCanvas_ = svgCanvas;
    }
}

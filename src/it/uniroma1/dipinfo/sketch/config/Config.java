/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package it.uniroma1.dipinfo.sketch.config;

import java.util.ResourceBundle;

/**
 *
 * This class loads configuration parameters from a properties file
 *
 * @author Alex Dafinei
 */
public class Config {
    private static ResourceBundle b;
    private static Config instance= null;

    //Variables

private String CAM_DRIVER;
private String CAM_DEVICE;
private boolean VERTICAL_ROTATION;
private boolean MIRROR_INVERT;
private int COLOR_THRESHOLD;
private boolean LARGEST_REGION;
private int AREA_THRESHOLD_MIN;
private int AREA_THRESHOLD_MAX;
private int DIST_THRESHOLD_MIN;
private int DIST_THRESHOLD_MAX;
private int THRESHOLD_FIX_POINTS_NUMBER;
private int IGNORED_POINTS_THRESHOLD;
private int FRAME_GRAB_FREQUENCY;
private int MIN_POINTS_PER_STROKE;
private String DB_FOLDER_NAME;
private int ANGLE_RATIO_STEP_PERCENT;
private int IOP_THRESHOLD;
private int LINE_DENSITY_PERCENT;
private int POINTING_DIRECTION_PERCENT;
private String SVG_LIBRARY_FOLDER;
private boolean RUNNING_IN_IDE;
private String SVG_ABS_PATH_LIBRARY_FOLDER;
private int TASK_TIMEOUT;
private String DB_ABS_PATH;
private String DB_DRIVER;
private String DB_PROTOCOL;
private int CONFLICT_THRESHOLD;
private int SYMBOL_DET;


/**
 * This class is a singleton, therefore getInstance should be called
 * before invoking other methods on this object
 *
 * @return an instance of this class
 */
    public synchronized static Config getInstance() {
        if(instance == null) {
            instance= new Config();
        }
        return instance;
    }

        private Config() {
        //Percorso relativo al file di properties contenente le informazioni
        try{
            Package packagePath= this.getClass().getPackage();
            String packageName= packagePath.getName();
            b= ResourceBundle.getBundle(packageName + ".config");
            //Integer.parseInt(RESOURCE_BUNDLE.getString("IS_CLUSTER"));
            CAM_DRIVER= b.getString("CAM_DRIVER").trim();
            CAM_DEVICE= b.getString("CAM_DEVICE").trim();
            VERTICAL_ROTATION= Boolean.parseBoolean(b.getString("VERTICAL_ROTATION").trim());
            MIRROR_INVERT= Boolean.parseBoolean(b.getString("MIRROR_INVERT").trim());
            COLOR_THRESHOLD= Integer.parseInt(b.getString("COLOR_THRESHOLD").trim());
            LARGEST_REGION= Boolean.parseBoolean(b.getString("LARGEST_REGION").trim());
            AREA_THRESHOLD_MIN= Integer.parseInt(b.getString("AREA_THRESHOLD_MIN").trim());
            AREA_THRESHOLD_MAX=Integer.parseInt(b.getString("AREA_THRESHOLD_MAX").trim());
            DIST_THRESHOLD_MIN=Integer.parseInt(b.getString("DIST_THRESHOLD_MIN").trim());
            DIST_THRESHOLD_MAX=Integer.parseInt(b.getString("DIST_THRESHOLD_MAX").trim());
            THRESHOLD_FIX_POINTS_NUMBER=Integer.parseInt(b.getString("THRESHOLD_FIX_POINTS_NUMBER").trim());
            IGNORED_POINTS_THRESHOLD=Integer.parseInt(b.getString("IGNORED_POINTS_THRESHOLD").trim());
            FRAME_GRAB_FREQUENCY=Integer.parseInt(b.getString("FRAME_GRAB_FREQUENCY").trim());
            MIN_POINTS_PER_STROKE=Integer.parseInt(b.getString("MIN_POINTS_PER_STROKE").trim());
            DB_FOLDER_NAME=b.getString("DB_FOLDER_NAME");
            ANGLE_RATIO_STEP_PERCENT=Integer.parseInt(b.getString("ANGLE_RATIO_STEP_PERCENT"));
            IOP_THRESHOLD=Integer.parseInt(b.getString("IOP_THRESHOLD").trim());
            LINE_DENSITY_PERCENT=Integer.parseInt(b.getString("LINE_DENSITY_PERCENT").trim());
            POINTING_DIRECTION_PERCENT=Integer.parseInt(b.getString("POINTING_DIRECTION_PERCENT").trim());
            SVG_LIBRARY_FOLDER=b.getString("SVG_LIBRARY_FOLDER");
            RUNNING_IN_IDE= Boolean.parseBoolean(b.getString("RUNNING_IN_IDE").trim());
            SVG_ABS_PATH_LIBRARY_FOLDER=b.getString("SVG_ABS_PATH_LIBRARY_FOLDER");
            TASK_TIMEOUT=Integer.parseInt(b.getString("TASK_TIMEOUT").trim());
            CONFLICT_THRESHOLD=Integer.parseInt(b.getString("CONFLICT_THRESHOLD").trim());
            DB_ABS_PATH=b.getString("DB_ABS_PATH");
            DB_DRIVER=b.getString("DB_DRIVER");
            DB_PROTOCOL=b.getString("DB_PROTOCOL");
            SYMBOL_DET=Integer.parseInt(b.getString("SYMBOL_DET").trim());
        }catch (Exception ex){
            System.err.print(ex.toString());
        }
    }

    /**
     * Returns the camera driver
     * 
     * @return the camera driver
     */
    public String getCAM_DRIVER() {
        return CAM_DRIVER;
    }

    /**
     * Returns the camera device
     * 
     * @return the camera device
     */
    public String getCAM_DEVICE() {
        return CAM_DEVICE;
    }

    /**
     * Returns the vertical rotation state
     *
     * @return the vertical rotation state
     */
    public boolean isVERTICAL_ROTATION() {
        return VERTICAL_ROTATION;
    }

    /**
     * Returns the mirror invertion state
     *
     * @return the mirror invertion state
     */
    public boolean isMIRROR_INVERT() {
        return MIRROR_INVERT;
    }

    /**
     * Returns the threshold within which two colors are considered equivalent
     *
     * @return the threshold within which two colors are considered equivalent
     */
    public int getCOLOR_THRESHOLD() {
        return COLOR_THRESHOLD;
    }

    /**
     * Returns the largest region/user size region state
     *
     * @return the largest region/user size region state
     */
    public boolean isLARGEST_REGION() {
        return LARGEST_REGION;
    }

    /**
     * Returns the minimum area threshold for an object
     *
     * @return the minimum area threshold for an object
     */
    public int getAREA_THRESHOLD_MIN() {
        return AREA_THRESHOLD_MIN;
    }

    /**
     * Returns the maximum area threshold for a tracked object
     *
     * @return the maximum area threshold for a tracked object
     */
    public int getAREA_THRESHOLD_MAX() {
        return AREA_THRESHOLD_MAX;
    }

    /**
     * Returns the maximum number of fixed points during tracking before which
     * considering stroke started or ended
     *
     * @return the maximum number of fixed points during tracking before which
     * considering stroke started or ended
     */
    public int getTHRESHOLD_FIX_POINTS_NUMBER() {
        return THRESHOLD_FIX_POINTS_NUMBER;
    }

    /**
     * Returns the threshold of ignored points
     *
     * @return the threshold of ignored points
     */
    public int getIGNORED_POINTS_THRESHOLD() {
        return IGNORED_POINTS_THRESHOLD;
    }


    /**
     * Returns the frequency with which perform the image processing
     * as the webcams usually declare 30 fps but rarely are beyound 15 fps
     * this value shoud stay between 33 and 66
     *
     * @return the frequency with which perform the image processing
     * as the webcams usually declare 30 fps but rarely are beyound 15 fps
     * this value shoud stay between 33 and 66
     */
    public int getFRAME_GRAB_FREQUENCY() {
        return FRAME_GRAB_FREQUENCY;
    }

    /**
     * Returns @return minimum number of points allowed per stroke
     *
     * @return minimum number of points allowed per stroke
     */
    public int getMIN_POINTS_PER_STROKE() {
        return MIN_POINTS_PER_STROKE;
    }

    /**
     * Returns the relative to the jar file folder name of the database
     *
     * @return the relative to the jar file folder name of the database
     */
    public String getDB_FOLDER_NAME() {
        return DB_FOLDER_NAME;
    }

    /**
     * Returnsa percent used by the feature, it determines the step
     * for segment approximation of a shape
     *
     * @return a percent used by the feature, it determines the step
     * for segment approximation of a shape
     */
    public int getANGLE_RATIO_STEP_PERCENT() {
        return ANGLE_RATIO_STEP_PERCENT;
    }

    /**
     * Returns a percent used by the feature IOP gets a threshold for calculating
     * difference of points
     *
     * @return a percent used by the feature IOP gets a threshold for calculating
     * difference of points
     */
    public int getIOP_THRESHOLD() {
        return IOP_THRESHOLD;
    }

    /**
     * Returns a percent used by the feature LD used to calculate NORTH/WEST/EAST/SOUTH
     * regions of the bounding rectangle
     *
     * @return a percent used by the feature LD used to calculate NORTH/WEST/EAST/SOUTH
     * regions of the bounding rectangle
     */
    public int getLINE_DENSITY_PERCENT() {
        return LINE_DENSITY_PERCENT;
    }

    /**
     * Returns a percent used by the feature PD
     *
     * @return a percent used by the feature PD
     */
    public int getPOINTING_DIRECTION_PERCENT() {
        return POINTING_DIRECTION_PERCENT;
    }

 
    /**
     * Returns the folder containing the SVG symbols
     *
     * @return the folder containing the SVG symbols
     */
    public String getSVG_LIBRARY_FOLDER() {
        return SVG_LIBRARY_FOLDER;
    }

    /**
     * Returns the status whether FcBD is launched from IDE or from the JAR
     *
     * @return the status whether FcBD is launched from IDE or from the JAR
     */
    public boolean isRUNNING_IN_IDE() {
        return RUNNING_IN_IDE;
    }

    /**
     * Returns the absolute path to the folder containing the SVG symbols
     *
     * @return the absolute path to the folder containing the SVG symbols
     */
    public String getSVG_ABS_PATH_LIBRARY_FOLDER() {
        return SVG_ABS_PATH_LIBRARY_FOLDER;
    }

    /**
     * Returns the time which should elapse before starting recognition in multistroke
     * mode
     * @return the time which should elapse before starting recognition in multistroke
     * mode
     */
    public int getTASK_TIMEOUT() {
        return TASK_TIMEOUT;
    }

    /**
     * Returns the absolute path to the folder containing the DB
     *
     * @return the absolute path to the folder containing the DB
     */
    public String getDB_ABS_PATH() {
        return DB_ABS_PATH;
    }

    /**
     * Returns the driver for the DB
     * @return the driver for the DB
     */
    public String getDB_DRIVER() {
        return DB_DRIVER;
    }

    /**
     * Returns the protocol for the DB
     *
     * @return the protocol for the DB
     */
    public String getDB_PROTOCOL() {
        return DB_PROTOCOL;
    }

    /**
     * Returns the threshold between certainty degrees for declaring a conflict
     *
     * @return the threshold between certainty degrees for declaring a conflict
     */
    public int getCONFLICT_THRESHOLD() {
        return CONFLICT_THRESHOLD;
    }

    /**
     * Returns the threshold under which a symbol is unknown
     *
     * @return the threshold under which a symbol is unknown
     */
    public int getSYMBOL_DET() {
        return SYMBOL_DET;
    }

    /**
     * @return the DIST_THRESHOLD_MIN
     */
    public int getDIST_THRESHOLD_MIN() {
        return DIST_THRESHOLD_MIN;
    }

    /**
     * @return the DIST_THRESHOLD_MAX
     */
    public int getDIST_THRESHOLD_MAX() {
        return DIST_THRESHOLD_MAX;
    }

}

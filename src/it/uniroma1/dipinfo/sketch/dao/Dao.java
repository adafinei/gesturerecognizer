package it.uniroma1.dipinfo.sketch.dao;

import it.uniroma1.dipinfo.sketch.dao.pojo.FeaturePojo;
import it.uniroma1.dipinfo.sketch.dao.pojo.SymbolFeatureValues;
import it.uniroma1.dipinfo.sketch.utils.ExternalFileLocator;
import it.uniroma1.dipinfo.sketch.config.Config;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * This class provides static methods for retrieving information from the database
 * Information is retrieved in POJO containers - POJOS are in the package dao.pojo
 * 
 * @author Alex Dafinei
 */
public class Dao {

    private static Dao instance_ = null;
    private static final String driver_ = Config.getInstance().getDB_DRIVER();
    private static final String protocol_ = Config.getInstance().getDB_PROTOCOL();
    private static String dbName_ = Config.getInstance().getDB_ABS_PATH();
    private static String relDbName_ = Config.getInstance().getDB_FOLDER_NAME();
    private Connection conn_ = null;

    private Dao() {
        
        try {
            loadDriver();
            getConnection();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    /**
     * Dao is a singleton class, invoke getInstance() and then the other
     * methods.
     *
     * @return an instance of Dao
     */
    public synchronized static Dao getInstance() {
        if (instance_ == null) {
            instance_ = new Dao();
        }
        return instance_;
    }

    /**
     * Loads the appropriate JDBC driver for this environment/framework. For
     * example, if we are in an embedded environment, we load Derby's
     * embedded Driver, <code>org.apache.derby.jdbc.EmbeddedDriver</code>.
     */
    private void loadDriver() {
        try {
            Class.forName(driver_).newInstance();
            System.out.println("Loaded the appropriate driver");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("\nUnable to load the JDBC driver " + driver_);
            System.err.println("Please check your CLASSPATH.");
            cnfe.printStackTrace(System.err);
        } catch (InstantiationException ie) {
            System.err.println(
                    "\nUnable to instantiate the JDBC driver " + driver_);
            ie.printStackTrace(System.err);
        } catch (IllegalAccessException iae) {
            System.err.println(
                    "\nNot allowed to access the JDBC driver " + driver_);
            iae.printStackTrace(System.err);
        }
    }

    /**
     * Gets a Connection to the database
     *
     * @throws SQLException
     */
    private void getConnection() throws SQLException {
        Properties props = new Properties();
        props.put("user", "");
        props.put("password", "");
        //setta il path al database
        if(!Config.getInstance().isRUNNING_IN_IDE()){
            ExternalFileLocator efl=new ExternalFileLocator();
            conn_ = DriverManager.getConnection(protocol_ +
                    efl.getAbsolutePathWin(relDbName_) + ";create=false", props);
        }else{
            conn_ = DriverManager.getConnection(protocol_ + dbName_ + ";create=false", props);
        }
        System.out.println("Connected to database " + dbName_);
    }

    /**
     * Gets info for the features for each recognizer; this method is not actually used.
     * Its aim is to automatically retrieve all information for recognizer
     *
     *
     * @param recognizerName the name of the recognizer
     * @return a list of features used by that recognizer, their names, and their weight in
     * the recognition process
     */
    public List<SymbolFeatureValues> getFeaturesInfoByRecognizerName(String recognizerName) {
        List<SymbolFeatureValues> lista = new ArrayList<SymbolFeatureValues>();
        
        String query = "SELECT DISTINCT RECOGNIZER_CLASSNAME, FEATURE_NAME, IN_INTERVAL, MIN_VALUE, "
                + "MAX_VALUE, WEIGHT FROM SYMBOLS, SYMBOLS_FEATURES, FEATURES WHERE SYMBOLS.RECOGNIZER_CLASSNAME "
                + "LIKE '" + recognizerName + "' AND SYMBOL_ID=SYMBOLS.ID AND FEATURE_ID=FEATURES.ID "
                + "AND NOT(FEATURE_NAME LIKE 'LD' OR FEATURE_NAME  LIKE 'PD' OR FEATURE_NAME LIKE 'SPD')";
        Statement stmt;
        try {
            stmt = conn_.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String rec = rs.getString("RECOGNIZER_CLASSNAME");
                String fn = rs.getString("FEATURE_NAME");
                int in = rs.getInt("IN_INTERVAL");
                double miv=rs.getDouble("MIN_VALUE");
                double mav=rs.getDouble("MAX_VALUE");
                int weight=rs.getInt("WEIGHT");
                SymbolFeatureValues sfv = new SymbolFeatureValues(rec,fn,in,miv,mav,weight);
                lista.add(sfv);
            }
            stmt.close();
        } catch (Exception ex) {
            ex.toString();
        }
        return lista;
    }
    /**
     * Gets info for the features for each recognizer, and each feature
     * This method is used in FcBD for retrieving data
     *
     * @param recognizerName  the name of the recognizer
     * @return a list of features used by that recognizer, their names, and their weight in
     * the recognition process
     */
    public SymbolFeatureValues getFeaturesInfoByRecognizerName(String recognizerName, String featName) {
        SymbolFeatureValues sfv=null;

        String query = "SELECT DISTINCT RECOGNIZER_CLASSNAME, FEATURE_NAME, IN_INTERVAL, MIN_VALUE, "
                + "MAX_VALUE, WEIGHT FROM SYMBOLS, SYMBOLS_FEATURES, FEATURES WHERE "
                + "SYMBOLS.RECOGNIZER_CLASSNAME LIKE '"+recognizerName+"' AND SYMBOL_ID=SYMBOLS.ID "
                + "AND FEATURE_ID=FEATURES.ID AND FEATURE_NAME LIKE '"+featName+"'";
        Statement stmt;
        try {
            stmt = conn_.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String rec = rs.getString("RECOGNIZER_CLASSNAME");
                String fn = rs.getString("FEATURE_NAME");
                int in = rs.getInt("IN_INTERVAL");
                double miv=rs.getDouble("MIN_VALUE");
                double mav=rs.getDouble("MAX_VALUE");
                int weight=rs.getInt("WEIGHT");
                sfv = new SymbolFeatureValues(rec,fn,in,miv,mav,weight);    
            }
            stmt.close();
        } catch (Exception ex) {
            ex.toString();
        }
        return sfv;
    }

    /**
     * Gets all the features from the database
     *
     * @return a list of feature classe to be loaded in a Map by the FeatureFactory
     */
    public List<FeaturePojo> getFeatures() {
        List<FeaturePojo> lista = new ArrayList<FeaturePojo>();
        String query;
        query = "SELECT FEATURE_NAME, FEATURE_CLASS FROM FEATURES";
        Statement stmt;
        try {
            stmt = conn_.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String featureName = rs.getString("FEATURE_NAME");
                String featureClass = rs.getString("FEATURE_CLASS");
                FeaturePojo fp = new FeaturePojo(featureName, featureClass);
                lista.add(fp);
            }
            stmt.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return lista;
    }

    /**
     * Gets all the recognizers from the database
     * 
     * @return a list of Strings containing the classnames for the recognizers.
     */
    public List<String> getRecognizers() {
        List<String> lista = new ArrayList<String>();
        String query;
        query = "SELECT DISTINCT RECOGNIZER_CLASSNAME FROM SYMBOLS";
        Statement stmt;
        try {
            stmt = conn_.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String rec = rs.getString("RECOGNIZER_CLASSNAME");
                lista.add(rec);
            }
            stmt.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return lista;
    }
}

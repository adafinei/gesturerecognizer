
package it.uniroma1.dipinfo.sketch.dao.pojo;

/**
 * Plain old java object aimed to contain two Strings, one for the Feature names
 * the other for the feature class names
 *
 * @author Alex Dafinei
 */
public class FeaturePojo {
    private String featureClass_;
    private String featureName_;


    /**
     * Constructor
     *
     * @param fn the name of the feature
     * @param fc the class of the feature
     */
    public FeaturePojo(String fn, String fc){
        this.featureName_=fn;
        this.featureClass_=fc;
        
    }

    /**
     * Accessor method
     *
     * @return the feature Class
     */
    public String getFeatureClass() {
        return featureClass_;
    }

    /**
     * Accessor method
     *
     * @param featureClass the feature Class to set
     */
    public void setFeatureClass(String featureClass) {
        this.featureClass_ = featureClass;
    }

    /**
     * Accessor method
     *
     * @return the feature Name
     */
    public String getFeatureName() {
        return featureName_;
    }

    /**
     * Accessor method
     * 
     * @param featureName the feature Name to set
     */
    public void setFeatureName(String featureName) {
        this.featureName_ = featureName;
    }

}


package it.uniroma1.dipinfo.sketch.dao.pojo;

/**
 * POJO containing join results between symbols and feature
 * @author Alex Dafinei
 */
public class SymbolFeatureValues implements Comparable<SymbolFeatureValues>{

    private String recognizer_;
    private String featureName_;
    private boolean inInterval_;
    private double minValue_;
    private double maxValue_;
    private double weight_;

    public SymbolFeatureValues(String rec, String fn, int in, double miv, double mav, int weight) {
        recognizer_=rec;
        featureName_=fn;
        setInInterval(in);
        minValue_=miv;
        maxValue_=mav;
        this.weight_=weight;
    }

    /**
     * Accessor method
     *
     * @return the recognizer
     */
    public String getRecognizer() {
        return recognizer_;
    }

    /**
     * Accessor method
     *
     * @param recognizer the recognizer to set
     */
    public void setRecognizer(String recognizer) {
        this.recognizer_ = recognizer;
    }

    /**
     * Accessor method
     *
     * @return the feature Name
     */
    public String getFeatureName() {
        return featureName_;
    }

    /**
     * Accessor method
     *
     * @param featureName the feature Name to set
     */
    public void setFeatureName(String featureName) {
        this.featureName_ = featureName;
    }

    /**
     * Accessor method
     *
     * @return the inInterval - specifies if feature value should be inside or outside
     * the feature interval of values
     */
    public boolean isInInterval() {
        return inInterval_;
    }

    /**
     * Accessor method
     *
     * @param inInterval specifies if feature value should be inside or outside
     * the feature interval
     */
    public final void setInInterval(int inInterval) {
        this.inInterval_ = inInterval==1?true:false;
    }

    /**
     * Accessor method
     *
     * @return the mininimum feature Value
     */
    public double getMinValue() {
        return minValue_;
    }

    /**
     * Accessor method
     *
     * @param minValue the mininimum feature Value to set
     */
    public void setMinValue(double minValue) {
        this.minValue_ = minValue;
    }

    /**
     * Accessor method
     *
     * @return the maximum feature Value
     */
    public double getMaxValue() {
        return maxValue_;
    }

    /**
     * Accessor method
     *
     * @param maxValue the maximum feature value Value to set
     */
    public void setMaxValue(double maxValue) {
        this.maxValue_ = maxValue;
    }

    /**
     * Accessor method
     *
     * @return the weight
     */
    public double getWeight() {
        return weight_;
    }

    /**
     * Accessor method
     * 
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight_ = weight;
    }

    public int compareTo(SymbolFeatureValues o) {
        return this.featureName_.compareTo(o.getFeatureName());
    }





}

package it.uniroma1.dipinfo.sketch.structures;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * This class contains information acquired during the drawing process,
 * about a single stroke. Information are first/last point of the stroke
 * start time and and time of the drawing process, and the list of points
 * of a stroke
 */
public class Stroke {

    private Point2D firstPoint;
    private Point2D lastPoint;
    private List<Point2D> pointsList;
    private TimeInterval timeInterval;
    

    /** Creates a new instance of Stroke */
    public Stroke(){}

    /** Creates a new instance of Stroke
     *
     * @param theFirstPoint the first point of the stroke
     * @param theLastPoint the last point of the stroke
     * @param thepointsList the list of points representing the stroke
     * @param theTimeInterval the interval of time between first and last point
     */
    public Stroke(Point2D theFirstPoint, Point2D theLastPoint, List<Point2D> thepointsList,
        TimeInterval theTimeInterval) {

        firstPoint = theFirstPoint;
        lastPoint = theLastPoint;

        pointsList = new ArrayList(thepointsList.size() + 2);
        pointsList.add(firstPoint);
        pointsList.addAll(thepointsList);
        pointsList.add(lastPoint);

        timeInterval = theTimeInterval;
    }

    /**
     * Draws the stroke on a graphic context
     *
     * @param g the graphic context
     */
    public void draw(Graphics2D g) {
        Point2D prevPoint = getFirstPoint();
        for (Iterator<Point2D> it = getPointsList().iterator(); it.hasNext();) {
            Point2D p = it.next();
            g.drawLine((int) prevPoint.getX(), (int) prevPoint.getY(), (int) p.getX(), (int) p.getY());
            prevPoint = p;
        }
        g.drawLine((int) prevPoint.getX(), (int) prevPoint.getY(),
            (int) getLastPoint().getX(), (int) getLastPoint().getY());        
    }

    /**
     * Accessor method; returns the first point
     *
     * @return the first point
     */
    public Point2D getFirstPoint() {
        return firstPoint;
    }

    /**
     * Accessor method; returns the last point
     *
     * @return the last point
     */
    public Point2D getLastPoint() {
        return lastPoint;
    }

    /**
     * Accessor method; gets the list of points
     *
     * @return a list of points
     */
    public List<Point2D> getPointsList() {
        return pointsList;
    }

    /**
     * Accessor method; Gets the time interval from when the stroke is started
     * until its finished
     *
     * @return the time interval
     */
    public TimeInterval getTimeInfo() {
        return timeInterval;
    }

    

    @Override
    public String toString() {
        return "Stroke (# punti=" + pointsList.size();
    }
    
    /**
     * Returns true if two points are near
     *
     * @return true if two points are near
     */
    public boolean areNear() {
        boolean isClose = false;
        double dist = getFirstPoint().distance(getLastPoint());
        if (dist < 32)
            isClose = true;     
       // System.out.println("Misura : Distanza Euclidea tra il Punto di START e quello di STOP --- (Se < 32 Disegno ; Se >= 33 Testo ----> " + dist);   
        return isClose;  
    }

    /**
     * Accessor method
     *
     * @param firstPoint the first point of the stroke to set
     */
    public void setFirstPoint(Point2D firstPoint) {
        this.firstPoint = firstPoint;
    }

    /**
     * Accessor method
     *
     * @param lastPoint the last point of the stroke to set
     */
    public void setLastPoint(Point2D lastPoint) {
        this.lastPoint = lastPoint;
    }

    /**
     * Accessor method
     *
     * @param pointsList the list of points to set
     */
    public void setPointsList(List<Point2D> pointsList) {
        this.pointsList = pointsList;
    }

    /**
     * Accessor method
     *
     * @param timeInterval the interval of time to set
     */
    public void setTimeInterval(TimeInterval timeInterval) {
        this.timeInterval = timeInterval;
    }

}

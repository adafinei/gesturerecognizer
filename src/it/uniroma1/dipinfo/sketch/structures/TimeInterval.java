package it.uniroma1.dipinfo.sketch.structures;

/**
 * Class representing a time interval
 */
public class TimeInterval {

    private long startMillis_;

    private long endMillis_;

    /** Creates a new instance of TimeInfo
     *
     * @param theStartMillis start time
     * @param theEndMillis end time
     */
    public TimeInterval(long theStartMillis, long theEndMillis) {
        startMillis_ = theStartMillis;
        endMillis_ = theEndMillis;
    }
    /** Creates a new instance of TimeInfo */
    public TimeInterval(){}

    /**
     * Accessor method
     *
     * @return the start time in ms
     */
    public long getStartMillis() {
        return startMillis_;
    }

    /**
     * Accessor method
     *
     * @return the end time in ms
     */
    public long getEndMillis() {
        return endMillis_;
    }

    /**
     * Gests the elapsed time in ms
     * 
     * @return the elapsed time in ms
     */
    public long getElapsedMillis() {
        return endMillis_ - startMillis_;
    }

    /**
     * Accessor method
     *
     * @param startMillis the start time in ms to set
     */
    public void setStartMillis(long startMillis) {
        this.startMillis_ = startMillis;
    }

    /**
     * Accessor method
     *
     * @param endMillis the end time in ms to set
     */
    public void setEndMillis(long endMillis) {
        this.endMillis_ = endMillis;
    }

}

package it.uniroma1.dipinfo.sketch.tracking;
/**
 * A selection listener interface used to notify classes when a selection has been drawn on an object.
 *
 * @author David Bull
 * @version 1.0, 20/03/2004
 */
public interface SelectionListener
{
	/**
	 * Calls the selection performed event, when the user selects a box over
         * the live image JPanel
         *
	 * @param e
	 */
	public void selectionPerformed(SelectionEvent e);
}

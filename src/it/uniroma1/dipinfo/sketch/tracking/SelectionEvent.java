package it.uniroma1.dipinfo.sketch.tracking;
/**
 * A class to store information about a selection event.
 * Stores the top-left and bottom-right coordinates of the selection,
 * as well as the object that the selection was drawn on.
 *
 * @author David Bull
 * @version 1.0, 20/03/2004
 */
public class SelectionEvent
{
	////////////////////////////////////////////////////
	// CONSTANTS AND CLASS VARIABLES
	////////////////////////////////////////////////////

	private Object source;
	private int X1, Y1, X2, Y2;






	////////////////////////////////////////////////////
	// CONSTRUCTOR
	////////////////////////////////////////////////////

	/**
	 *
	 * @param source the source of the event
	 * @param X1 coordinate of the selection box
	 * @param Y1 coordinate of the selection box
	 * @param X2 coordinate of the selection box
	 * @param Y2 coordinate of the selection box
	 */
	public SelectionEvent(Object source, int X1, int Y1, int X2, int Y2)
	{
		this.source = source;
		this.X1 = X1;
		this.Y1 = Y1;
		this.X2 = X2;
		this.Y2 = Y2;
	}






	////////////////////////////////////////////////////
	// METHODS
	////////////////////////////////////////////////////

	/**
	 * Returns the source of the event
         *
	 * @return the source of the event
	 */
	public Object getSource() { return source; }


	/**
         * Returns the coordinate of the selection box
	 *
	 * @return the coordinate of the selection box
	 */
	public int getX1() { return X1; }


	/**
	 * Returns coordinate of the selection box
         *
	 * @return Y1 coordinate of the selection bo
	 */
	public int getY1() { return Y1; }


	/**
	 * Returns coordinate of the selection box
         *
	 * @return X2 coordinate of the selection box
	 */
	public int getX2() { return X2; }


	/**
	 * Returns coordinate of the selection box
         *
	 * @return Y2 coordinate of the selection box
	 */
	public int getY2() { return Y2; }
}

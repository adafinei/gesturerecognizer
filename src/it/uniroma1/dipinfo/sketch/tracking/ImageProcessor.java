package it.uniroma1.dipinfo.sketch.tracking;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import it.uniroma1.dipinfo.sketch.gui.strokeproducer.StrokeProducer;
import it.uniroma1.dipinfo.sketch.recognizer.RecognizerManager;
import it.uniroma1.dipinfo.sketch.structures.Stroke;
import it.uniroma1.dipinfo.sketch.structures.TimeInterval;
import java.util.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.*;
import it.uniroma1.dipinfo.sketch.config.Config;

/**
 * Performs the blob colouring algorithm on frames
 * from the camera and locatates the largest object
 * of the required colour.
 *
 * Replaced Vectors with ArrayLists, removed synchronized code;
 * rewritten the flatten method; added buildTrajectory method.
 * replaced HSI recognition with RGB recognition.
 *
 * Credits to David Bull for inspiration
 *
 * @author Alex Dafinei
 * @version 2.1, 19/07/11
 * 
 */
public class ImageProcessor implements StrokeProducer {
    ////////////////////////////////////////////////////
    // CONSTANTS AND CLASS VARIABLES
    ////////////////////////////////////////////////////

    /**Frequency at which perform the tracking/processing */
    public static final int FRAME_GRAB_FREQUENCY = Config.getInstance().getFRAME_GRAB_FREQUENCY();
    /**Color of regions which are not in the minRed,maxRed - minGreen,maxGreen, minBlue,maxBlue interval */
    public static final int EMPTY_REGION = Color.BLACK.getRGB();
    /**Color of the box used to track colors */
    public static final Color BOX_COLOR = Color.RED;
    /** Size threshold for accepting color regions */
    public static int AREA_THRESHOLD_MIN = Config.getInstance().getAREA_THRESHOLD_MIN(); //smaller objects will be ignored
    /** Size threshold for accepting color regions */
    public static int AREA_THRESHOLD_MAX = Config.getInstance().getAREA_THRESHOLD_MAX(); //attualmente non usati, questi 2 campi
    //fanno parte del metodo build trajectory. scommentare per l'eventuale uso, limitano l'area della'oggetto da considerare
    //bigger objects will be ignored
    /** Size threshold for the number of points captured before launching the recognition engine */
    public static final int THRESHOLD_LIST_SIZE = Config.getInstance().getMIN_POINTS_PER_STROKE();
    /** Threshold for determining if to start or stop the stroke acquisition process */
    public static final int THRESHOLD_FIX_POINTS_NUMBER = Config.getInstance().getTHRESHOLD_FIX_POINTS_NUMBER();
    /** Threshold for determining if too many points are ignored; if this is the case, data acquired is reset */
    public static final int IGNORED_POINTS_THRESHOLD = Config.getInstance().getIGNORED_POINTS_THRESHOLD();
    /** Threshold for distances between points. under min we count fix points, between min and max user is drawing, over max we count ignored points */
    public static final int DIST_THRESHOLD_MIN = Config.getInstance().getDIST_THRESHOLD_MIN();
    /** Threshold for distances between points. under min we count fix points, between min and max user is drawing, over max we count ignored points */
    public static final int DIST_THRESHOLD_MAX = Config.getInstance().getDIST_THRESHOLD_MAX();
    /** counts the number of points more or less in the same area*/
    private static int fixPointsEnd_ = 0;
    /** counts the number of points more or less in the same area*/
    private static int fixPointsStart_ = 0;
    /** flag true if user is tracing*/
    private static boolean drawingStroke_ = false;
    /** number of ignored points*/
    private static int ignored_ = 0;
    /** last point acquired*/
    private static Point2D lastP_ = null;
    /** last rectangle (BoundingBox) object acquired*/
    private static Rectangle lastRec_;
    /** last point of stroke acquisition timestamp*/
    private static long lastPointTimeStamp_;
    /** first point of stroke acquisition timestamp*/
    private static long firstPointTimeStamp_;
    /** list of acquired points to build a stroke*/
    private static LinkedList<Point2D> objList_;

    private BufferedImage liveImage_;
    private BufferedImage processedImage_;
    private VideoUpdateThread videoUpdateThread_;
    private ArrayList imageListeners = new ArrayList();
    private ArrayList boundingBoxListeners = new ArrayList();
    private int minRed_ = 0;
    private int minGreen_ = 0;
    private int minBlue_ = 0;
    private int maxRed_ = 0;
    private int maxGreen_ = 0;
    private int maxBlue_ = 0;
    //dim della bounding box selezionata per la ricerca di regioni
    private int distX_ = 0;
    private int distY_ = 0;
    //
    private BoundingBox objectBoundingBox = new BoundingBox();

    ////////////////////////////////////////////////////
    // CONSTRUCTOR
    ////////////////////////////////////////////////////
    /**
     * Creates a new Image Processor.
     */
    public ImageProcessor() {
        liveImage_ = new BufferedImage(416, 312, BufferedImage.TYPE_INT_ARGB);
        processedImage_ = new BufferedImage(liveImage_.getWidth(), liveImage_.getHeight(), BufferedImage.TYPE_INT_ARGB);
    }

    ////////////////////////////////////////////////////
    // METHODS
    ////////////////////////////////////////////////////
    /**
     * Shutsdown the Video Update Thread.
     */
    public void shutdown() {
        videoUpdateThread_.shutdown();
    }

    /**
     * Returns the live image from the video capture devive.
     *
     * @return live image from the video capture devive.
     */
    public BufferedImage getLiveImage() {
        return liveImage_;
    }

    /**
     * Returns the processed image.
     *
     * @return the processed image.
     */
    public BufferedImage getProcessedImage() {
        return processedImage_;
    }

    /**
     * Returns the bounding box of the tracking object
     *
     * @return the bounding box
     */
    public BoundingBox getObjectBoundingBox() {
        return objectBoundingBox;
    }

    /**
     * Returns the max red value.
     *
     * @return Returns the max red value.
     */
    public int getMaxRed() {
        return maxRed_;
    }

    /**
     * Sets the maximum red channel value for the color to track
     *
     * @param maxRed The max Red to set.
     * @throws IllegalArgumentException
     */
    public void setMaxRed(int maxRed) throws IllegalArgumentException {
        if (maxRed >= 0 && maxRed <= 255) {
            this.maxRed_ = maxRed;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the minimum red channel value for the color to track
     *
     * @return Returns the minRed.
     */
    public int getMinRed() {
        return minRed_;
    }

    /**
     * Sets the minimum red channel value for the color to track
     *
     * @param minRed The minRed to set.
     * @throws IllegalArgumentException
     */
    public void setMinRed(int minRed) throws IllegalArgumentException {
        if (minRed >= 0 && minRed <= 255) {
            this.minRed_ = minRed;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the maximum blue channel value for the color to track
     *
     * @return Returns the maxBlue.
     */
    public int getMaxBlue() {
        return maxBlue_;
    }

    /**
     * Sets the maximum blue channel value for the color to track
     *
     * @param maxBlue The maxBlue to set.
     * @throws IllegalArgumentException
     */
    public void setMaxBlue(int maxBlue) throws IllegalArgumentException {
        if (maxBlue >= 0 && maxBlue <= 255) {
            this.maxBlue_ = maxBlue;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the minimum blue channel value for the color to track
     *
     * @return Returns the min blue.
     */
    public int getMinBlue() {
        return minBlue_;
    }

    /**
     * Sets minimum blue channel value for the color to track
     *
     * @param minBlue The min Blue to set.
     * @throws IllegalArgumentException
     */
    public void setMinBlue(int minBlue) throws IllegalArgumentException {
        if (minBlue >= 0 && minBlue <= 255) {
            this.minBlue_ = minBlue;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns maximum green channel value for the color to track
     *
     * @return Returns the max Green.
     */
    public int getMaxGreen() {
        return maxGreen_;
    }

    /**
     * Sets the maximum green channel value for the color to track
     *
     * @param maxGreen The max Green to set.
     * @throws IllegalArgumentException
     */
    public void setMaxGreen(int maxGreen) throws IllegalArgumentException {
        if (maxGreen >= 0 && maxGreen <= 255) {
            this.maxGreen_ = maxGreen;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the minimum green channel value for the color to track
     *
     * @return Returns the min green.
     */
    public int getMinGreen() {
        return minGreen_;
    }

    /**
     * Sets the minimum green channel value for the color to track
     *
     * @param minGreen The min green to set.
     * @throws IllegalArgumentException
     */
    public void setMinGreen(int minGreen) throws IllegalArgumentException {
        if (minGreen >= 0 && minGreen <= 255) {
            this.minGreen_ = minGreen;
        } else {
            throw new IllegalArgumentException();
        }
    }

    ////////////////////////////////////////////////////
    // LISTENER METHODS
    ////////////////////////////////////////////////////
    /**
     * Listener method
     *
     * @param listener
     */
    public void addImageUpdateListener(ImageUpdateListener listener) {
        imageListeners.add(listener);
    }

    /**
     * Listener method
     *
     * @param listener
     */
    public void removeImageUpdateListener(ImageUpdateListener listener) {
        imageListeners.remove(listener);
    }

    /**
     * Listener method
     */
    public void fireImageUpdateEvent() {
        for (int i = 0; i < imageListeners.size(); i++) {
            ((ImageUpdateListener) imageListeners.get(i)).imagesUpdated();
        }
    }

    /**
     * Listener method
     *
     * @param listener
     */
    public void addBoundingBoxUpdateListener(BoundingBoxUpdateListener listener) {
        boundingBoxListeners.add(listener);
    }

    /**
     * Listener method
     *
     * @param listener
     */
    public void removeBoundingBoxUpdateListener(BoundingBoxUpdateListener listener) {
        boundingBoxListeners.remove(listener);
    }

    /**
     * Listener method
     *
     * @param listener
     */
    private void fireBoundingBoxUpdateEvent() {
        for (int i = 0; i < boundingBoxListeners.size(); i++) {
            ((BoundingBoxUpdateListener) boundingBoxListeners.get(i)).boundingBoxUpdated();
        }
    }

    /**
     * Image processing method. Code by Alex Dafinei
     * Credits to David Bull for inspiration
     */
    public void processImageGS() {


        int width = liveImage_.getWidth(); //Image Width
        int height = liveImage_.getHeight(); //Image Height
        int scan = 1;
        int ct = 30;
        int[][] regionMap = new int[height][width];
        //System.out.println("Process....");

        short[][] matrix = new short[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                matrix[i][j] = 0;
            }
        }
        boolean larg_reg = Config.getInstance().isLARGEST_REGION();

        ArrayList regionEquivalenceTree = new ArrayList();
        short numAbove = 0, numLeft = 0, numCurrent = 0, regionIndex = 0;
        double distLeft = 0, distAbove = 0;
        int mapFrom;
        Integer mapTo = new Integer(-1), prevMapTo = new Integer(-1), lowerRegion = new Integer(-1);


        processedImage_ = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB); //Create Overlay

        Color flagColor = Color.YELLOW;
        //calcola il colore selezionato
        int r = (maxRed_ + minRed_) / 2;
        int g = (maxGreen_ + minGreen_) / 2;
        int b = (maxBlue_ + minBlue_) / 2;

        Color tColor = tColor = new Color(r, g, b);
        //int avgT=(tColor.getRed()+tColor.getGreen()+tColor.getBlue())/3;
        int avgT = (int) (tColor.getRed() * 0.299 + tColor.getGreen() * 0.587 + tColor.getBlue() * 0.114);
        for (int x = 0; x < width; x += scan) { //Scan through each X of the image
            for (int y = 0; y < height; y += scan) { //For each X scan through each Y of the image

                Color col = new Color(liveImage_.getRGB(x, y)); //Get the pixel color at the current position
                int greyScale = ((col.getRed() + col.getGreen() + col.getBlue()) / 3); //Generate the GrayScale
                //Color Detection
                processedImage_.setRGB(x, y, Color.BLACK.getRGB());
                if ((avgT - ct) < greyScale && (avgT + ct) > greyScale) { //Is the GrayScale within threshold?
                    processedImage_.setRGB(x, y, Color.GREEN.getRGB()); //If so mark with a semi transparent blue mark

                    //Is each color channel within threshold?
                    if ((tColor.getRed() - ct) < col.getRed() && (tColor.getRed() + ct) > col.getRed()) {
                        if ((tColor.getGreen() - ct) < col.getGreen() && (tColor.getGreen() + ct) > col.getGreen()) {
                            if ((tColor.getBlue() - ct) < col.getBlue() && (tColor.getBlue() + ct) > col.getBlue()) {
                                //processedImage.setRGB(x, y, flagColor.getRGB()); //If R,G, and B Channels are with threshold mark with yellow
                                processedImage_.setRGB(x, y, flagColor.getRGB());
                                matrix[x][y] = 1;
                            }
                        }
                    }
                }

            }
        }
        //Identifica regione
        for (int y = 0; y < height; y++) { //Scan through each Y of the matrix
            for (int x = 0; x < width; x++) {
                if (x != 0) {
                    numLeft = matrix[x - 1][y];
                }
                numCurrent = matrix[x][y];
                if (y != 0) {
                    numAbove = matrix[x][y - 1];
                }

                // calculate distance
                if (x != 0) {
                    if ((numLeft == 0 && numCurrent != 0) || (numLeft != 0 && numCurrent == 0)) {
                        distLeft = 1;
                    } else {
                        distLeft = 0;
                    }
                }
                if (y != 0) {
                    if ((numAbove == 0 && numCurrent != 0) || (numAbove != 0 && numCurrent == 0)) {
                        distAbove = 1;
                    } else {
                        distAbove = 0;
                    }
                }

                // check distances against threshold
                if (x == 0 || y == 0) {
                    if (x != 0 && y == 0) {
                        if (distLeft == 1) // CASE 1
                        {	// current pixel different to neighbour - assign new region
                            regionIndex++;
                            regionMap[y][x] = regionIndex;
                            regionEquivalenceTree.add(new TreeSet());
                        } else // CASE 2
                        {	// current pixel similar to left pixel - assign to same region as left pixel
                            regionMap[y][x] = regionMap[y][x - 1];
                        }
                    } else if (x == 0 && y != 0) {
                        if (distAbove == 1) // CASE 1
                        {	// current pixel different to neighbour - assign new region
                            regionIndex++;
                            regionMap[y][x] = regionIndex;
                            regionEquivalenceTree.add(new TreeSet());
                        } else // CASE 3
                        {	// current pixel similar to pixel above - assign to same region as pixel above
                            regionMap[y][x] = regionMap[y - 1][x];
                        }
                    } else if (x == 0 && y == 0) // First pixel in image
                    {	// first pixel in image - assign new region number
                        regionMap[y][x] = regionIndex;
                        regionEquivalenceTree.add(new TreeSet());
                    }
                } else {
                    if ((distLeft == 1) && (distAbove == 1)) // CASE 1
                    {	// current pixel different to neighbours - assign new region
                        // also add new region to equivalence map and make equivalent to itself
                        regionIndex++;
                        regionMap[y][x] = regionIndex;
                        regionEquivalenceTree.add(new TreeSet());
                    } else if ((distLeft == 0) && (distAbove == 1)) // CASE 2
                    {	// current pixel similar to left pixel - assign to same region as left pixel
                        regionMap[y][x] = regionMap[y][x - 1];
                    } else if ((distLeft == 1) && (distAbove == 0)) // CASE 3
                    {	// current pixel similar to pixel above - assign to same region as pixel above
                        regionMap[y][x] = regionMap[y - 1][x];
                    } else if ((distLeft == 0) && (distAbove == 0)) // CASE 4
                    {	// pixel similar to both neighbours - assign to same region as neighbours
                        // if neighbours have different region numbers then add to region equivalence map
                        if (regionMap[y][x - 1] != regionMap[y - 1][x]) {	// make equivalence pointer point to lower region number
                            if (regionMap[y][x - 1] < regionMap[y - 1][x]) {
                                mapFrom = regionMap[y - 1][x];
                                mapTo = new Integer(regionMap[y][x - 1]);
                            } else {
                                mapFrom = regionMap[y][x - 1];
                                mapTo = new Integer(regionMap[y - 1][x]);
                            }
                            if (mapTo.equals(prevMapTo)) {
                                mapTo = lowerRegion;
                            } else {
                                lowerRegion = new Integer(findLowestEquivalentRegion(regionEquivalenceTree, mapTo.intValue()));
                            }
                            ((TreeSet) (regionEquivalenceTree.get(mapFrom))).add(lowerRegion);
                            prevMapTo = mapTo;
                            regionMap[y][x] = lowerRegion.intValue();
                        } else {
                            regionMap[y][x] = regionMap[y][x - 1];
                        }
                    }
                }

            }
        }

        // STEP 4: Flatten equivalence map
        flatten(regionEquivalenceTree);

        // STEP 5: Re-calculate region map taking into account the region equivalence tree
        for (int y = 0; y < height; y++) // scan rows
        {
            for (int x = 0; x < width; x++) // scan pixels in current row
            {
                TreeSet region = (TreeSet) (regionEquivalenceTree.get(regionMap[y][x]));
                if (region.size() == 1) {
                    regionMap[y][x] = ((Integer) (region.first())).intValue();
                }
            }
        }

        // STEP 6: Count regions
        int regionCount = 0;
        for (int j = 0; j < regionEquivalenceTree.size(); j++) {
            if (((TreeSet) (regionEquivalenceTree.get(j))).isEmpty()) {
                regionCount++;
            }
        }
        //app.log("Found "+regionCount+" regions");


        if (regionCount > 1) {
            // STEP 7: Find biggest region that isnt 0
            int[] regionSize = new int[regionIndex + 2];
            for (int j = 0; j <= regionIndex; j++) {
                regionSize[j] = 0;
            }
            for (int y = 0; y < height; y++) // scan rows
            {
                for (int x = 0; x < width; x++) // scan pixels in current row
                {
                    if (matrix[x][y] != 0) {
                        regionSize[regionMap[y][x]]++;
                    }
                }
            }
            int largestRegion = 0;
            int maxSize = 0;
            if (larg_reg) {
                for (int j = 0; j <= regionIndex; j++) {
                    if (regionSize[j] > maxSize) {
                        maxSize = regionSize[j];
                        largestRegion = j;
                    }
                }
                //System.out.println("[DEBUG]: Largest region size:" + maxSize + "Select size: " + distX * distY);
            } else {
                AREA_THRESHOLD_MIN = (distX_ * distY_) / 3;
                AREA_THRESHOLD_MAX = (distX_ * distY_) * 3;
                for (int j = 0; j <= regionIndex; j++) {
                    if (distX_ > 0 && distY_ > 0) {
                        if (regionSize[j] >= (AREA_THRESHOLD_MIN) & regionSize[j] < AREA_THRESHOLD_MAX) {
                            maxSize = regionSize[j];
                            largestRegion = j;
                            break;
                        }
                    }
                }
                //System.out.println("[DEBUG]: Largest region is "+largestRegion+" containing "+maxSize+" pixels.");
            }



            // STEP 8: Now find the bounding coords for the largest region
            int X1 = width, Y1 = height, X2 = 0, Y2 = 0;
            for (int y = 0; y < height; y++) // scan rows
            {
                for (int x = 0; x < width; x++) // scan pixels in current row
                {
                    if (regionMap[y][x] == largestRegion) {
                        if (x < X1) {
                            X1 = x;
                        }
                        if (x > X2) {
                            X2 = x;
                        }
                        if (y < Y1) {
                            Y1 = y;
                        }
                        if (y > Y2) {
                            Y2 = y;
                        }
                    }
                }
            }


            if (X1 != objectBoundingBox.getX1() || Y1 != objectBoundingBox.getY1() || X2 != objectBoundingBox.getX2() || Y2 != objectBoundingBox.getY2()) {
                objectBoundingBox.setCoords(X1, Y1, X2, Y2);

                //PanelAccessor.getOutputText().append("Found object! Bounding coords are: " + X1 + "," + Y1 + "," + X2 + "," + Y2 + "\n");
                Rectangle rec = new Rectangle(X1, Y1, X2 - X1, Y2 - Y1);
                if (X1 != 0 && Y1 != 0 && X2 != 0 && Y2 != 0) {

                    if (buildTrajectory(rec)) {
                        //System.out.println("[DEBUG]: Trajectory built...");
                        Stroke stroke = new Stroke(objList_.getFirst(), objList_.getLast(), objList_, new TimeInterval(0, lastPointTimeStamp_ - firstPointTimeStamp_));
                        PanelAccessor.getResultPanel().drawStroke(stroke, Color.RED);
                        if (objList_.size() >= THRESHOLD_LIST_SIZE) {
                            new RecognizerManager().recognize(stroke);
                            //System.out.println("[DEBUG]: Stroke drawn: "+objList.size());
                        }
                        initTrajectoryVars();
                    }
                }
                fireBoundingBoxUpdateEvent();
            }

            // STEP 9: Draw bounding box
            Graphics gr = processedImage_.getGraphics();
            gr.setColor(BOX_COLOR);

            gr.drawRect(X1, Y1, X2 - X1, Y2 - Y1);
            gr.drawLine(((X2 - X1) / 2) + X1, ((Y2 - Y1) / 2) + Y1 - 5, ((X2 - X1) / 2) + X1, ((Y2 - Y1) / 2) + Y1 + 5);
            gr.drawLine(((X2 - X1) / 2) + X1 - 5, ((Y2 - Y1) / 2) + Y1, ((X2 - X1) / 2) + X1 + 5, ((Y2 - Y1) / 2) + Y1);



        } else {
            if (objList_ != null) {
                PanelAccessor.getOutputText().append("Object lost \n");
                //si aumentano i punti ignorati
                ignored_++;
                //se per caso l'oggetto esce dal campo della webcam e lo stroke non
                //era ancora finito, si fa un ulteriore controllo ed eventualmente
                //si costruisce lo stroke.
                if (ignored_ > IGNORED_POINTS_THRESHOLD) {
                    if (objList_.size() >= THRESHOLD_LIST_SIZE) {
                        Stroke stroke = new Stroke(objList_.getFirst(), objList_.getLast(), objList_, new TimeInterval(0, lastPointTimeStamp_ - firstPointTimeStamp_));
                        PanelAccessor.getResultPanel().drawStroke(stroke, Color.RED);
                        new RecognizerManager().recognize(stroke);
                    }
                    initTrajectoryVars();

                }
            }
            if (objectBoundingBox.getX1() != 0 || objectBoundingBox.getY1() != 0 || objectBoundingBox.getX2() != 0 || objectBoundingBox.getX2() != 0) {
                //l'oggetto e' stato perso, si ignorano dei punti, ma non si resetta lo stroke,
                //poiche potrebbe comparire di nuovo al frame successivo
                objectBoundingBox.setCoords(0, 0, 0, 0);
                fireBoundingBoxUpdateEvent();
            }
        }
    }

    /**
     * <p>
     * Flattens a region equivalence tree.
     * Starting at the highest region, a call to <code>findLowestEquivalentRegion()</code> is made to find the
     * lowest region number that it is equivalent to. This is then propogated down the path in the tree from the
     * current region number to the lowest.
     * </p><p>
     * E.g. If 8 points to 6, which points to 4 points to 2, then starting at 8 its lowest equivalent region is 2.
     * Propogating this down the path sets 6 to 2 and then 4 to 2.
     * </p>
     * @param tree
     */
    private static void flatten(ArrayList tree) {
        for (int i = tree.size() - 1; i >= 0; i--) {
            int lowestEquivalentRegion = findLowestEquivalentRegion(tree, i);
            propogateLowestRegion(tree, i, lowestEquivalentRegion);

        }
    }

    /**
     * Given a found object, decides based on a size threshold and on spatial information
     * if the found object is relevant to build a trajectory for a stroke, or not
     */
    private boolean buildTrajectory(Rectangle rect) {
        Rectangle tempRec = lastRec_;
        lastRec_ = rect;
        //recupera i punti tracciati
        Point2D p = new Point2D.Double(rect.getCenterX(), rect.getCenterY());
        Point2D tempP = lastP_;
        lastP_ = p;

        if (tempRec == null || tempP == null) {
            //primo punto, esce
            return false;
        }
        //se la lista di punti e' null, la crea
        if (objList_ == null) {
            objList_ = new LinkedList<Point2D>();
        }
        //calcolo soglie distanza fra punti: la minima e' pari a 1/4
        int dtMin = (int) (rect.getHeight() + rect.getWidth()) / 4;
        dtMin = dtMin >= DIST_THRESHOLD_MIN ? dtMin : DIST_THRESHOLD_MIN;
        //la massima e'pari al lato piu grande della bounding box
        int dtMax = 2 * (int) Math.max(rect.getHeight(), rect.getWidth());
        dtMax = dtMax >= DIST_THRESHOLD_MAX ? dtMax : DIST_THRESHOLD_MAX;
        int distP = (int) p.distance(tempP);

        if (distP <= dtMax) {
            if (rect.contains(tempP) || distP <= dtMin) {
              //if (rect.contains(tempP)) {
                //Punto fisso
                PanelAccessor.getResultPanel().drawFreePoints(p, tempP, Color.GREEN);
                //l'oggetto e' fisso
                if (drawingStroke_) {
                    fixPointsEnd_++;
                    if (fixPointsEnd_ > THRESHOLD_FIX_POINTS_NUMBER) {
                        //System.out.println("fixPointsEnd_>THRESHOLD_FIX_POINTS_NUMBER");
                        drawingStroke_ = false;
                        fixPointsEnd_ = 0;
                        fixPointsStart_ = 0;
                        //System.out.println("[DEBUG]: Stopped drawing");
                        //timestamp punto e inserimento in lista
                        lastPointTimeStamp_ = System.currentTimeMillis();
                        objList_.add(p);
                        ignored_=0;
                        return true;
                    }
                    objList_.add(p);
                    ignored_=0;
                } else {
                    fixPointsStart_++;
                }
            }
            if (distP >= dtMin && distP <= dtMax) {

                //System.out.println("p.distance(tempP)>DISTANCE_THRESHOLD_MIN&&p.distance(tempP)<=DISTANCE_THRESHOLD_MAX");
                if (fixPointsStart_ > THRESHOLD_FIX_POINTS_NUMBER) {
                    if (!drawingStroke_) {
                        //timestamp punto e inserimento in lista
                        System.out.println("[DEBUG]: Started drawing");
                        drawingStroke_ = true;
                        firstPointTimeStamp_ = System.currentTimeMillis();
                        //si inizia un nuovo disegno, pulisce i panelli
                        PanelAccessor.getResultPanel().cleanScreen();
                        PanelAccessor.getSvgPanel().drawBeautifiedSymbol("UNKNOWN", 0, 0, 0, 0);
                        objList_.clear();

                    }
                    fixPointsEnd_ = 0; //per evitare che si fermi se l'utente rallenta
                    //System.out.println("fixPointsStart_>THRESHOLD_FIX_POINTS_NUMBER");
                    PanelAccessor.getResultPanel().drawFreePoints(p, tempP, Color.GREEN);
                    objList_.add(p);
                    ignored_=0;

                }
            }
            //System.out.println("[DEBUG]: Rettangoli si intersecano");
        } else {
            //non si intersecano, tracciamento veloce potrebbe darsi che l'utente
            //non stia piu tracciando
            ignored_++;
            if (ignored_ > IGNORED_POINTS_THRESHOLD) {
                if (objList_.size() > THRESHOLD_LIST_SIZE) {
                    //si è preso uno stroke, ma l'oggetto da seguire e' scomparso prima
                    //dell'accettazione di questo
                    lastPointTimeStamp_ = System.currentTimeMillis();
                    return true;
                } else {
                    //non si è preso lo stroke, si ricomincia
                    initTrajectoryVars();
                }

            }
        }
        return false;
    }

    /**
     * Given a region number, this method finds the lowest equivalent region number by recursively following
     * the path of equivalent regions down the tree.
     * @param tree equivalent region
     * @param region the starting region.
     * @return the lowest equivalent region.
     */
    private static int findLowestEquivalentRegion(ArrayList tree, int region) {
        TreeSet equivalentRegions = (TreeSet) (tree.get(region));


        if (equivalentRegions.size() > 0) {
            TreeSet lowestEquivalentRegions = new TreeSet();
            Iterator it = equivalentRegions.iterator();


            while (it.hasNext()) {
                int r = ((Integer) (it.next())).intValue();
                lowestEquivalentRegions.add(new Integer(findLowestEquivalentRegion(tree, r)));


            }
            return ((Integer) (lowestEquivalentRegions.first())).intValue();


        } else {
            return region;


        }
    }

    /**
     * Given a region number, and it's lowest equivalent region, this method propogates the lowest
     * equivalent region number down the path from the starting region number to the lowest equivalent
     * region number.
     * @param tree equivalent region.
     * @param start the starting region number.
     * @param lowestEquivalentRegion the lowest equivalent region number.
     */
    private static void propogateLowestRegion(ArrayList tree, int start, int lowestEquivalentRegion) {
        TreeSet regions = (TreeSet) (tree.get(start));


        if (regions.size() > 0) {
            Iterator it = regions.iterator();


            while (it.hasNext()) {
                int r = ((Integer) (it.next())).intValue();
                propogateLowestRegion(
                        tree, r, lowestEquivalentRegion);
            }
            regions.clear();
            regions.add(new Integer(lowestEquivalentRegion));


        } else if (start != lowestEquivalentRegion) {
            regions.add(new Integer(lowestEquivalentRegion));


        }
    }

    /**
     * @param liveImage the live Image to set
     */
    public void setLiveImage(BufferedImage liveImage) {
        this.liveImage_ = liveImage;


    }

    /**
     * @param processedImage the processed Image to set
     */
    public void setProcessedImage(BufferedImage processedImage) {
        this.processedImage_ = processedImage;


    }

    /**
     *
     * Returns a List of strokes identified in video input mode
     * Actually multistroke is not supported in video mode
     *
     * @return a list of strokes
     */
    public java.util.List<Stroke> getStrokes() {
        throw new UnsupportedOperationException("Not supported yet.");


    }

    /**
     * Clears a list of strokes identified in video input mode
     * Actually multistroke is not supported in video mode
     */
    public void clearStrokes() {
        throw new UnsupportedOperationException("Not supported yet.");


    }

    /**
     * Returns a distance parameter for a bounding box size; this distance
     * is used for identifying the most similar region of the size selected by the
     * user when coosing the color
     *
     * @return the dist X
     */
    public int getDistX() {
        return distX_;


    }

    /**
     * Sets a distance parameter for a bounding box size; this distance
     * is used for identifying the most similar region of the size selected by the
     * user when coosing the color
     *
     * @param distX the distX to set
     */
    public void setDistX(int distX) {
        this.distX_ = distX;


    }

    /**
     * Returns a distance parameter for a bounding box size; this distance
     * is used for identifying the most similar region of the size selected by the
     * user when coosing the color
     *
     * @return the distY
     */
    public int getDistY() {
        return distY_;
    }

    /**
     * Sets a distance parameter for a bounding box size; this distance
     * is used for identifying the most similar region of the size selected by the
     * user when coosing the color
     *
     * @param distY the distY to set
     */
    public void setDistY(int distY) {
        this.distY_ = distY;
    }

    /**
     * Reinitialise all data structures
     */
    public void initTrajectoryVars() {
        objList_ = null;
        fixPointsStart_ = 0;
        fixPointsEnd_ = 0;
        ignored_ = 0;

    }

    ////////////////////////////////////////////////////
    // INNER-CLASSES
    ////////////////////////////////////////////////////
    /**
     * Thread to grab images from the video capture device and process the image for colour tracking.
     */
    private class VideoUpdateThread extends Thread {

        private boolean running = false;

        public VideoUpdateThread() {
        }

        @Override
        public void run() {
            setPriority(Thread.MAX_PRIORITY);
            running = true;
            while (running) {
                processImageGS();
                fireImageUpdateEvent();
                Thread.yield();
            }
        }

        public void shutdown() {
            running = false;
        }
    }

    /*
     * Stops the tracking thread
     */
    public void stopTracking() {
        videoUpdateThread_.shutdown();
    }

    /**
     * Starts the tracking thread
     */
    public void startTracking() {
        videoUpdateThread_ = new VideoUpdateThread();
        videoUpdateThread_.start();
    }
}

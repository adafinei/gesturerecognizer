package it.uniroma1.dipinfo.sketch.tracking;
/**
 * Image Update Listener
 * 
 * @author David Bull
 * @version 1.0, 21/03/2004
 */
public interface ImageUpdateListener 
{
	/**
	 * Notices an update of the image
	 */
	public void imagesUpdated();
}

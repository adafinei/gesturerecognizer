package it.uniroma1.dipinfo.sketch.tracking;

/**
 * A class to represent the bounding box of the objectr being tracked.
 *
 * @author Alex Dafinei based on code by David Bull; added methods for calculating
 * the area of the bounding box.
 * @version 1.0, 23/05/2011
 */
public class BoundingBox {
    ////////////////////////////////////////////////////
    // CONSTANTS AND CLASS VARIABLES
    ////////////////////////////////////////////////////

    private int X1 = 0, Y1 = 0, X2 = 0, Y2 = 0;

    ////////////////////////////////////////////////////
    // CONSTRUCTORS
    ////////////////////////////////////////////////////
    /**
     *
     */
    public BoundingBox() {
    }

    /**
     * A bounding box constructor
     *
     * @param X1
     * @param Y1
     * @param X2
     * @param Y2
     * @throws IllegalArgumentException
     */
    public BoundingBox(int X1, int Y1, int X2, int Y2) throws IllegalArgumentException {
        if (X1 <= X2 && Y1 <= Y2) {
            this.X1 = X1;
            this.Y1 = Y1;
            this.X2 = X2;
            this.Y2 = Y2;
        } else {
            throw new IllegalArgumentException();
        }
    }

    ////////////////////////////////////////////////////
    // METHODS
    ////////////////////////////////////////////////////
    /**
     * Accessor method
     *
     * @return the x1 coordinate
     */
    public int getX1() {
        return X1;
    }

    /**
     * Accessor method
     *
     * @return the y1 coordinate
     */
    public int getY1() {
        return Y1;
    }

    /**
     * Accessor method
     *
     * @return the x2 coordinate
     */
    public int getX2() {
        return X2;
    }

    /**
     * Accessor method
     * @return the y2 coordinate
     */
    public int getY2() {
        return Y2;
    }

    /**
     * Gets the center of the bounding box
     * @return - the center of the bounding box
     */
    public int[] getCenter() {
        return new int[]{(X2 - X1) / 2, (Y2 - Y1) / 2};
    }

    /**
     * Gets the center coordinates of the bounding box
     * @return - the center of the bounding box absolute coordinates
     */
    public int[] getCenterCoordinates(){

        int[] center=getCenter();
        return new int[]{(X1+center[0]),(Y1+center[1])};
    }


    /**
     * Gets the area of the bounding box
     * @return - the area of the bounding box
     */
    public int getArea(){
        return (X2-X1)*(Y2-Y1);
    }




    /**
     * Checks if boundingbox has size 0
     * @return true if the bounding box has size 0
     */
    public boolean isEmpty() {
        return (X1 == 0 && Y1 == 0 && X2 == 0 && Y2 == 0);
    }

    /**
     * Sets coordinates of the bounding box
     *
     * @param X1
     * @param Y1
     * @param X2
     * @param Y2
     * @throws IllegalArgumentException
     */
    public void setCoords(int X1, int Y1, int X2, int Y2) throws IllegalArgumentException {
        if (X1 <= X2 && Y1 <= Y2) {
            this.X1 = X1;
            this.Y1 = Y1;
            this.X2 = X2;
            this.Y2 = Y2;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Checks if coordinates are equal to another object's coordinates
     *
     * @return true if objects are equal, i.e. bounding boxes have the same coordinates
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof BoundingBox) {
            BoundingBox b = (BoundingBox) o;
            if (b.getX1() != X1) {
                return false;
            }
            if (b.getY1() != Y1) {
                return false;
            }
            if (b.getX2() != X2) {
                return false;
            }
            if (b.getY2() != Y2) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.X1;
        hash = 41 * hash + this.Y1;
        hash = 41 * hash + this.X2;
        hash = 41 * hash + this.Y2;
        return hash;
    }
}

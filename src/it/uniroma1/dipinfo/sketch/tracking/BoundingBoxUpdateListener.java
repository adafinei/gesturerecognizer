package it.uniroma1.dipinfo.sketch.tracking;
/**
 * A listener interface used to notify classes when the bounding box for the object being tracked has been updated.
 *
 * @author David Bull
 * @version 1.0, 21/03/2004
 */
public interface BoundingBoxUpdateListener
{
	/**
	 * Event handler
	 */
	public void boundingBoxUpdated();
}

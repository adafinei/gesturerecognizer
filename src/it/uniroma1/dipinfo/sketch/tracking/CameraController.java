/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma1.dipinfo.sketch.tracking;

import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IContainerFormat;
import com.xuggle.xuggler.IContainerParameters;
import com.xuggle.xuggler.IError;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IRational;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.video.ArgbConverter;
import it.uniroma1.dipinfo.sketch.config.Config;
import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * This class interfaces with Xuggler library in order to acquire input
 * from webcam. The class is a Thread which opens a stream to a device,
 * by using a driver name and a device name. Driver and device can be set up
 * in the <code>it.uniroma1.dipinfo.sketch.config.config.properties</code>
 * properties file.
 *
 * @author Alex Dafinei
 */
public class CameraController extends Thread {

    ImageProcessor imp_;
    IContainer container_;
    IStreamCoder videoCoder_;
    String driverName_ = Config.getInstance().getCAM_DRIVER();
    String deviceName_ = Config.getInstance().getCAM_DEVICE();
    private boolean rotate_ = Config.getInstance().isVERTICAL_ROTATION();
    private boolean mirror_ = Config.getInstance().isMIRROR_INVERT();

    /**
     * Builds a new CameraController object
     *
     * @param imp a reference to the <code>ImageProcessor object</code>
     */
    public CameraController(ImageProcessor imp) {
        imp_ = imp;
    }

    @Override
    public void run() {
        setPriority(Thread.MIN_PRIORITY);
        //Xuggler video acquisition


        // Let's make sure that we can actually convert video pixel formats.
        if (!IVideoResampler.isSupported(IVideoResampler.Feature.FEATURE_COLORSPACECONVERSION)) {
            throw new RuntimeException("you must install the GPL version of Xuggler (with IVideoResampler support) for this demo to work");
        }

        // Create a Xuggler container object
        container_ = IContainer.make();

        // Devices, unlike most files, need to have parameters set in order
        // for Xuggler to know how to configure them.  For a webcam, these
        // parameters make sense
        IContainerParameters params = IContainerParameters.make();

        // The timebase here is used as the camera frame rate
        params.setTimeBase(IRational.make(30, 1));

        // we need to tell the driver what video with and height to use
        params.setVideoWidth(320);
        params.setVideoHeight(240);

        // and finally, we set these parameters on the container_ before opening
        container_.setParameters(params);

        // Tell Xuggler about the device format
        IContainerFormat format = IContainerFormat.make();
        if (format.setInputFormat(driverName_) < 0) {
            throw new IllegalArgumentException("couldn't open webcam device: " + driverName_);
        }

        // Open up the container_
        int retval = container_.open(deviceName_, IContainer.Type.READ, format);
        if (retval < 0) {
            // This little trick converts the non friendly integer return value into
            // a slightly more friendly object to get a human-readable error name
            IError error = IError.make(retval);
            throw new IllegalArgumentException("could not open file: " + deviceName_ + "; Error: " + error.getDescription());
        }

        // query how many streams the call to open found
        int numStreams = container_.getNumStreams();

        // and iterate through the streams to find the first video stream
        int videoStreamId = -1;
        videoCoder_ = null;
        for (int i = 0; i < numStreams; i++) {
            // Find the stream object
            IStream stream = container_.getStream(i);
            // Get the pre-configured decoder that can decode this stream;
            IStreamCoder coder = stream.getStreamCoder();

            if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
                videoStreamId = i;
                videoCoder_ = coder;
                break;
            }
        }
        if (videoStreamId == -1) {
            throw new RuntimeException("could not find video stream in container: " + deviceName_);
        }

        /*
         * Now we have found the video stream in this file.  Let's open up our decoder so it can
         * do work.
         */
        if (videoCoder_.open() < 0) {
            throw new RuntimeException("could not open video decoder for container: " + deviceName_);
        }

        IVideoResampler resampler = null;
        if (videoCoder_.getPixelType() != IPixelFormat.Type.ARGB) {

            // if this stream is not in BGR24, we're going to need to
            // convert it.  The VideoResampler does that for us.
            resampler = IVideoResampler.make(videoCoder_.getWidth(), videoCoder_.getHeight(), IPixelFormat.Type.ARGB,
                    videoCoder_.getWidth(), videoCoder_.getHeight(), videoCoder_.getPixelType());
            if (resampler == null) {
                throw new RuntimeException("could not create color space resampler for: " + deviceName_);
            }
        }

        /*
         * Now, we start walking through the container_ looking at each packet.
         */
        IPacket packet = IPacket.make();
        //while (container_.isOpened()&videoCoder_.isOpen()) {
        while (container_ != null & videoCoder_ != null & container_.readNextPacket(packet) >= 0) {
            //System.out.println("[DEBUG]:opened");

            /*
             * Now we have a packet, let's see if it belongs to our video stream
             */
            if (packet.getStreamIndex() == videoStreamId) {
                /*
                 * We allocate a new picture to get the data out of Xuggler
                 */
                IVideoPicture picture = IVideoPicture.make(videoCoder_.getPixelType(),
                        videoCoder_.getWidth(), videoCoder_.getHeight());

                int offset = 0;
                while (offset < packet.getSize()) {
                    /*
                     * Now, we decode the video, checking for any errors.
                     *
                     */
                    int bytesDecoded = videoCoder_.decodeVideo(picture, packet, offset);
                    if (bytesDecoded < 0) {
                        throw new RuntimeException("got error decoding video in: " + deviceName_);
                    }
                    offset += bytesDecoded;

                    /*
                     * Some decoders will consume data in a packet, but will not be able to construct
                     * a full video picture yet.  Therefore you should always check if you
                     * got a complete picture from the decoder
                     */
                    if (picture.isComplete()) {
                        IVideoPicture newPic = picture;
                        /*
                         * If the resampler is not null, that means we didn't get the video in BGR24 format and
                         * need to convert it into BGR24 format.
                         */
                        if (resampler != null) {
                            // we must resample
                            newPic = IVideoPicture.make(resampler.getOutputPixelFormat(), picture.getWidth(), picture.getHeight());
                            if (resampler.resample(newPic, picture) < 0) {
                                throw new RuntimeException("could not resample video from: " + deviceName_);
                            }
                        }
                        if (newPic.getPixelType() != IPixelFormat.Type.ARGB) {
                            throw new RuntimeException("could not decode video as BGR 24 bit data in: " + deviceName_);
                        }

                        ArgbConverter rgb = new ArgbConverter(IPixelFormat.Type.ARGB, 320, 240, 416, 312);
                        //newPic.
                        //Convert
                        BufferedImage javaImage = rgb.toImage(newPic);
                        BufferedImage javaDest = new BufferedImage(416, 312, BufferedImage.TYPE_INT_ARGB);
                        //Blur
                        float data[] = {0.0625f, 0.125f, 0.0625f, 0.125f, 0.25f, 0.125f, 0.0625f, 0.125f, 0.0625f};
                        Kernel kernel = new Kernel(3, 3, data);
                        ConvolveOp convolve = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
                        convolve.filter(javaImage, javaDest);
                        javaImage = javaDest;
                        javaDest = null;

                        if (isRotate_() || isMirror_()) {
                            //Transform
                            AffineTransform tx = new AffineTransform();
                            if (isRotate_()) {
                                tx.rotate(Math.PI, javaImage.getWidth() / 2, javaImage.getHeight() / 2);
                            }
                            if (isMirror_()) {
                                
                                //traslo rispetto al centro, poi lo rimetto a posto
                                tx.translate(javaImage.getWidth() / 2.0, javaImage.getHeight() / 2.0);
                                tx.scale(-1.0, 1.0);
                                tx.translate(-javaImage.getWidth() / 2.0, -javaImage.getHeight() / 2.0);
                            }
                            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
                            javaImage = op.filter(javaImage, null);
                        }

                        PanelAccessor.getLiveImage().setImage(javaImage);
                        //Il settaggio della live image si fa su un metodo synchronized, cosi come
                        //la lettura dell'imagine da parte di ImageProcessor
                        imp_.setLiveImage(javaImage);
                        Thread.yield();
//                        try {
//                            sleep(5);
//                        } catch (InterruptedException ex) {
//                            System.out.println(ex.toString());
//                        }
                    }
                }
            } else {
                /*
                 * This packet isn't part of our video stream, so we just silently drop it.
                 */
                do {
                } while (false);
            }

        }
    }

    /**
     * Closes the video acquisition process; for a bug in Xuggle this must
     * be closed when closing the whole app otherwise the JVM crashes
     */
    public void shutdown() {
        if (videoCoder_ != null) {
            System.out.println("[DEBUG]: Closing VideoCoder");

            videoCoder_.close();
            videoCoder_ = null;
        }
        if (container_ != null) {
            System.out.println("[DEBUG]: Closing Container");
            container_.close();
            container_ = null;
        }
    }

    /**
     * Return the rotate state of the image
     *
     * @return the rotate_
     */
    public boolean isRotate_() {
        return rotate_;
    }

    /**
     * Sets the rotate state
     *
     * @param rotate_ the rotate_ to set
     */
    public void setRotate_(boolean rotate_) {
        this.rotate_ = rotate_;
    }

    /**
     * Returns the mirror state of the image
     *
     * @return the mirror_
     */
    public boolean isMirror_() {
        return mirror_;
    }

    /**
     * Sets the mirror state of the image
     *
     * @param mirror_ the mirror_ to set
     */
    public void setMirror_(boolean mirror_) {
        this.mirror_ = mirror_;
    }
}

package it.uniroma1.dipinfo.sketch.recognizer.agents;

import it.uniroma1.dipinfo.sketch.dao.Dao;
import it.uniroma1.dipinfo.sketch.structures.Stroke;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is an abstract class from which the other Recognizers extend
 *
 * @author Alex Dafinei
 */
public abstract class BaseRecognizer extends Thread
        implements Comparable<BaseRecognizer> {

    /**A map of features which the recognizer accesses. It is set before the recognition process starts */
    protected Map features_;
    /**The stroke to recognize */
    protected Stroke stroke_;
    /**The list of strokes to recognize */
    protected List<Stroke> strokeList_;
    /**The probability/certainty degree of recognition */
    protected int correspondency_;
    /**The recognized symbol: it is an Enum of String */
    protected Enum recognizedSymbol_;
    /**The name of the recognizer */
    protected String recognizerName_;
    /**The name of the symbol with which there's a recognition conflict */
    protected String conflictSymbol_;
    /**States whether in conflict with other recognizers or not */
    protected boolean inConflict_ = false;
    /**States if recognition process is finished */
    protected boolean finished_ = false;
    /**States if recognizer is running or not */
    protected boolean running_;
    /**package of agent classes */
    public static final String AGENT_PACKAGE = "it.uniroma1.dipinfo.sketch.recognizer.agents";

    /**An enumeration of all recognizable shapes */
    public enum SHAPE_TYPES {

        CIRCLE,
        ELLIPSE,
        LINE_NORTH_SOUTH,
        LINE_SOUTH_NORTH,
        LINE_EAST_WEST,
        LINE_WEST_EAST,
        RECTANGLE,
        ARROWHEAD_NORTH,
        ARROWHEAD_SOUTH,
        ARROWHEAD_EAST,
        ARROWHEAD_WEST,
        CLOSED_ARC_NORTH,
        CLOSED_ARC_SOUTH,
        CLOSED_ARC_EAST,
        CLOSED_ARC_WEST,
        VERTICAL_INFINITY,
        HORIZONTAL_INFINITY,
        UNKNOWN
    }

    /**
     * Factory class for returning a set of recognizer agents
     */
    public static final class Factory {

        public static List create() {
            List<BaseRecognizer> retShapeRecogn=new LinkedList();
            List agentsCls=Dao.getInstance().getRecognizers();
            Iterator<String> it = agentsCls.iterator();
            try {
                while (it.hasNext()) {
                    //inserisce le classi feature in una MAP
                    String recClass  = it.next();
                    Class c = Class.forName(AGENT_PACKAGE +"."+recClass);
                    retShapeRecogn.add((BaseRecognizer)c.newInstance());

                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(BaseRecognizer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex){
                Logger.getLogger(BaseRecognizer.class.getName()).log(Level.SEVERE, null, ex);
            } catch(IllegalAccessException ex){
                Logger.getLogger(BaseRecognizer.class.getName()).log(Level.SEVERE, null, ex);
            }
            return retShapeRecogn;
        }
    }

    /**
     *
     * @param o - the other Recognizer
     * @return 0 if they have the same certainty degree, 1 if grater, -1 if smaller
     */
    public int compareTo(BaseRecognizer o) {
        if (this.correspondency_ == o.getCorrespondency()) {
            return 0;
        } else if (this.correspondency_ > o.getCorrespondency()) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Sets the features and the stroke; this method should be called
     * before invoking the start() method on the recognizer class which
     * extend this base class
     * @param features - a map of calculated featuree
     * @param stroke - the stroke traced by the user
     */
    public void recognize(Map features, Stroke stroke) {
        this.features_ = features;
        this.stroke_ = stroke;
    }

    /**
     * Sets the features and the strokes (list); this method should be called
     * before invoking the start() method on the recognizer class which 
     * extend this base class
     * @param features - a map of calculated featuree
     * @param strokeList - a list of strokes traced by the user
     */
    public void recognize(Map features, List<Stroke> strokeList) {
        this.features_ = features;
        this.strokeList_ = strokeList;
    }

    /**
     * @return A map of common features based on the stroke's hull
     * properties
     */
    public synchronized Map getFeatures() {
        return features_;
    }

    /**
     * @param features A map of common features based on the stroke's hull
     * properties
     */
    public synchronized void setFeatures(Map features) {
        this.features_ = features;
    }

    /**
     * @return the stroke the user drawed
     */
    public synchronized Stroke getStroke() {
        return stroke_;
    }

    /**
     * @param stroke the stroke the user drawed
     */
    public synchronized void setStroke(Stroke stroke) {
        this.stroke_ = stroke;
    }

    /**
     * @return the percentage of correspondency of the drawn symbol with the
     * recognized symbol
     */
    public synchronized int getCorrespondency() {
        return correspondency_;
    }

    /**
     * @param correspondency  the correspondency to set
     */
    public synchronized void setCorrespondency(int correspondency) {
        this.correspondency_ = correspondency;
    }

    /**
     * @return the recognizedSymbol_
     */
    public synchronized Enum getRecognizedSymbol() {
        return recognizedSymbol_;
    }

    /**
     * @param recognizedSymbol_ the recognizedSymbol_ to set
     */
    public synchronized void setRecognizedSymbol(Enum recognizedSymbol_) {
        this.recognizedSymbol_ = recognizedSymbol_;
    }

    /**
     * Checks if a feature falls within a given interval of values
     * @param feat the feature value
     * @param start start of interval
     * @param end end of interval
     * @return true if the feature falls in the interval
     */
    public boolean isFeatureInInterval(double feat, double start, double end) {
        if (feat >= start && feat <= end) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @deprecated
     * 
     * Checks if a feature falls precesily in the middle of a given interval of values
     * @param feat the feature value
     * @param start start of interval
     * @param end end of interval
     * @param thresholdPercent : percentage of threshold to determine if feat
     * falls precisely into the interval
     * @return true if the feature falls in the interval
     */


    public boolean isFeatureInMiddleOfInterval(double feat, double start, double end, double thresholdPercent) {
        double threshold;
        if (feat >= start && feat <= end) {
            double dist = (Double) Math.abs(start - end);
            threshold = (Double) (dist * thresholdPercent) / 100;
            System.out.println("[DEBUG]: Threshold for middle interval is" + threshold);
            double middle = (Double) dist / 2;
            if (feat >= middle - threshold && feat <= middle + threshold) {
                return true;
            }
        }
        return false;
    }

    /**
     * Marks this recognizer in conflict with an other recognizer
     */
    public void markForConflict(String otherSymbol) {
        this.inConflict_ = true;
        this.conflictSymbol_ = otherSymbol;
        this.finished_ = false;
    }

    /**
     * @param inConflict_ set to true if a conflict is found by the
     * <code>RecognizerManager</code> class
     */
    public void setInConflict(boolean inConflict_) {
        this.inConflict_ = inConflict_;
    }

    /**
     * @return the name of the recognizer
     */
    public synchronized String getRecognizerName() {
        return recognizerName_;
    }

    /**
     * @return the conflictSymbol_ name of the symbol with which this
     * thread is in conflict
     */
    public String getConflictSymbol() {
        return conflictSymbol_;
    }

    /**
     * @return true if this agent is in conflict
     */
    public boolean isInConflict() {
        return inConflict_;
    }

    /**
     * @return true if this thread has finished execution
     */
    public boolean isFinished() {
        return finished_;
    }

    /**
     * @param finished true if this thread has finished execution
     */
    public void setFinished(boolean finished) {
        this.finished_ = finished;
    }

    /**
     * @return the running - true if this thread is running
     */
    public boolean isRunning() {
        return running_;
    }

    /**
     * @param running_ - true if this thread is running
     */
    public void setRunning(boolean running_) {
        this.running_ = running_;
    }

    /**
     * Resets this agent thread
     */
    public void resetAgent() {
        this.recognizedSymbol_ = BaseRecognizer.SHAPE_TYPES.UNKNOWN;
        this.running_ = false;
        this.finished_ = false;
        this.inConflict_ = false;
        this.correspondency_ = 0;
    }
}

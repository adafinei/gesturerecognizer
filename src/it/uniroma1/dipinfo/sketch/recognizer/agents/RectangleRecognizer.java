package it.uniroma1.dipinfo.sketch.recognizer.agents;

import it.uniroma1.dipinfo.sketch.config.Config;
import it.uniroma1.dipinfo.sketch.dao.Dao;
import it.uniroma1.dipinfo.sketch.dao.pojo.SymbolFeatureValues;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeature;

/**
 * This class recognises rectangles
 *
 * @author Alex Dafinei
 */
public class RectangleRecognizer extends BaseRecognizer {

 private int SYMBOL_DET=Config.getInstance().getSYMBOL_DET();
    @Override
    public void run() {
        running_ = true;
        super.recognizerName_ = "RectangleRecognizer";
        this.correspondency_ = 0;
        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
       
        //prende i valori delle features:
        ShapeFeature feat1 = (ShapeFeature) features_.get(ShapeFeature.AlqAer);
        ShapeFeature feat2 = (ShapeFeature) features_.get(ShapeFeature.AchAer);
        ShapeFeature feat3 = (ShapeFeature) features_.get(ShapeFeature.InnerOuterPoints);
        ShapeFeature feat4 = (ShapeFeature) features_.get(ShapeFeature.AlqAch);

        SymbolFeatureValues sfv1 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AlqAer);
        SymbolFeatureValues sfv2 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_,ShapeFeature.AchAer);
        SymbolFeatureValues sfv3 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.InnerOuterPoints);
        SymbolFeatureValues sfv4 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AlqAch);

        while (running_) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }

            if (!inConflict_ && !finished_) {

                if (isFeatureInInterval(feat1.getFeatureValue(), sfv1.getMinValue(),sfv1.getMaxValue())) {
                    correspondency_ += sfv1.getWeight();
                }

                if (isFeatureInInterval(feat2.getFeatureValue(), sfv2.getMinValue(),sfv2.getMaxValue())) {
                    correspondency_ += sfv2.getWeight();
                }

                if (isFeatureInInterval(feat3.getFeatureValue(), sfv3.getMinValue(),sfv3.getMaxValue())) {
                    correspondency_ += sfv3.getWeight();
                }

                if (isFeatureInInterval(feat4.getFeatureValue(), sfv4.getMinValue(),sfv4.getMaxValue())) {
                    correspondency_ += sfv4.getWeight();
                }


                if (correspondency_ >= SYMBOL_DET) {
                    //e' un rettangolo
                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.RECTANGLE);

                }
                
                finished_ = true;
                inConflict_=false;
                System.out.println("[DEBUG]: Rectangle finished");
                
                
            }
            if (inConflict_ && !finished_) {
                //utilizzo altre features 
                //verifica il rapporto dei perimetri
                ShapeFeature cfeat1 = (ShapeFeature) features_.get(ShapeFeature.PerPf);
                double cvalue1 = cfeat1.getFeatureValue();

                ShapeFeature cfeat2 = (ShapeFeature) features_.get(ShapeFeature.AngleRatio);
                double cvalue2 = cfeat2.getFeatureValue();

                if(cvalue1>1)
                    correspondency_-=5;

                if(cvalue2<5)
                    correspondency_-=5;

                if (correspondency_ < SYMBOL_DET) {
                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
                }
                

                finished_ = true;
                inConflict_=false;
            }
        }
    }
}

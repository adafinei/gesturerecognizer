package it.uniroma1.dipinfo.sketch.recognizer.agents;

import it.uniroma1.dipinfo.sketch.config.Config;
import it.uniroma1.dipinfo.sketch.dao.*;
import it.uniroma1.dipinfo.sketch.dao.pojo.*;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeature;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeaturePointingDirection;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeatureTriangleOrient;

/**
 * This class recognises elements from the arrowhead family and their
 * orientation
 *
 * @author Alex Dafinei
 */
public class ArrowRecognizer extends BaseRecognizer {

    private int SYMBOL_DET = Config.getInstance().getSYMBOL_DET();

    @Override
    public void run() {

        running_ = true;
        super.recognizerName_ = "ArrowRecognizer";
        this.correspondency_ = 0;
        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);

        //prende i valori delle features:
        ShapeFeature feat1 = (ShapeFeature) features_.get(ShapeFeature.AltAch);
        ShapeFeature feat2 = (ShapeFeature) features_.get(ShapeFeature.PltPch);
        ShapeFeature feat3 = (ShapeFeature) features_.get(ShapeFeature.ClosedFigure);
        ShapeFeature feat4 = (ShapeFeature) features_.get(ShapeFeature.TriangleOrient);
        ShapeFeature feat5 = (ShapeFeature) features_.get(ShapeFeature.PointingDirection);


        //prende i valori che devono assumere le feature dal database
        SymbolFeatureValues sfv1 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AltAch);
        SymbolFeatureValues sfv2 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.PltPch);
        SymbolFeatureValues sfv3 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.ClosedFigure);
        

        while (running_) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }

            if (!inConflict_ && !finished_) {

                if (isFeatureInInterval(feat1.getFeatureValue(), sfv1.getMinValue(), sfv1.getMaxValue())) {
                    correspondency_ += sfv1.getWeight();
                }

                if (isFeatureInInterval(feat2.getFeatureValue(), sfv2.getMinValue(), sfv2.getMaxValue())) {
                    correspondency_ += sfv2.getWeight();
                }

                if (isFeatureInInterval(feat3.getFeatureValue(), sfv3.getMinValue(), sfv3.getMaxValue())) {
                    correspondency_ += sfv3.getWeight();
                }



                if (feat4.getFeatureValue() != -1 && correspondency_ >= SYMBOL_DET) {
                    if (feat4.getFeatureValue() == ShapeFeatureTriangleOrient.EAST) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_EAST);
                    }

                    if (feat4.getFeatureValue() == ShapeFeatureTriangleOrient.WEST) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_WEST);
                    }

                    if (feat4.getFeatureValue() == ShapeFeatureTriangleOrient.NORTH) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_NORTH);
                    }

                    if (feat4.getFeatureValue() == ShapeFeatureTriangleOrient.SOUTH) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_SOUTH);
                    }
                }else{
                    if (feat5.getFeatureValue() == ShapeFeaturePointingDirection.EAST) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_EAST);
                    }

                    if (feat5.getFeatureValue() == ShapeFeaturePointingDirection.WEST) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_WEST);
                    }

                    if (feat5.getFeatureValue() == ShapeFeaturePointingDirection.NORTH) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_NORTH);
                    }

                    if (feat5.getFeatureValue() == ShapeFeaturePointingDirection.SOUTH) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ARROWHEAD_SOUTH);
                    }
                }

                super.finished_ = true;
                super.inConflict_ = false;
                System.out.println("[DEBUG]: Arrow finished");


            }

            if (inConflict_ && !finished_) {
                //conflitto, verifica altre feature in funzione del simbolo:settate nel codice,
                //a cura dello sviluppatore


                //la freccia puo andare in conflitto con INFINITE, per via di MiddlePoints
                if (conflictSymbol_.equalsIgnoreCase(SHAPE_TYPES.VERTICAL_INFINITY.toString())
                        || conflictSymbol_.equalsIgnoreCase(SHAPE_TYPES.HORIZONTAL_INFINITY.toString())) {
                    ShapeFeature cfeat1 = (ShapeFeature) features_.get(ShapeFeature.AngleRatio);
                    double cvalue1 = cfeat1.getFeatureValue();
                    if (cvalue1 > 4) {
                        correspondency_ -= 15;
                    }
                }


                if (correspondency_ < SYMBOL_DET) {
                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
                }
                super.finished_ = true;
                super.inConflict_ = false;

            }
        }
    }
}

package it.uniroma1.dipinfo.sketch.recognizer.agents;



import it.uniroma1.dipinfo.sketch.dao.Dao;
import it.uniroma1.dipinfo.sketch.dao.pojo.SymbolFeatureValues;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeature;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeatureUtils;

/**
 * This class recognises lines with their orientation and direction
 *
 * @author Alex Dafinei
 */
public class LineRecognizer extends BaseRecognizer {

    @Override
    public void run() {
        super.recognizerName_="LineRecognizer";
        this.correspondency_ = 0;
        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
        //prende i valori delle features:
        ShapeFeature feat1 = (ShapeFeature) features_.get(ShapeFeature.HerWer);
        SymbolFeatureValues sfv1 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.HerWer);


       
        correspondency_ = 0;

        if (isFeatureInInterval(feat1.getFeatureValue(), sfv1.getMinValue(),sfv1.getMaxValue())) {
                    correspondency_ += sfv1.getWeight();
        }
        if (correspondency_ == 100) {

            //e' una linea ne vediamo la direzione
            String dir = ShapeFeatureUtils.getDirection(super.getStroke().getPointsList());
            if (dir.equals(ShapeFeatureUtils.DIRECTION_EAST)) {
                super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.LINE_WEST_EAST);
            }

            if (dir.equals(ShapeFeatureUtils.DIRECTION_WEST)) {
                super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.LINE_EAST_WEST);
            }

            if (dir.equals(ShapeFeatureUtils.DIRECTION_NORTH)) {
                super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.LINE_SOUTH_NORTH);
            }

            if (dir.equals(ShapeFeatureUtils.DIRECTION_SOUTH)) {
                super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.LINE_NORTH_SOUTH);
            }

        } else {
            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
        }
        System.out.println("[DEBUG]: Line finished");
        
        finished_ = true;
        inConflict_ = false;
    }
}

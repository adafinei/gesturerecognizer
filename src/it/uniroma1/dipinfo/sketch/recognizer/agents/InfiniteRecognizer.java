package it.uniroma1.dipinfo.sketch.recognizer.agents;

import it.uniroma1.dipinfo.sketch.config.Config;
import it.uniroma1.dipinfo.sketch.dao.Dao;
import it.uniroma1.dipinfo.sketch.dao.pojo.SymbolFeatureValues;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeature;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeatureUtils;

/**
 * This class recognises infinity symbols.
 *
 * @author Alex Dafinei
 */
public class InfiniteRecognizer extends BaseRecognizer {

    private int SYMBOL_DET=Config.getInstance().getSYMBOL_DET();
    
    @Override
    public void run() {
        running_=true;
        super.recognizerName_ = "InfiniteRecognizer";
        this.correspondency_ = 0;
        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
   
        //prende i valori delle features:
        ShapeFeature feat1 = (ShapeFeature) features_.get(ShapeFeature.AlqAer);
        ShapeFeature feat2 = (ShapeFeature) features_.get(ShapeFeature.Pch2Ach);
        ShapeFeature feat3 = (ShapeFeature) features_.get(ShapeFeature.InnerOuterPoints);
        ShapeFeature feat4 = (ShapeFeature) features_.get(ShapeFeature.PltPch);
        ShapeFeature feat5 = (ShapeFeature) features_.get(ShapeFeature.AltAch);
       
        SymbolFeatureValues sfv1 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AlqAer);
        SymbolFeatureValues sfv2 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_,ShapeFeature.Pch2Ach);
        SymbolFeatureValues sfv3 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.InnerOuterPoints);
        SymbolFeatureValues sfv4 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.PltPch);
        SymbolFeatureValues sfv5 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AltAch);



        while (running_) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }

            if (!inConflict_ && !finished_) {
                if (isFeatureInInterval(feat1.getFeatureValue(), sfv1.getMinValue(),sfv1.getMaxValue())) {
                    correspondency_ += sfv1.getWeight();
                }

                if (isFeatureInInterval(feat2.getFeatureValue(), sfv2.getMinValue(),sfv2.getMaxValue())) {
                    correspondency_ += sfv2.getWeight();
                }

                if (isFeatureInInterval(feat3.getFeatureValue(), sfv3.getMinValue(),sfv3.getMaxValue())) {
                    correspondency_ += sfv3.getWeight();
                }

                if (isFeatureInInterval(feat4.getFeatureValue(), sfv4.getMinValue(),sfv4.getMaxValue())) {
                    correspondency_ += sfv4.getWeight();
                }

                if (isFeatureInInterval(feat5.getFeatureValue(), sfv5.getMinValue(),sfv5.getMaxValue())) {
                    correspondency_ += sfv5.getWeight();
                }
                

                if (correspondency_ >= SYMBOL_DET) {
                    if (ShapeFeatureUtils.findMaxExcursionX(super.stroke_.getPointsList())
                            < ShapeFeatureUtils.findMaxExcursionY(super.stroke_.getPointsList())) {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.VERTICAL_INFINITY);
                    } else {
                        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.HORIZONTAL_INFINITY);
                    }
                }
                System.out.println("[DEBUG]: Infinite finished");
                finished_ = true;
                inConflict_=false;
                
            }

            if (inConflict_ && !super.finished_) {
                //verifica il rapporto dei perimetri
                ShapeFeature cfeat1 = (ShapeFeature) features_.get(ShapeFeature.PerPf);
                double cvalue1 = cfeat1.getFeatureValue();

                ShapeFeature cfeat2 = (ShapeFeature) features_.get(ShapeFeature.AngleRatio);
                double cvalue2 = cfeat2.getFeatureValue();

                if(cvalue1<1)
                    correspondency_-=10;

                if(cvalue2>5)
                    correspondency_-=5;

                ShapeFeature cfeat3 = (ShapeFeature) features_.get(ShapeFeature.AltAch);
                double cvalue3 = cfeat3.getFeatureValue();
                if(conflictSymbol_.toString().startsWith("ARROWHEAD")){
                    if(isFeatureInInterval(cvalue3, 0.55, 0.79))
                        correspondency_-=20;
                }



                if (correspondency_ < SYMBOL_DET) {
                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
                }


                finished_ = true;
                inConflict_ = false;

            }
        }

    }
}

package it.uniroma1.dipinfo.sketch.recognizer.agents;

import it.uniroma1.dipinfo.sketch.dao.Dao;
import it.uniroma1.dipinfo.sketch.dao.pojo.SymbolFeatureValues;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeature;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeatureLineDensity;
import it.uniroma1.dipinfo.sketch.config.Config;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeatureTriangleOrient;

/**
 * This class is aimed to recognize elements of the closed Arc family
 *
 * @author Alex Dafinei
 */
public class ArcRecognizer extends BaseRecognizer {

    private int SYMBOL_DET = Config.getInstance().getSYMBOL_DET();

    /*
     * Launches the recognition process
     */
    @Override
    public void run() {

        running_ = true;
        super.recognizerName_ = "ArcRecognizer";
        this.correspondency_ = 0;
        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
        //feature utilizzate dal riconoscitore
        ShapeFeature feat1 = (ShapeFeature) features_.get(ShapeFeature.AlqAer);
        ShapeFeature feat2 = (ShapeFeature) features_.get(ShapeFeature.InnerOuterPoints);
        ShapeFeature feat3 = (ShapeFeature) features_.get(ShapeFeature.AlqAch);
        ShapeFeature feat4 = (ShapeFeature) features_.get(ShapeFeature.ClosedFigure);
        ShapeFeature feat5 = (ShapeFeature) features_.get(ShapeFeature.LineDensity);
        ShapeFeature feat6 = (ShapeFeature) features_.get(ShapeFeature.HerWer);
        ShapeFeature feat7 = (ShapeFeature) features_.get(ShapeFeature.TriangleOrient);

        //valori che devono assumere le feature per questo riconoscitore
        SymbolFeatureValues sfv1 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AlqAer);
        SymbolFeatureValues sfv2 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.InnerOuterPoints);
        SymbolFeatureValues sfv3 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AlqAch);
        SymbolFeatureValues sfv4 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.ClosedFigure);
        SymbolFeatureValues sfv6 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.HerWer);


        //controllo dei valori assunti dalle feature
        while (running_) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }

            if (!inConflict_ && !finished_) {

                if (isFeatureInInterval(feat1.getFeatureValue(), sfv1.getMinValue(), sfv1.getMaxValue())) {
                    correspondency_ += sfv1.getWeight();
                }

                if (isFeatureInInterval(feat2.getFeatureValue(), sfv2.getMinValue(), sfv2.getMaxValue())) {
                    correspondency_ += sfv2.getWeight();
                }

                if (isFeatureInInterval(feat3.getFeatureValue(), sfv3.getMinValue(), sfv3.getMaxValue())) {
                    correspondency_ += sfv3.getWeight();
                }

                if (isFeatureInInterval(feat4.getFeatureValue(), sfv4.getMinValue(), sfv4.getMaxValue())) {
                    correspondency_ += sfv4.getWeight();
                }

                if (isFeatureInInterval(feat6.getFeatureValue(), sfv6.getMinValue(), sfv6.getMaxValue())) {
                    correspondency_ += sfv6.getWeight();
                }

                if (correspondency_ >= SYMBOL_DET) {
                    //prova a determinare l'orientamento dell'arco in base alla base parallela
                    //con uno degli assi del triangolo iscritto
                    if (feat7.getFeatureValue() != -1) {
                        //System.out.println("[DEBUG]: Triangle feature value "+feat7.getFeatureValue());
                        
                        if (feat7.getFeatureValue() == ShapeFeatureTriangleOrient.NORTH) {
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_NORTH);
                        }else if(feat7.getFeatureValue() == ShapeFeatureTriangleOrient.SOUTH){
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_SOUTH);
                        }else if(feat7.getFeatureValue() == ShapeFeatureTriangleOrient.EAST){
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_EAST);
                        }else if(feat7.getFeatureValue() == ShapeFeatureTriangleOrient.WEST){
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_WEST);
                        }
                        
                    } else {
                        //il primo metodo e' fallito prova a determinare l'orientamento dell'arco
                        //in base a un rapporto di densita di punti nelle varie zone
                        if (feat5.getFeatureValue() == ShapeFeatureLineDensity.EAST) {
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_EAST);
                        } else if (feat5.getFeatureValue() == ShapeFeatureLineDensity.WEST) {
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_WEST);
                        } else if (feat5.getFeatureValue() == ShapeFeatureLineDensity.NORTH) {
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_NORTH);
                        } else if (feat5.getFeatureValue() == ShapeFeatureLineDensity.SOUTH) {
                            super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_SOUTH);
                        } else {
                            //sceglie un' orientameno a caso se anche il secondo metodo fallisce
                            int rand = (int) (4 * Math.random()) + 1;
                            switch (rand) {
                                case 1:
                                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_NORTH);
                                case 2:
                                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_SOUTH);
                                case 3:
                                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_EAST);
                                default:
                                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.CLOSED_ARC_WEST);
                            }
                        }

                    }
                }
                super.finished_ = true;
                super.inConflict_ = false;
                System.out.println("[DEBUG]: Arc finished");

            }

            if (inConflict_ && !finished_) {
                //conflitto, verifica altre feature in funzione del simbolo: questa parte va impostata
                //a mano dallo sviluppatore, non è contemplata sul database per il momento

                //l'arco puo andare in conflitto con ELLIPSE
                if (conflictSymbol_.equalsIgnoreCase(SHAPE_TYPES.ELLIPSE.toString())) {
                    ShapeFeature cfeat1 = (ShapeFeature) features_.get(ShapeFeature.AltAch);
                    ShapeFeature cfeat2 = (ShapeFeature) features_.get(ShapeFeature.PltPch);
                    double cvalue1 = cfeat1.getFeatureValue();
                    double cvalue2 = cfeat2.getFeatureValue();
                    if (!isFeatureInInterval(cvalue1, 0.5, 0.8)) {
                        correspondency_ -= 5;
                    }

                    if (!isFeatureInInterval(cvalue2, 0.90, 0.99)) {
                        correspondency_ -= 5;
                    }
                }
                //l'arco puo andare in conflitto con INFINITE
                if (conflictSymbol_.toString().endsWith("INFINITE")) {
                    ShapeFeature cfeat1 = (ShapeFeature) features_.get(ShapeFeature.AltAch);
                    double cvalue1 = cfeat1.getFeatureValue();
                    if (!isFeatureInInterval(cvalue1, 0.5, 0.8)) {
                        correspondency_ -= 5;
                    }

                }
                //l'arco puo andare in conflitto con RECTANGLE
                if (conflictSymbol_.equalsIgnoreCase(SHAPE_TYPES.RECTANGLE.toString())) {
                    ShapeFeature cfeat1 = (ShapeFeature) features_.get(ShapeFeature.AngleRatio);
                    ShapeFeature cfeat2 = (ShapeFeature) features_.get(ShapeFeature.PerPf);
                    double cvalue1 = cfeat1.getFeatureValue();
                    double cvalue2 = cfeat2.getFeatureValue();
                    if (cvalue1 > 3) {
                        correspondency_ -= 5;
                    }
                    if (cvalue2 < 1.1) {
                        correspondency_ -= 5;
                    }
                }

                if (correspondency_ < SYMBOL_DET) {
                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
                }
                super.finished_ = true;
                super.inConflict_ = false;
            }
        }
    }
}

package it.uniroma1.dipinfo.sketch.recognizer.agents;

import it.uniroma1.dipinfo.sketch.config.Config;
import it.uniroma1.dipinfo.sketch.dao.Dao;
import it.uniroma1.dipinfo.sketch.dao.pojo.SymbolFeatureValues;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeature;

/**
 * This class recognises ellipses
 *
 * @author Alex Dafinei
 */
class EllipseRecognizer extends BaseRecognizer {

    private int SYMBOL_DET = Config.getInstance().getSYMBOL_DET();

    @Override
    public void run() {
        running_ = true;
        super.recognizerName_ = "EllipseRecognizer";
        this.correspondency_ = 0;
        super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);

        //prende i valori delle features:
        ShapeFeature feat1 = (ShapeFeature) features_.get(ShapeFeature.AltAch);
        ShapeFeature feat2 = (ShapeFeature) features_.get(ShapeFeature.AlqAch);
        ShapeFeature feat3 = (ShapeFeature) features_.get(ShapeFeature.InnerOuterPoints);
        ShapeFeature feat4 = (ShapeFeature) features_.get(ShapeFeature.HerWer);


        SymbolFeatureValues sfv1 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AltAch);
        SymbolFeatureValues sfv2 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.AlqAch);
        SymbolFeatureValues sfv3 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.InnerOuterPoints);
        SymbolFeatureValues sfv4 = Dao.getInstance().getFeaturesInfoByRecognizerName(recognizerName_, ShapeFeature.HerWer);

        while (running_) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }

            if (!inConflict_ && !finished_) {
                 if (isFeatureInInterval(feat1.getFeatureValue(), sfv1.getMinValue(),sfv1.getMaxValue())) {
                    correspondency_ += sfv1.getWeight();
                }

                if (isFeatureInInterval(feat2.getFeatureValue(), sfv2.getMinValue(),sfv2.getMaxValue())) {
                    correspondency_ += sfv2.getWeight();
                }

                if (isFeatureInInterval(feat3.getFeatureValue(), sfv3.getMinValue(),sfv3.getMaxValue())) {
                    correspondency_ += sfv3.getWeight();
                }
                //non e' un cerchio
                if (!isFeatureInInterval(feat4.getFeatureValue(), sfv4.getMinValue(),sfv4.getMaxValue())) {
                    correspondency_ += sfv4.getWeight();
                }

                if (correspondency_ >= SYMBOL_DET) {
                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.ELLIPSE);
                }

                super.finished_ = true;
                super.inConflict_ = false;
                System.out.println("[DEBUG]: Ellipse finished");


            }

            if (inConflict_ && !finished_) {
                //verifichiamo se ellipse e' tondeggiante
                ShapeFeature cfeat1 = (ShapeFeature) features_.get(ShapeFeature.AngleRatio);
                double cvalue1 = cfeat1.getFeatureValue();

                ShapeFeature cfeat2 = (ShapeFeature) features_.get(ShapeFeature.AchAer);
                double cvalue2 = cfeat2.getFeatureValue();

                if (conflictSymbol_.contains("CLOSED_ARC")) {
                    ShapeFeature cfeat3 = (ShapeFeature) features_.get(ShapeFeature.HerWer);
                    double cvalue3 = cfeat3.getFeatureValue();
                    if (cvalue3 > 0.65) {
                        correspondency_ -= 3;
                    }

                }

                if (cvalue1 > 4) {
                    correspondency_ -= 5;
                }

                if (!isFeatureInInterval(cvalue2, 0.70, 0.85)) {
                    correspondency_ -= 5;
                }

                if (correspondency_ < SYMBOL_DET) {
                    super.setRecognizedSymbol(BaseRecognizer.SHAPE_TYPES.UNKNOWN);
                }

                super.finished_ = true;
                super.inConflict_ = false;

            }

        }
    }
}

package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import java.util.logging.Logger;

/**
 * This class computes the ratio between the perimeter of a figure
 * and the perimeter of its bounding box
 *
 * @author Alex Dafinei
 */
public class ShapeFeaturePerPf implements ShapeFeature {
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeatureAer.class.getName());
    private double perimeter_;


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {
        this.perimeter_=hullvalues.getEnclosingRectangle().getQuadrilateralPerimeter()/hullvalues.getPerimeter();
        System.out.println("[DEBUG]: Perimeter/ERPerimeter: "+this.perimeter_);
    }

    public double getFeatureValue() {
        return perimeter_;
    }

    public void clearFeatureValue() {
        this.perimeter_=-1;
    }

    public String getFeatureName() {
        return "PERPF";
    }

}

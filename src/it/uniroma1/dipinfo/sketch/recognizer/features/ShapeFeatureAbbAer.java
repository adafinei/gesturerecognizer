package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.List;

/**
 * This class calculates the ratio between a bounding box of the polygon and
 * its encolsing rectangle. Its purpose is mainly to determine how much
 * a figure is drwan in diagonal and not in horizonatal and vertical directions
 *
 * @author Alex Dafinei
 */
public class ShapeFeatureAbbAer implements ShapeFeature {


    private double aBb_aEr_;

    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hull) {
        double aEr = hull.getErDimension().getWidth() * hull.getErDimension().getHeight();
        Polygon p=new Polygon();
        List<Point2D> pl=ShapeFeatureUtils.getBeautifiedPixelList(hull.getPointsList(), 5);
        Iterator<Point2D> it=pl.iterator();
        while(it.hasNext()){
            Point2D pt=it.next();
            p.addPoint((int)pt.getX(), (int)pt.getY());
        }
        Rectangle2D r=p.getBounds2D();
        double aBb=r.getHeight()*r.getHeight();
        aBb_aEr_=aBb/aEr;
    }

    public double getFeatureValue() {
        return aBb_aEr_;
    }

    public String getFeatureName() {
        return "ABBAER";
    }

    public void clearFeatureValue() {
        aBb_aEr_=-1;
    }

}

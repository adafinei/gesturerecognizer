package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;


/**
 * Calculates the ratio between the area of the largest quadrilateral
 * and the area of the enclosing rectangle
 */
class ShapeFeatureAlqAer implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeatureAlqAer.class.getName());
    private double aLq_aEr_;

    /** Creates a new instance of ShapeFeaturePch2Ach */
    public ShapeFeatureAlqAer() {
    }

    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {

        double aLq = hullvalues.getLargestQuad().getQuadrilateralArea();
        double aEr = hullvalues.getErDimension().getWidth() * hullvalues.getErDimension().getHeight();
        aLq_aEr_ = aLq / aEr;
        String outStr = "Alq / Aer = " + Math.round(aLq_aEr_ * 1000) / 1000.0 + " (" + aLq + " / " + aEr + ")";
        CON_.debug("Alq / Aer = " + Math.round(aLq_aEr_ * 1000) / 1000.0 + " (" + aLq + " / " + aEr + ")");
        PanelAccessor.getOutputText().append(outStr + "\n");
    }

    public double getFeatureValue() {
        return aLq_aEr_;
    }

    public void clearFeatureValue() {
        aLq_aEr_ = -1;
    }

    public String getFeatureName() {
        return "ALQAER";
    }
}

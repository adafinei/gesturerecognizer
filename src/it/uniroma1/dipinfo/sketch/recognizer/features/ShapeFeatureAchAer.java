package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;


/**
 * This class calculates the ratio between the area
 * of the convex hull and the area of the enclosing
 * rectangle.
 */
class ShapeFeatureAchAer implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeatureAchAer.class.getName());

    private double aCh_aEr_;


    /** Creates a new instance of ShapeFeaturePch2Ach */
    public ShapeFeatureAchAer() {
    }


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {


        double aCh = hullvalues.getConvexHull().calcConvexHullArea();
        double aEr = hullvalues.getEnclosingRectangle().getDimension().getWidth() * hullvalues.getEnclosingRectangle().getDimension().getHeight();

        aCh_aEr_ = aCh / aEr;
        String outStr="Ach / Aer = " + Math.round(aCh_aEr_ * 1000) / 1000.0 + " (" + aCh + " / " + aEr + ")";
        CON_.debug(outStr);
        PanelAccessor.getOutputText().append(outStr+"\n");
    }


    public double getFeatureValue() {
        return aCh_aEr_;
    }


    public void clearFeatureValue() {
        aCh_aEr_ = -1;
    }

    public String getFeatureName() {
        return "ACHAER";
    }

}

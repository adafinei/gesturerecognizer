package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;

/**
 * This class calculates if a stroke is closed (i.e square or circle)
 * or open (line or arrowhead)
 *
 * @author Alex Dafinei
 */
public class ShapeFeatureClosedFigure implements ShapeFeature {

    private double closed;
    public static double CLOSED=0.0;
    public static double OPEN=1.0;


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hull) {
        boolean closedB=ShapeFeatureUtils.isClosed(hull.getPointsList());
        this.closed=closedB?ShapeFeatureClosedFigure.CLOSED:ShapeFeatureClosedFigure.OPEN;
    }

    public double getFeatureValue() {
        return this.closed;
    }

    public void clearFeatureValue() {
        this.closed=-1;
    }

    public String getFeatureName() {
        return "CLOSEDFIGURE";
    }

}

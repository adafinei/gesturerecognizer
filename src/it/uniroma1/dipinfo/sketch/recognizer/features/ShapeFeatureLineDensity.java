/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import it.uniroma1.dipinfo.sketch.config.Config;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This class calculates based on a horizontal then vertical line sweep
 * over the stroke's points the orientation of a figure based on differences
 * between upper/lower and left/right density of points
 *
 * @author Alex Dafinei
 */
public class ShapeFeatureLineDensity implements ShapeFeature {

    public static double NORTH = 1.0;
    public static double SOUTH = 2.0;
    public static double EAST = 3.0;
    public static double WEST = 4.0;

    private static int PERCENT = Config.getInstance().getLINE_DENSITY_PERCENT();
    private double pointDir_=-1.0;



    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hull) {


        int maxPointsVUp = 0;
        int maxPointsVDown = 0;
        int maxPointsHLeft = 0;
        int maxPointsHRight = 0;

        //calcola la soglia
        List<Point2D> pList = hull.getPointsList();
        int lmi = ShapeFeatureUtils.findLeftmostIndex(pList);
        int rmi = ShapeFeatureUtils.findRightmostIndex(pList);
        int umi = ShapeFeatureUtils.findUpmostIndex(pList);
        int dmi = ShapeFeatureUtils.findDownmostIndex(pList);

        //Le coordinate Y partono dall'alto
        //quindi si prende quello piu piccolo
        Point2D lmp = pList.get(lmi);
        Point2D rmp = pList.get(rmi);
        Point2D ump = pList.get(dmi);
        Point2D dmp = pList.get(umi);

        double lmpx = lmp.getX();
        double rmpx = rmp.getX();
        double umpy = ump.getY();
        double dmpy = dmp.getY();
        //calcolo soglia
        double distX = ShapeFeatureUtils.findMaxExcursionX(pList);
        double distY = ShapeFeatureUtils.findMaxExcursionY(pList);

        int sogliaX = (int) ((distX * PERCENT) / 100);
        int sogliaY = (int) ((distY * PERCENT) / 100);




        List<Density> ld = calcLineDensity(hull);
        Iterator<Density> it = ld.iterator();
        while (it.hasNext()) {

            Density d = it.next();
            if (d.getSweep().equalsIgnoreCase("V")) {
                if (maxPointsVUp <= d.getNumPoints()||maxPointsVDown <= d.getNumPoints() ) {
                    if(d.getCoordinate()>=umpy &&d.getCoordinate()<=umpy+sogliaY*2){
                        maxPointsVUp = maxPointsVUp<d.getNumPoints()?d.getNumPoints():maxPointsVUp;
                    }else if(d.getCoordinate()<=dmpy &&d.getCoordinate()>=dmpy-sogliaY*2){
                        maxPointsVDown = maxPointsVDown<d.getNumPoints()?d.getNumPoints():maxPointsVDown;
                    }
                }
            } else {
                if (maxPointsHLeft < d.getNumPoints() || maxPointsHRight < d.getNumPoints() ) {
                    if(d.getCoordinate()>=lmpx &&d.getCoordinate()<=lmpx+sogliaX*2){
                        maxPointsHLeft = maxPointsHLeft<d.getNumPoints()?d.getNumPoints():maxPointsHLeft;
                    }else if(d.getCoordinate()<=rmpx &&d.getCoordinate()>=rmpx-sogliaX*2){
                        maxPointsHRight = maxPointsHRight<d.getNumPoints()?d.getNumPoints():maxPointsHRight;
                    }
                }
            }
        }
        
//        System.out.println("[DEBUG]: Line density MaxPoints V sweep Up="+maxPointsVUp);
//        System.out.println("[DEBUG]: Line density MaxPoints V sweep Down="+maxPointsVDown);
//        System.out.println("[DEBUG]: Line density MaxPoints H sweep Left="+maxPointsHLeft);
//        System.out.println("[DEBUG]: Line density MaxPoints H sweep Right="+maxPointsHRight);
        
        if (Math.abs(maxPointsHLeft-maxPointsHRight)>Math.abs(maxPointsVUp-maxPointsVDown)) {
            //est/ovest
            if (maxPointsHLeft>maxPointsHRight) {
                this.pointDir_ = ShapeFeatureLineDensity.EAST;
            } else {
                this.pointDir_ = ShapeFeatureLineDensity.WEST;
            }
        } else {
            //nord/sud
            if (maxPointsVUp>maxPointsVDown) {
                this.pointDir_ = ShapeFeatureLineDensity.SOUTH;
            } else{
                this.pointDir_ = ShapeFeatureLineDensity.NORTH;
            }
        }
        System.out.println("[DEBUG]: ShapeFeatureLineDensity value:" + pointDir_);
    }




    public double getFeatureValue() {
        return this.pointDir_;
    }

    private List<Density> calcLineDensity(PolygonValues hull) {
        List<Point2D> pList = hull.getPointsList();

        if (pList.size() > 400) {
            pList = ShapeFeatureUtils.getBeautifiedPixelList(pList, 1);
        }

        int lmi = ShapeFeatureUtils.findLeftmostIndex(pList);
        int rmi = ShapeFeatureUtils.findRightmostIndex(pList);
        int umi = ShapeFeatureUtils.findUpmostIndex(pList);
        int dmi = ShapeFeatureUtils.findDownmostIndex(pList);

        //Le coordinate Y partono dall'alto
        //quindi si prende quello piu piccolo
        Point2D lmp = pList.get(lmi);
        Point2D rmp = pList.get(rmi);
        Point2D ump = pList.get(dmi);
        Point2D dmp = pList.get(umi);

        double lmpx = lmp.getX();
        double rmpx = rmp.getX();
        double umpy = ump.getY();
        double dmpy = dmp.getY();

        Point2D lump = new Point2D.Double(lmpx, umpy);
        Point2D rump = new Point2D.Double(rmpx, umpy);
        Point2D ldmp = new Point2D.Double(lmpx, dmpy);
        //lista che contiene la densita' dei punti
        List<Density> ld = new LinkedList<Density>();

        //passaggio verticale
        Line2D lv = new Line2D.Double((int) lump.getX(), (int) lump.getY(), (int) rump.getX(), (int) rump.getY());

        //si fa un vertical sweep;
        int count = 0;
        for (int i = (int) umpy; i < (int) dmpy; i++) {

            int numPoints = 0;
            for (int j = 0; j < pList.size(); j++) {
                Point2D p = pList.get(j);
                if (lv.ptLineDist(p) < 0.4) {
                    numPoints++;
                }
            }
            count++;
            //la linea scorre giu di 1
            lv = new Line2D.Double((int) lump.getX(), (int) lump.getY() + count, (int) rump.getX(), (int) rump.getY() + count);
            Density d = new Density(i, numPoints, "V");
            ld.add(d);
        }
        //passaggio orizzontale
        count = 0;
        Line2D lh = new Line2D.Double((int) lump.getX(), (int) lump.getY(), (int) ldmp.getX(), (int) ldmp.getY());
        //si fa un horizontal sweep;
        for (int i = (int) lmpx; i < (int) rmpx; i++) {
            int numPoints = 0;
            for (int j = 0; j < pList.size(); j++) {
                Point2D p = pList.get(j);
                if (lh.ptLineDist(p) < 0.5) {
                    numPoints++;
                }
            }
            count++;
            //la linea scorre giu di 1
            lh = new Line2D.Double((int) lump.getX() + count, (int) lump.getY(), (int) ldmp.getX() + count, (int) ldmp.getY());
            Density d = new Density(i, numPoints, "H");
            ld.add(d);
        }
        return ld;
    }

    public String getFeatureName() {
        return "LINEDENSITY";
    }

    public void clearFeatureValue() {
        this.pointDir_ = -1;
    }


    /*
     * Container class for density values.
     */
    private class Density {

        private double coordinate;
        private int numPoints;
        private String sweep;

        public Density(double coordinate, int numPoints, String sweep) {
            this.coordinate = coordinate;
            this.numPoints = numPoints;
            this.sweep = sweep;
        }

        /**
         * @return the coordinate
         */
        public double getCoordinate() {
            return coordinate;
        }

        /**
         * @param coordinate the coordinate to set
         */
        public void setCoordinate(double coordinate) {
            this.coordinate = coordinate;
        }

        /**
         * @return the numPoints
         */
        public int getNumPoints() {
            return numPoints;
        }

        /**
         * @param numPoints the numPoints to set
         */
        public void setNumPoints(int numPoints) {
            this.numPoints = numPoints;
        }

        /**
         * @return the sweep
         */
        public String getSweep() {
            return sweep;
        }

        /**
         * @param sweep the sweep to set
         */
        public void setSweep(String sweep) {
            this.sweep = sweep;
        }
    }
}

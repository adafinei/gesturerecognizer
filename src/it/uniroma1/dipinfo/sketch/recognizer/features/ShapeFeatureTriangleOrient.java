package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
//import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
//import java.awt.Color;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.List;

/**
 *
 * This feature calculates orientation of a shape by building the largest
 * enclosed triangle to a figure, calculating the angle between the triangle
 * segments and the x-axis and returning a value based on the position of the 
 * parallel to the x-axis triangle segment. To retrieve the position it also
 * calculates a polygon from the points, retrieves its bounding box, and finds
 * the position of the segment parallel to the x-axxis
 *
 * @author Alex Dafinei
 * 
 */
public class ShapeFeatureTriangleOrient implements ShapeFeature {

    private double dir_ = 0;
    public static double NORTH = 1.0;
    public static double SOUTH = 2.0;
    public static double EAST = 3.0;
    public static double WEST = 4.0;


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hull) {

        List<Point2D> l = hull.getLargestTriangle().getExtremalPolyPoints_();
        Point2D p1 = l.get(0);
        Point2D p2 = l.get(1);
        Point2D p3 = l.get(2);

        Line2D line12 = new Line2D.Double(p1, p2);
        Line2D line13 = new Line2D.Double(p1, p3);
        Line2D line23 = new Line2D.Double(p2, p3);

//        PanelAccessor.getResultPanel().drawFreePoints(p1, p2, Color.pink);
//        PanelAccessor.getResultPanel().drawFreePoints(p1, p3, Color.red);
//        PanelAccessor.getResultPanel().drawFreePoints(p2, p3, Color.cyan);

        double angle12 = ShapeFeatureUtils.angleBetween2LinesDotProduct(line12, buildXAxisParallelLine(p1, p2));
        double angle13 = ShapeFeatureUtils.angleBetween2LinesDotProduct(line13, buildXAxisParallelLine(p1, p3));
        double angle23 = ShapeFeatureUtils.angleBetween2LinesDotProduct(line23, buildXAxisParallelLine(p2, p3));


        if (angle12 > 0 && angle12 < 20) {
            dir_ = findPositionInBoundingBox(p1, p2, hull);
        } else if (angle13 > 0 && angle13 < 20) {
            dir_ = findPositionInBoundingBox(p1, p3, hull);
        } else if (angle23 > 0 && angle23 < 20) {
            dir_ = findPositionInBoundingBox(p2, p3, hull);
        }

        if (angle12 > 260 && angle12 < 285) {
            dir_ = findPositionInBoundingBox(p1, p2, hull);
            System.out.println("[DEBUG]: ANGLE 1-2 chiamo dist!!");
        } else if (angle13 > 260 && angle13 < 285) {
            dir_ = findPositionInBoundingBox(p1, p3, hull);
        } else if (angle23 > 260 && angle23 < 285) {
            dir_ = findPositionInBoundingBox(p2, p3, hull);
        }

//        System.out.println("[DEBUG]: Angle12: " + angle12 + "Angle13: " + angle13 + "Angle23: " + angle23);
    }

    /**
     * Finds position of a segment inside a bounding box. The segment is
     * parallel to the axis
     *
     * @param p1 first point of the segment
     * @param p2 second point of the segment
     * @param hull container object for special polygon values
     * @return a double 1.0,2.0,3.0,4.0 for NORTH, SOUTH, EAST, WEST positions
     */
    private double findPositionInBoundingBox(Point2D p1, Point2D p2, PolygonValues hull) {
        double retVal = 0;
        //si costruisce la bounding box intorno al poligono passato in input
        List<Point2D> l = hull.getPointsList();
        Polygon p = new Polygon();
        Iterator<Point2D> it = l.iterator();
        while (it.hasNext()) {
            Point2D point = it.next();
            p.addPoint((int) point.getX(), (int) point.getY());
        }
        Rectangle2D r = p.getBounds2D();

        int minX = (int) r.getMinX();
        int maxX = (int) r.getMaxX();
        int minY = (int) r.getMinY();
        int maxY = (int) r.getMaxY();
        Line2D line = new Line2D.Double(p1, p2);
        int dtLim = ((maxX - minX) + (maxY - minY)) / 5; //1/5 della figura distanza dalla bounding box
        if (line.ptLineDist(new Point2D.Double(minX, minY)) < dtLim
                && line.ptLineDist(new Point2D.Double(maxX, minY)) < dtLim) {
            retVal = SOUTH;

        } else if (line.ptLineDist(new Point2D.Double(minX, maxY)) < dtLim
                && line.ptLineDist(new Point2D.Double(maxX, maxY)) < dtLim) {
            retVal = NORTH;

        } else if (line.ptLineDist(new Point2D.Double(minX, minY)) < dtLim
                && line.ptLineDist(new Point2D.Double(minX, maxY)) < dtLim) {
            retVal = EAST;
        } else /*if (line.ptLineDist(new Point2D.Double(maxX, maxY)) < dtLim
                && line.ptLineDist(new Point2D.Double(maxX, minY)) < dtLim)*/ {
            retVal = WEST;
        }
        return retVal;
    }

    private Line2D buildXAxisParallelLine(Point2D p1, Point2D p2) {
        Line2D retLine = new Line2D.Double(p1.getX(), p1.getY(), p2.getX(), p1.getY());
        return retLine;
    }

    public double getFeatureValue() {
        return dir_;
    }

    public String getFeatureName() {
        return "TriangleOrient";
    }

    public void clearFeatureValue() {
        dir_ = -1;
    }
}

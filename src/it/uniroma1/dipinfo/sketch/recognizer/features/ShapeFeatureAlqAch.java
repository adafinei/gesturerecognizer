package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;

/**
 * Calculates the ratio between the area of the largest quadrilateral
 * and the area of the convex hull
 */
class ShapeFeatureAlqAch implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeatureAlqAch.class.getName());

    private double aLq_aCh_;


    /** Creates a new instance of ShapeFeaturePch2Ach */
    public ShapeFeatureAlqAch() {
    }


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {

        double aLq = hullvalues.getLargestQuad().getQuadrilateralArea();
        double aCh = hullvalues.getConvexHull().calcConvexHullArea();
        aLq_aCh_ = aLq / aCh;
        String outStr="Alq / Ach = " + Math.round(aLq_aCh_ * 1000) / 1000.0 + " (" + aLq + " / " + aCh + ")";
        CON_.debug(outStr);
        PanelAccessor.getOutputText().append(outStr+"\n");
    }


    public double getFeatureValue() {
        return aLq_aCh_;
    }


    public void clearFeatureValue() {
        aLq_aCh_ = -1;
    }

    public String getFeatureName() {
       return "ALQACH";
    }

}

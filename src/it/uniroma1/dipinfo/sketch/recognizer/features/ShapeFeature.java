package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.dao.Dao;
import it.uniroma1.dipinfo.sketch.dao.pojo.FeaturePojo;
import java.util.HashMap;
import java.util.Map;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Interface Implemented by all Feature class; for defining new features,
 * a user should implement this class, then fill in the database (features table)
 * with the feature parameters.
 *
 * @author Alex Dafinei
 */
public interface ShapeFeature {
    /**package of feature classes */
    public static final String FEATURE_PACKAGE = "it.uniroma1.dipinfo.sketch.recognizer.features";
    /**feature code names */
    public static final String AchAer = "AchAer";
    public static final String AlqAch = "AlqAch";
    public static final String AlqAer = "AlqAer";
    public static final String HerWer = "HerWer";
    public static final String Pch2Ach = "Pch2Ach";
    public static final String AltAch = "AltAch";
    public static final String PltPch = "PltPch";
    public static final String Aer = "Aer";
    public static final String AngleRatio = "AR";
    public static final String ClosedFigure = "CF";
    public static final String PointingDirection = "PD";
    public static final String TriangleOrient = "TO";
    public static final String InnerOuterPoints = "IOP";
    public static final String LineDensity = "LD";
    public static final String AbbAer = "AbbAer";
    public static final String PerPf = "PerPf";

    /**
     * Factory class for loading features from the database
     */
    public static final class Factory {

        public static Map create() {

            Map retShapeFeatures = new HashMap();
            //carica le classi feature dalla base di dati
            List<FeaturePojo> listaFeature = Dao.getInstance().getFeatures();
            Iterator<FeaturePojo> it = listaFeature.iterator();
            try {
                while (it.hasNext()) {
                    //inserisce le classi feature in una MAP
                    FeaturePojo fp = it.next();
                    String key = fp.getFeatureName();
                    Class c = Class.forName(FEATURE_PACKAGE +"."+fp.getFeatureClass());
                    ShapeFeature feat=(ShapeFeature)c.newInstance();
                    retShapeFeatures.put(key, feat);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ShapeFeature.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex){
                Logger.getLogger(ShapeFeature.class.getName()).log(Level.SEVERE, null, ex);
            } catch(IllegalAccessException ex){
                Logger.getLogger(ShapeFeature.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return retShapeFeatures;
        }
    }

    /**Calculates the feature value */
    public void calculate(PolygonValues hull);

    /**Gets the feature value */
    public double getFeatureValue();

    /**Gets the feature name */
    public String getFeatureName();

    /**Clears the feature value */
    public void clearFeatureValue();
}

package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;

/**
 * Calculates the ratio between the height of the enclosing rectangle
 * and the width of the enclosing rectangle
 *
 * @author Alex Dafinei
 */
class ShapeFeatureHerWer implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeatureHerWer.class.getName());
    private double hEr_wEr_;

    /** Creates a new instance of ShapeFeaturePch2Ach */
    public ShapeFeatureHerWer() {
    }

    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {

        double hEr = hullvalues.getErDimension().getHeight();
        double wEr = hullvalues.getErDimension().getWidth();
        if (hEr > wEr) {
            double swap = hEr;
            hEr = wEr;
            wEr = swap;
        }
        hEr_wEr_ = hEr / wEr;
        String outStr = "Her / Wer = " + Math.round(hEr_wEr_ * 1000) / 1000.0 + " (" + hEr + " / " + wEr + ")";
        CON_.debug("Her / Wer = " + Math.round(hEr_wEr_ * 1000) / 1000.0 + " (" + hEr + " / " + wEr + ")");
        PanelAccessor.getOutputText().append(outStr + "\n");
    }

    public double getFeatureValue() {
        return hEr_wEr_;
    }

    public void clearFeatureValue() {
        hEr_wEr_ = -1;
    }

    public String getFeatureName() {
        return "HERWER";
    }
}

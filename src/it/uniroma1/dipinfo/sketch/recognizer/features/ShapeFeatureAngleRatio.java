package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import it.uniroma1.dipinfo.sketch.structures.Stroke;
import it.uniroma1.dipinfo.sketch.config.Config;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.List;

/**
 * This class calculates the ratio between acute and obtuse angles in a stroke
 * after approximation of the stroke through line segments.
 *
 * @author Alex Dafinei
 */
public class ShapeFeatureAngleRatio implements ShapeFeature {

    private double angleRatio_;
    private int step_ = 10;
    private static int AR_PERCENT = Config.getInstance().getANGLE_RATIO_STEP_PERCENT();

    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hull) {


        //fa un'approssimazione della curva disegnata con segmenti, e verifica
        //la misura degli angoli compresi fra loro
        double width = hull.getErDimension().getWidth();
        double height = hull.getErDimension().getHeight();
        double st = (((width + height) / 2) * (AR_PERCENT)) / 100;
        step_ = (int) st;
        System.out.println("[DEBUG]: Step:" + step_);
        List<Point2D> listapunti = hull.getPointsList();
        //fa una approssimazione della lista in ingresso prendendo la media 
        //dei punti su un intervallo se i punti della lista in ingresso sono tanti
        if (listapunti.size() > 200) {
            listapunti = ShapeFeatureUtils.getBeautifiedPixelList(listapunti, 8);
            PanelAccessor.getResultPanel().drawStroke(new Stroke(listapunti.get(0),
                    listapunti.get(listapunti.size() - 1), listapunti, null), Color.BLACK);
        }
        if (listapunti.size() <= step_ + 1) {
            return;
        }
        Point2D pin = listapunti.get(0);
        int acute = 1;
        int obtuse = 1;



        Line2D l1 = new Line2D.Double(pin, listapunti.get(step_));
        Line2D l2;

        try {
            int j = 1;
            if (step_ != 0) {
                for (int i = step_; i < listapunti.size() - step_; i += step_) {

                    l2 = new Line2D.Double(listapunti.get(i), listapunti.get(i + step_));
                    //calcolo angolo fra le linne
                    double angle = ShapeFeatureUtils.angleBetween2LinesDotProduct(l1, l2);
                    //double angle=ShapeFeatureUtils.angleBetween2Lines(l1, l2);
                    System.out.println("[DEBUG]: Angle found at step: " + i + ":" + angle);
                    if (angle == Double.NaN) {
                        angleRatio_ = 180;
                    }

                    j++;
                    i = i + step_;
                    l1 = l2;
                    int angleInt = (int) Math.abs(angle);

                    if ((angleInt > 0 && angleInt < 75) || (angleInt > 180 && angleInt < 255)) {
                        acute++;
                    } else if ((angleInt > 105 && angleInt < 180) || (angleInt > 260) && (angleInt < 355)) {
                        obtuse++;
                    }

                }
            }

            this.angleRatio_ = (double) acute / obtuse;
        } catch (Exception ex) {
        }
        this.angleRatio_ = (double) acute / obtuse;
        System.out.println("[DEBUG]: acute/obtuse:" + acute + "/" + obtuse + "=" + this.angleRatio_);
    }

    public double getFeatureValue() {
        return this.angleRatio_;
    }

    public void clearFeatureValue() {
        this.angleRatio_ = -1;
    }

    public String getFeatureName() {
        return "ANGLERATIO";
    }
}

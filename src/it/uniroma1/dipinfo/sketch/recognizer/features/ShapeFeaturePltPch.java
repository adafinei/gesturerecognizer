package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;


/**
 * Calculates the ratio between the perimeter of the largest triangle
 * and the perimeter of the convex hull
 *
 * @author Alex Dafinei
 */
class ShapeFeaturePltPch implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger(ShapeFeaturePltPch.class.getName());

    private double pLt_pCh_;


    /** Creates a new instance of ShapeFeaturePch2Ach */
    public ShapeFeaturePltPch() {
    }


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {

        double pLt = hullvalues.getLargestTriangle().getQuadrilateralPerimeter();
        double pCh = hullvalues.getConvexHull().calcConvexHullPerimeter();
        pLt_pCh_ = pLt / pCh;
        String outStr="Plt / Pch = " + Math.round(pLt_pCh_ * 1000) / 1000.0 + " (" + pLt + " / " + pCh + ")";
        CON_.debug("Plt / Pch = " + Math.round(pLt_pCh_ * 1000) / 1000.0 + " (" + pLt + " / " + pCh + ")");
        PanelAccessor.getOutputText().append(outStr + "\n");
    }


    public double getFeatureValue() {
        return pLt_pCh_;
    }


    public void clearFeatureValue() {
        pLt_pCh_ = -1;
    }

    public String getFeatureName() {
        return "PLTPCH";
    }

}

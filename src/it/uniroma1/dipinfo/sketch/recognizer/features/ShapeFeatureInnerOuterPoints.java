package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import it.uniroma1.dipinfo.sketch.config.Config;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;

/**
 * This class computes the ratio between points on the border
 * and points on the inside of the drawn figure
 *
 * @author Alex Dafinei
 */
public class ShapeFeatureInnerOuterPoints implements ShapeFeature {


    private double borderInside_;
    private static int THRESHOLD=Config.getInstance().getIOP_THRESHOLD();


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hull) {

        //La coordinata piu a Sud, quella piu a nord, quella piu a ovest e est
        double maxX=ShapeFeatureUtils.findRightmost(hull.getPointsList());
        double maxY=ShapeFeatureUtils.findUpmost(hull.getPointsList());
        double minX=ShapeFeatureUtils.findLeftmost(hull.getPointsList());
        double minY=ShapeFeatureUtils.findDownmost(hull.getPointsList());

        double xStart=minX+Math.abs((maxX-minX)/THRESHOLD);
        double xStop=maxX-Math.abs((maxX-minX)/THRESHOLD);

        double yStart=minY+Math.abs((maxY-minY)/THRESHOLD);
        double yStop=maxY-Math.abs((maxY-minY)/THRESHOLD);

        int outerPoints=1;
        int innerPoints=1;
        Point2D punto;
        List<Point2D> lista=hull.getPointsList();
        Iterator<Point2D> it=lista.iterator();
        while(it.hasNext()){
            punto=it.next();
            //controllo delle coordinate del punto
            double xPunto=punto.getX();
            double yPunto=punto.getY();
            if(xPunto<xStop&&xPunto>xStart&&yPunto>yStart&&yPunto<yStop){
                innerPoints++;
            }else{
                outerPoints++;
            }


        }
        this.borderInside_=(double)innerPoints/(double)outerPoints;
        System.out.println("[DEBUG]: inner/outer:"+innerPoints+"/"+outerPoints+"="+this.borderInside_);

    }

    public double getFeatureValue() {
        return this.borderInside_;
    }

    public void clearFeatureValue() {
        this.borderInside_=-1;
    }

    public String getFeatureName() {
        return "INNEROUTERPOINTS";
    }

}

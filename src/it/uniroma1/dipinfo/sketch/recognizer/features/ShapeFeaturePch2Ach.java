package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;



/**
 * Calculates the ratio between the squared perimeter of the convex hull
 * and the area of the convex hull
 */
class ShapeFeaturePch2Ach implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeaturePch2Ach.class.getName());

    private double pCh2_aCh_;


    /** Creates a new instance of ShapeFeaturePch2Ach */
    public ShapeFeaturePch2Ach() {
    }


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {

        double aCh = hullvalues.getConvexHull().calcConvexHullArea();
        double pCh = hullvalues.getConvexHull().calcConvexHullPerimeter();
        pCh2_aCh_ = pCh * pCh / aCh;
        String outStr="P2ch / Ach = " + Math.round(pCh2_aCh_ * 1000) / 1000.0 + " (" + pCh + "^2 / " + aCh + ")";
        CON_.debug(outStr);
        PanelAccessor.getOutputText().append(outStr + "\n");
    }


    public double getFeatureValue() {
        return pCh2_aCh_;
    }


    public void clearFeatureValue() {
        pCh2_aCh_ = -1;
    }

    public String getFeatureName() {
        return "PCH2ACH";
    }

}

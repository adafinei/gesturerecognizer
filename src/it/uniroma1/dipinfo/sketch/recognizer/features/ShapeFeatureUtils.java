package it.uniroma1.dipinfo.sketch.recognizer.features;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for various shape features,
 * such as orientation etc.
 */
public class ShapeFeatureUtils {

    public static String DIRECTION_NORTH = "NORTH";
    public static String DIRECTION_SOUTH = "SOUTH";
    public static String DIRECTION_EAST = "EAST";
    public static String DIRECTION_WEST = "WEST";
    public static String DIRECTION_NORTHEAST = "NORTHEAST";
    public static String DIRECTION_NORTHWEST = "NORTHWEST";
    public static String DIRECTION_SOUTHEAST = "SOUTHEAST";
    public static String DIRECTION_SOUTHWEST = "SOUTHWEST";
    public static String DIRECTION_UNKNOWN = "UNKNOWN";

    /**
     * Finds the rightmost x coordinate of a point
     *
     * @param pList - list of points
     * @return the x coord of the rightmost point
     */
    public static double findRightmost(List<Point2D> pList) {
        double max = Double.MIN_VALUE;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        Point2D p;
        while (it.hasNext()) {
            p = it.next();
            if (p.getX() > max) {
                max = p.getX();
            }
        }

        return max;
    }

    /**
     * Finds the index of the rightmost point in a list of points
     *
     * @param pList - list of points
     * @return the  index of the point
     */
    public static int findRightmostIndex(List<Point2D> pList) {
        double max = Double.MIN_VALUE;
        int i = 0;
        int index = 0;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        Point2D p;
        while (it.hasNext()) {
            p = it.next();

            if (p.getX() > max) {
                max = p.getX();
                index = i;
            }
            i++;
        }

        return index;
    }

    /**
     * Finds the Leftmost x coordinate of a point
     *
     * @param pList - list of points
     * @return the x coord of the Leftmost point
     */
    public static double findLeftmost(List<Point2D> pList) {
        double min = Double.MAX_VALUE;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        Point2D p;
        while (it.hasNext()) {
            p = it.next();
            if (p.getX() < min) {
                min = p.getX();
            }
        }

        return min;
    }

    /**
     * Finds the index of the Leftmost point in a list of points
     *
     * @param pList - list of points
     * @return the index of the point
     */
    public static int findLeftmostIndex(List<Point2D> pList) {
        double min = Double.MAX_VALUE;
        int i = 0;
        int index = 0;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        Point2D p;
        while (it.hasNext()) {
            p = it.next();
            if (p.getX() < min) {
                min = p.getX();
                index = i;
            }
            i++;
        }

        return index;
    }

    /**
     * Finds the Upmost y coordinate of a point
     *
     * @param pList - list of points
     * @return the y coord of the Leftmost point
     */
    public static double findUpmost(List<Point2D> pList) {
        double max = Double.MIN_VALUE;
        Point2D p;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        while (it.hasNext()) {
            p = it.next();
            if (p.getY() > max) {
                max = p.getY();
            }
        }
        return max;
    }

    /**
     * Finds the index of the Upmost point in a list of points
     *
     * @param pList - list of points
     * @return the index of the point
     */
    public static int findUpmostIndex(List<Point2D> pList) {
        double max = Double.MIN_VALUE;
        Point2D p;
        int i = 0;
        int index = 0;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        while (it.hasNext()) {
            p = it.next();
            if (p.getY() > max) {
                max = p.getY();
                index = i;
            }
            i++;
        }
        return index;
    }

    /**
     * Finds the Downmost y coordinate of a point
     *
     * @param pList - list of points
     * @return the y coord of the Downmost point
     */
    public static double findDownmost(List<Point2D> pList) {
        double min = Double.MAX_VALUE;

        Point2D p;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        while (it.hasNext()) {
            p = it.next();
            if (p.getY() < min) {
                min = p.getY();
            }
        }
        return min;
    }

    /**
     * Finds the index of the Downmost point in a list of points
     *
     * @param pList - list of points
     * @return the index of the point
     */
    public static int findDownmostIndex(List<Point2D> pList) {
        double min = Double.MAX_VALUE;
        int i = 0;
        int index = 0;
        Point2D p;
        Iterator<Point2D> it = (Iterator<Point2D>) pList.iterator();
        while (it.hasNext()) {
            p = it.next();
            if (p.getY() < min) {
                min = p.getY();
                index = i;
            }
            i++;
        }
        return index;
    }

    /**
     * Maximum distance from left to right of the figure
     *
     * @param pList
     * @return Maximum distance from left to right of the figure
     */
    public static double findMaxExcursionX(List<Point2D> pList) {
        return Math.abs(findLeftmost(pList) - findRightmost(pList));
    }

    /**
     * Maximum distance from top to bottom of the figure
     *
     * @param pList
     * @return maximum distance from top to bottom of the figure
     */
    public static double findMaxExcursionY(List<Point2D> pList) {
        return Math.abs(findUpmost(pList) - findDownmost(pList));
    }

    /**
     * Checks if a figure is closed
     *
     * @param pList
     * @return true if a figure is closed
     */
    public static boolean isClosed(List<Point2D> pList) {
        Point2D firstPoint = pList.get(0);
        Point2D lastPoint = pList.get(pList.size() - 1);
        if (firstPoint.distance(lastPoint) < 50) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Calculates the angle between 2 lines with the atan java method
     *
     * @param line1
     * @param line2
     * @return the angle between 2 lines with the atan java method
     */
    public static double angleBetween2Lines(Line2D line1, Line2D line2) {
        double angle1 = Math.atan2(line1.getY1() - line1.getY2(),
                line1.getX1() - line1.getX2());
        double angle2 = Math.atan2(line2.getY1() - line2.getY2(),
                line2.getX1() - line2.getX2());
        return Math.abs(angle1 - angle2) * 180;
    }

    /**
     * Calculates the angle between 2 lines with the acos java method
     *
     * @param line1
     * @param line2
     * @return the angle between 2 lines with the acos java method
     */
    public static double angleBetween2LinesDotProduct(Line2D line1, Line2D line2) {
        double dx1 = line1.getX2() - line1.getX1();
        double dy1 = line1.getY2() - line1.getY1();
        double dx2 = line2.getX2() - line2.getX1();
        double dy2 = line2.getY2() - line2.getY1();
        double d = dx1 * dx2 + dy1 * dy2;   // dot product of the 2 vectors
        double l2 = (dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2); // product of the squared lengths
        double angle = Math.acos(d / Math.sqrt(l2));
        angle = angle * 180;
        return angle = angle >= 360 ? 0.0 : angle;

    }

    /**
     * Gets the direction of a line
     *
     * @param pList the list of points traced by the user
     * @return the direction of a line note: one of the four cardinal directions
     * north south east west
     */
    public static String getDirection(List<Point2D> pList) {

        Point2D firstPoint = pList.get(0);
        Point2D secondPoint = pList.get(pList.size() - 1);

        //Punto in comune tra le due rette (firstPoint).
        double xCommon = firstPoint.getX();
        double yCommon = firstPoint.getY();

        //Coordinata per costruire la retta orizzontale .
        double xHorizontal = secondPoint.getX();
        double yHorizontal = firstPoint.getY();

        //Coordinata del secondo punto (lastPoint).
        double xLast = secondPoint.getX();
        double yLast = secondPoint.getY();

        //Calcolo delle distanze tra i punti.
        double dX1 = xCommon - xHorizontal;
        double dY1 = yCommon - yHorizontal;
        double dX2 = xCommon - xLast;
        double dY2 = yCommon - yLast;

        double theta = ((dX1 * dX2) + (dY1 * dY2))
                / (Math.sqrt(Math.pow(dX1, 2) + Math.pow(dY1, 2))
                * Math.sqrt(Math.pow(dX2, 2) + Math.pow(dY2, 2)));

        double product = Math.acos(theta);
        double result = Math.toDegrees(product);

        //Analisi caso Nord.
        if ((yLast <= yCommon) && ((result >= 45 && result <= 90) || xCommon == xLast)) {
            return DIRECTION_NORTH;
        } //Analisi caso Sud.
        else if ((yLast > yCommon) && ((result >= 45 && result <= 90) || xCommon == xLast)) {
            return DIRECTION_SOUTH;
        } //Analisi caso Ovest.
        else if ((xLast <= xCommon) && ((result >= 0 && result <= 45) || yCommon == yLast)) {
            return DIRECTION_WEST;
        } //Analisi caso Est.
        else if ((xLast > xCommon) && ((result >= 0 && result <= 45) || yCommon == yLast)) {
            return DIRECTION_EAST;
        }

        return DIRECTION_UNKNOWN;

    }

    /**
     * Gets the diagonal direction of a line
     *
     * @param pList the list of points traced by the user
     * @return the direction of a line note: one of the four middle cardinal directions
     * northwest southwest northeast southeast
     */
    public static String getDiagonalDirection(List<Point2D> pList) {

        Point2D firstPoint = pList.get(0);
        Point2D secondPoint = pList.get(pList.size() - 1);
        //Punto in comune tra le due rette (firstPoint).
        double xCommon = firstPoint.getX();
        double yCommon = firstPoint.getY();
        //Coordinata per costruire la retta orizzontale .
        double xHorizontal = secondPoint.getX();
        double yHorizontal = firstPoint.getY();
        //Coordinata del secondo punto (lastPoint).
        double xLast = secondPoint.getX();
        double yLast = secondPoint.getY();
        //Calcolo delle distanze tra i punti.
        double dX1 = xCommon - xHorizontal;
        double dY1 = yCommon - yHorizontal;
        double dX2 = xCommon - xLast;
        double dY2 = yCommon - yLast;

        double theta = ((dX1 * dX2) + (dY1 * dY2))
                / (Math.sqrt(Math.pow(dX1, 2) + Math.pow(dY1, 2))
                * Math.sqrt(Math.pow(dX2, 2) + Math.pow(dY2, 2)));

        double product = Math.acos(theta);
        double result = Math.toDegrees(product);

        //Analisi caso NordOvest.
        if ((yLast <= yCommon && xLast <= xCommon) && (result >= 22.5 && result <= 67.5)) {
            return DIRECTION_NORTHWEST;
        } //Analisi caso NordEst.
        else if ((yLast <= yCommon && xLast >= xCommon) && (result >= 22.5 && result <= 67.5)) {
            return DIRECTION_NORTHEAST;
        } //Analisi caso SudEst.
        else if ((yLast >= yCommon && xLast >= xCommon) && (result >= 22.5 && result <= 67.5)) {
            return DIRECTION_SOUTHEAST;
        } //Analisi caso SudOvest.
        else if ((yLast >= yCommon && xLast <= xCommon) && (result >= 22.5 && result <= 67.5)) {
            return DIRECTION_SOUTHWEST;
        }
        //Analisi caso fuori dai 4 ranges.
        return DIRECTION_UNKNOWN;
    }

    /**
     * Returns a string form for the direction of line
     *
     * @param  pList - the list of points
     */
    public static String getDirectionString(List<Point2D> pList) {
        String dir = getDirection(pList);

        if (dir.equalsIgnoreCase(DIRECTION_NORTH)) {
            return " verticale dal basso verso l'alto";
        }

        if (dir.equalsIgnoreCase(DIRECTION_SOUTH)) {
            return " verticale dall'alto verso il basso";
        }

        if (dir.equalsIgnoreCase(DIRECTION_EAST)) {
            return " orizzontale da sinistra verso destra";
        }

        if (dir.equalsIgnoreCase(DIRECTION_WEST)) {
            return " orizzontale da destra verso sinistra";
        }

        return dir;
    }

    /**
     * Returns a string form for the direction of line
     *
     * @param  pList - the list of points
     */
    public static String getDiagonalDirectionString(List<Point2D> pList) {
        String dir = getDirection(pList);

        if (dir.equalsIgnoreCase(DIRECTION_NORTHWEST)) {
            return " da basso destra verso alto sinistra";
        }

        if (dir.equalsIgnoreCase(DIRECTION_NORTHEAST)) {
            return " da basso sinistra verso alto destra";
        }

        if (dir.equalsIgnoreCase(DIRECTION_SOUTHEAST)) {
            return " da alto sinistra verso basso destra";
        }

        if (dir.equalsIgnoreCase(DIRECTION_SOUTHWEST)) {
            return " da alto destra verso basso sinistra";
        }

        return dir;
    }

    /**
     * @deprecated
     * Performs an average filtering process over the list of points
     * actually this method is never used
     *
     * @param lista the list of points
     * @param threshold a threshold for the filter
     * @return a filtered list
     */
    public static List<Point2D> getAveragePixelList(List<Point2D> lista, int threshold) {
        List<Point2D> av = new LinkedList<Point2D>();
        int step = (lista.size() * threshold) / 100;
        int i = 0;
        double sumX = 0;
        double sumY = 0;
        double avX = 0;
        double avY = 0;
        Point2D avp;
        while (i + step < lista.size()) {
            List<Point2D> sub = lista.subList(i, i + step);
            for (int j = 0; j < sub.size(); j++) {
                sumX += (double) sub.get(j).getX();
                sumY += (double) sub.get(j).getY();
            }
            avX = sumX / sub.size();
            avY = sumY / sub.size();
            avp = new Point2D.Double(avX, avY);
            av.add(avp);
            sumX = 0;
            sumY = 0;
            avX = 0;
            avY = 0;
            sub = null;
            i = i + step;
        }

        av.add(lista.get(lista.size() - 1));

        return av;
    }

    /**
     * @deprecated
     * Performs an average filtering process over the list of points
     * actually this method is never used
     *
     * @param lista the list of points
     * @param threshold a threshold for the filter
     * @return a filtered list
     */
    public static List<Point2D> getFastAveragePixelList(List<Point2D> lista, int threshold) {
        List<Point2D> av = new LinkedList<Point2D>();
        int step = (lista.size() * threshold) / 100;
        int i = 0;
        double avX = 0;
        double avY = 0;
        Point2D avp;
        while (i + step < lista.size()) {
            avX = (lista.get(i).getX() + lista.get(i + step).getX()) / 2;
            avY = (lista.get(i).getY() + lista.get(i + step).getY()) / 2;
            avp = new Point2D.Double(avX, avY);
            av.add(avp);
            i = i + step;
        }
        return av;
    }

    /**
     * Performs a tunnel filtering over the list
     *
     * @param lista the list of points
     * @param threshold a threshold for the filter
     * @return a filtered list
     */
    public static List<Point2D> getBeautifiedPixelList(List<Point2D> lista, int threshold) {
        List<Point2D> av = new LinkedList<Point2D>();
        double dist = (Double) (((ShapeFeatureUtils.findMaxExcursionX(lista)
                + ShapeFeatureUtils.findMaxExcursionY(lista)) / 2) * threshold) / 100;
        double ll; //limit left
        double lr; //limit right
        double lu; //limit up;
        double ld; //limit down;
        Point2D lastPoint = (Point2D) lista.get(0);
        ll = lastPoint.getX() - dist;
        lr = lastPoint.getX() + dist;
        lu = lastPoint.getY() - dist;
        ld = lastPoint.getY() + dist;
        //inserisce il punto nella nuova lista
        av.add(lastPoint);
        for (int i = 1; i < lista.size(); i++) {
            Point2D p = (Point2D) lista.get(i);
            double x = p.getX();
            double y = p.getY();

            if (x >= ll && x <= lr && y >= lu && y <= ld) {
                //do nothing
            } else {
                lastPoint = p;
                ll = lastPoint.getX() - dist;
                lr = lastPoint.getX() + dist;
                lu = lastPoint.getY() - dist;
                ld = lastPoint.getY() + dist;
                av.add(p);
            }
        }
        //aggiunge l'ultimo punto
        av.add(lista.get(lista.size() - 1));
        return av;
    }

    /**
     * Calculates the angle between the line in input and the X axis
     *
     * @param line segment
     * @param theOrigin the origin point
     * @return retRadAngle the angle between lines
     */

    public static double calcAngle(Line2D line, Point2D theOrigin) {
        double retRadAngle = 0;

        Point2D first = new Point2D.Double(line.getX1(), line.getY1());
        Point2D second = new Point2D.Double(line.getX2(), line.getY2());
        

        // Se origine e' null ==> prende come origine un punto qualsiasi
        if (theOrigin == null) {
            theOrigin = first;
        }

        // Sceglie tra i due il punto finale in base all'origine
        Point2D endPoint = (theOrigin.equals(first) ? second : first);
        System.out.println("[DEBUG]: Uso (origine, finale) = (" + theOrigin + "," + endPoint + ")");

        double y = theOrigin.getY() - endPoint.getY();
        double x = endPoint.getX() - theOrigin.getX();
        retRadAngle = Math.atan2(y, x);
        if (retRadAngle < 0) {
            retRadAngle += 2 * Math.PI;
        }

        double degAngle = Math.toDegrees(retRadAngle);
        System.out.println("[DEBUG]: Line (" + first + "," + second + "), angle = " + retRadAngle + " (" + degAngle + " deg)");
        return retRadAngle;
    }
}

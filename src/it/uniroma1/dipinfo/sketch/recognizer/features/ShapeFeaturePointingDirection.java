package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import java.awt.geom.Point2D;
import java.util.List;
import it.uniroma1.dipinfo.sketch.config.Config;

/**
 * This class calculates the pointing direction of an arrowhead.
 *
 * @author Alex Dafinei
 */
public class ShapeFeaturePointingDirection implements ShapeFeature {

    public static double NORTH = 1.0;
    public static double SOUTH = 2.0;
    public static double EAST = 3.0;
    public static double WEST = 4.0;
    private double pointDir_;
    private static int PD_PERCENT = Config.getInstance().getPOINTING_DIRECTION_PERCENT();
    private static int THRESHOLD_FILTER = Config.getInstance().getMIN_POINTS_PER_STROKE();

    public void calculate(PolygonValues hull) {
         List<Point2D> l=hull.getLargestTriangle().getExtremalPolyPoints_();
        //       
        this.pointDir_ = getPointingDirection2(hull.getPointsList());
    }


    /*
     * Calculates the feature value
     */
    private double getPointingDirection2(List<Point2D> pList) {
        //ridimensiona la lista dei punti
        if (pList.size() > THRESHOLD_FILTER) {
            pList = ShapeFeatureUtils.getBeautifiedPixelList(pList, 8);
        }
        double distX = ShapeFeatureUtils.findMaxExcursionX(pList);
        double distY = ShapeFeatureUtils.findMaxExcursionY(pList);

        int sogliaX = (int) ((distX * PD_PERCENT) / 100);
        int sogliaY = (int) ((distY * PD_PERCENT) / 100);

        //bisogna trovare la direzione della punta
        int lmi = ShapeFeatureUtils.findLeftmostIndex(pList);
        int rmi = ShapeFeatureUtils.findRightmostIndex(pList);
        int umi = ShapeFeatureUtils.findUpmostIndex(pList);
        int dmi = ShapeFeatureUtils.findDownmostIndex(pList);
        
        Point2D lmp=pList.get(lmi);
        Point2D rmp=pList.get(rmi);
        Point2D ump=pList.get(umi);
        Point2D dmp=pList.get(dmi);

        double lmpx=lmp.getX();
        double lmpy=lmp.getY();

        double rmpx=rmp.getX();
        double rmpy=rmp.getY();

        double umpx=ump.getX();
        double umpy=ump.getY();

        double dmpx=dmp.getX();
        double dmpy=dmp.getY();

        if(Math.abs(lmpy-rmpy)<sogliaY && Math.abs(dmpy-(lmpy+rmpy)/2)<sogliaY){
            return SOUTH;
        }
        if(Math.abs(lmpy-rmpy)<sogliaY && Math.abs(umpy-(lmpy+rmpy)/2)<sogliaY){
            return NORTH;
        }
        if(Math.abs(umpx-dmpx)<sogliaX && Math.abs(rmpx-(umpx+dmpx)/2)<sogliaX){
            return WEST;
        }
        if(Math.abs(umpx-dmpx)<sogliaX && Math.abs(lmpx-(umpx+dmpx)/2)<sogliaX){
            return EAST;
        }
        return NORTH;
    }

    private double getPointingDirection(List<Point2D> pList) {
        //prende il primo e l'ultimo punto dello stroke
        Point2D firstPoint = pList.get(0);
        Point2D lastPoint = pList.get(pList.size() - 1);
        //prende dei punti in mezzo allo stroke.
        Point2D middlePoint = pList.get(pList.size() / 2);
        double xfp = firstPoint.getX();
        double yfp = firstPoint.getY();
        double xlp = lastPoint.getX();
        double ylp = lastPoint.getY();
        double xmp = middlePoint.getX();
        double ymp = middlePoint.getY();
        //da un'idea di quanto e grande la figura in modo da impostare la soglia
        double sogliaY = ShapeFeatureUtils.findMaxExcursionY(pList) * 25 / 100;
        double sogliaX = ShapeFeatureUtils.findMaxExcursionX(pList) * 25 / 100;

        if (Math.abs(yfp - ylp) < sogliaY && ymp < (Math.abs(yfp + ylp) / 2) + sogliaY) {
            return ShapeFeaturePointingDirection.NORTH;
        }

        if (Math.abs(yfp - ylp) < sogliaY && ymp > (Math.abs(yfp + ylp) / 2) + sogliaY) {
            return ShapeFeaturePointingDirection.SOUTH;
        }

        if (Math.abs(xfp - xlp) < sogliaX && xmp < (Math.abs(xfp + xlp) / 2) + sogliaX) {
            return ShapeFeaturePointingDirection.WEST;
        }

        if (Math.abs(xfp - xlp) < sogliaX && xmp > (Math.abs(xfp + xlp) / 2) + sogliaX) {
            return ShapeFeaturePointingDirection.EAST;
        }

        return -1;
    }

    public double getFeatureValue() {
        return this.pointDir_;
    }

    public void clearFeatureValue() {
        this.pointDir_ = -1;
    }

    public String getFeatureName() {
        return "POINTINGDIRECTION";
    }
}

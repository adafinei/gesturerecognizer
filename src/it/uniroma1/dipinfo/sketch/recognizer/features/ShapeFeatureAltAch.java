package it.uniroma1.dipinfo.sketch.recognizer.features;

import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;


/**
 * Calculates the ratio between the area of the largest triangle
 * and the area of the convex hull
 */
class ShapeFeatureAltAch implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeatureAltAch.class.getName());

    private double aLt_aCh_;


    /** Creates a new instance of ShapeFeaturePch2Ach */
    public ShapeFeatureAltAch() {
    }


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {

        double aLt = hullvalues.getLargestTriangle().getQuadrilateralArea();
        double aCh = hullvalues.getConvexHull().calcConvexHullArea();
        aLt_aCh_ = aLt / aCh;
        String outStr="Alt / Ach = " + Math.round(aLt_aCh_ * 1000) / 1000.0 + " (" + aLt + " / " + aCh + ")";
        CON_.debug("Alt / Ach = " + Math.round(aLt_aCh_ * 1000) / 1000.0 + " (" + aLt + " / " + aCh + ")");
        PanelAccessor.getOutputText().append(outStr + "\n");
    }


    public double getFeatureValue() {
        return aLt_aCh_;
    }


    public void clearFeatureValue() {
        aLt_aCh_ = -1;
    }

    public String getFeatureName() {
        return "ALTACH";
    }

}

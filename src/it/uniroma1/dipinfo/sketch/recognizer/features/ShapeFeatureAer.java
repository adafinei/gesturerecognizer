package it.uniroma1.dipinfo.sketch.recognizer.features;


import org.apache.log4j.Logger;
import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;


/**
 * This class calculates the product between
 * height and width of the enclosing rectangle of
 * the drawn figure, a.k.a the area of the bounding box
 *
 */
class ShapeFeatureAer implements ShapeFeature {

    /** Logger per console */
    private static final Logger CON_ = Logger.getLogger("CON." + ShapeFeatureAer.class.getName());
    private double aEr_;


    /** Creates a new instance of ShapeFeatureAer */
    public ShapeFeatureAer() {
    }


    /*
     * Calculates the feature value
     */
    public void calculate(PolygonValues hullvalues) {

        double hEr = hullvalues.getErDimension().getHeight();
        double wEr = hullvalues.getErDimension().getWidth();
       
        aEr_ = hEr * wEr;
        CON_.debug("Aer       = " + Math.round(aEr_ * 1000) / 1000.0 + " (" + hEr + " * " + wEr + ")");
    }


    public double getFeatureValue() {
        return aEr_;
    }


    public void clearFeatureValue() {
        aEr_ = -1;
    }

    public String getFeatureName() {
        return "AER";
    }

}

package it.uniroma1.dipinfo.sketch.recognizer;

import it.uniroma1.dipinfo.sketch.geom2d.PolygonValues;
import it.uniroma1.dipinfo.sketch.gui.PanelAccessor;
import it.uniroma1.dipinfo.sketch.recognizer.agents.BaseRecognizer;
import it.uniroma1.dipinfo.sketch.recognizer.features.ShapeFeature;
import it.uniroma1.dipinfo.sketch.structures.Stroke;
import it.uniroma1.dipinfo.sketch.structures.TimeInterval;
import it.uniroma1.dipinfo.sketch.config.Config;
import java.awt.geom.Point2D;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This is a manager class for handling the stroke recognition
 * It first calls an instance of PolygonValues, in order to calculate
 * values for the convex hull of the drawn figure, which will be used
 * by the Recognizer agents in order to recognize the figure
 *
 * @author Alex Dafinei 
 */
public class RecognizerManager extends Thread {

    /**The stroke for recognition */
    private static Stroke stroke_;
    /**The stroke list for recognition */
    private static List<Stroke> strokeList_;
    /**Common values based on convex hull parameters of the stroke */
    private static PolygonValues hullValues_;
    /**Collection of recognizers */
    private static List<BaseRecognizer> recognizers_;
    /**Collection of features */
    private static Map features_;
    /**Control flag for execution */
    private static boolean running_ = false;
    /**Correspondency of the actual recognized symbol */
    private static int correspondency_;
    /**Name of the actual symbol */
    private static String symbolName_;
    /**List of recognized unistroke symbols which compose a complex symbol */
    private static List<String> symbolNames_ = new LinkedList<String>();
    /**Computation start time */
    private static long startTime_;
    /**Computation end time */
    private static long endTime_;
    /**Threshold to define conflicts: minimum difference of certainty values
     * under which a conflict is created
     */
    public static final int CONFLICT_THRESHOLD = Config.getInstance().getCONFLICT_THRESHOLD();
    /**
     * Determines if the recognition is to be made with constraints
     * or by collapsing all strokes into one
     */
    private static boolean contraintRecognition_ = false;

    /**
     * Constructor
     */
    public RecognizerManager() {
        if (recognizers_ == null) {
            recognizers_ = BaseRecognizer.Factory.create();
        }
        if (RecognizerManager.features_ == null) {
            RecognizerManager.features_ = ShapeFeature.Factory.create();
        }
    }

    /**
     * Starts the recognition process
     */
    private void startRecognizers() {
        
        //lanciamo il manager
        RecognizerManager.running_ = true;
        new Thread(this).start();

    }

    /**
     * Appends on a debug file info about the features actually not used
     *
     * @param filePath path to the file to append info about the features on
     * @param message optional message
     */
    private void externalFileDebugAppend(String filePath, String message){
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePath, true));
            out.write("\n"+message+"\n");

            Iterator<ShapeFeature> it = RecognizerManager.features_.keySet().iterator();
            while (it.hasNext()) {
                ShapeFeature feat = (ShapeFeature) features_.get(it.next());
                out.write(feat.getFeatureName() + ":" + feat.getFeatureValue() + "\n");
            }
            out.close();
        } catch (IOException e) {
        }

    }

    /**
     * Starts the agent-based recognizing process
     * The strokes are assumed to represent one symbol,
     * all the strokes are collapsed into one stroke
     *
     * @param strokeList the list of strokes to be recognized
     */
    public void recognize(List<Stroke> strokeList) {
        RecognizerManager.contraintRecognition_ = false;
        RecognizerManager.strokeList_ = strokeList;
        //unisce tutti gli stroke in un unico stroke in modo da
        //applicarci le feature:
        Stroke strNew = new Stroke();
        Point2D fp;
        Point2D lp;
        TimeInterval ti = new TimeInterval();
        if (strokeList.size() == 1)//1 stroke;
        {
            RecognizerManager.stroke_ = strokeList.get(0);
        } else if (strokeList.size() == 2) {//2 stroke;
            Stroke sf = strokeList.get(0);
            Stroke sl = strokeList.get(1);
            strNew.setFirstPoint(sf.getFirstPoint());
            strNew.setLastPoint(sl.getLastPoint());
            ti.setStartMillis(sf.getTimeInfo().getStartMillis());
            ti.setEndMillis(sl.getTimeInfo().getEndMillis());
            strNew.setTimeInterval(ti);
            List<Point2D> concList = sf.getPointsList();
            concList.addAll(sl.getPointsList());
            strNew.setPointsList(concList);
            RecognizerManager.stroke_=strNew;
        } else {
            //da 3 stroke in su
            for (int i = 0; i < strokeList.size(); i++) {
                Stroke s = strokeList.get(i);
                if (i == 0) {//primo stroke;
                    fp = s.getFirstPoint();
                    strNew.setFirstPoint(fp);
                    ti.setStartMillis(s.getTimeInfo().getStartMillis());
                    strNew.setTimeInterval(ti);
                    strNew.setPointsList(s.getPointsList());
                } else if (i > 0 && i < strokeList.size() - 1) {//stroke intermedi
                    List<Point2D> concList = strNew.getPointsList();
                    concList.addAll(s.getPointsList());
                    strNew.setPointsList(concList);
                } else {//ultimo stroke
                    lp = s.getLastPoint();
                    strNew.setLastPoint(lp);
                    List<Point2D> concList = strNew.getPointsList();
                    concList.addAll(s.getPointsList());
                    strNew.setPointsList(concList);
                    strNew.getTimeInfo().setEndMillis(s.getTimeInfo().getEndMillis());
                }
            }
            RecognizerManager.stroke_=strNew;
        }

        
        RecognizerManager.hullValues_ = new PolygonValues(stroke_.getPointsList());
        this.startRecognizers();
    }

    /**
     * Starts the agent-based recognizing process
     * The stroke list in input is assumed to represent more symbols,
     * recognizers are launched for each stroke
     *
     * @param strokeList the list of strokes to be recognized
     */
    public void recognizeWithConstraints(List<Stroke> strokeList) {
        RecognizerManager.contraintRecognition_ = true;
        RecognizerManager.strokeList_ = strokeList;
        RecognizerManager.stroke_ = strokeList_.get(0);
        //consuma il primo elemento della lista per il riconoscimento
        strokeList_.remove(0);
        //lancia il riconoscimento
        RecognizerManager.hullValues_ = new PolygonValues(stroke_.getPointsList());
        this.startRecognizers();
    }

    /**
     * Starts the agent-based recognizing process
     * @param stroke the stroke to be recognized
     */
    public void recognize(Stroke stroke) {
        RecognizerManager.contraintRecognition_ = false;
        RecognizerManager.stroke_ = stroke;
        RecognizerManager.hullValues_ = new PolygonValues(stroke_.getPointsList());
        this.startRecognizers();
    }

    /**
     * The heart of the recognition process; agents are running and informing
     * the manager about their results.
     */
    @Override
    public void run() {
        //resetta i recognizers
        resetAgents();

        //reinizializza le feature
        Iterator<ShapeFeature> it = RecognizerManager.features_.keySet().iterator();
        while (it.hasNext()) {
            ShapeFeature feat = (ShapeFeature) features_.get(it.next());
            feat.clearFeatureValue();
        }

        startTime_ = System.currentTimeMillis();

        //esegue il calcolo delle feature
        it = RecognizerManager.features_.keySet().iterator();
        while (it.hasNext()) {
            ShapeFeature feat = (ShapeFeature) features_.get(it.next());
            feat.calculate(hullValues_);
        }
        //DEBUG ON SCREEN
        it = RecognizerManager.features_.keySet().iterator();
        PanelAccessor.getOutputText().append("\n");
        while (it.hasNext()) {
            ShapeFeature feat = (ShapeFeature) features_.get(it.next());
            PanelAccessor.getOutputText().append(feat.getFeatureName() + ":" + feat.getFeatureValue() + "\n");
        }

        //lancia i Recognizers
        Iterator<BaseRecognizer> itrec = this.getRecognizers_().iterator();
        while (itrec.hasNext()) {
            BaseRecognizer ar = itrec.next();
            //ogni agente conosce tutte le features e lo stroke
            ar.setFeatures(features_);
            ar.setStroke(stroke_);
            new Thread(ar).start();
        }


        Iterator<BaseRecognizer> itRec;
        boolean conflict = false;
        while (running_) {

            System.out.println("[DEBUG]: Nel WHILE di RECMAMANAGER");
//            Thread.yield();
            try {
              Thread.sleep(400);
            } catch (InterruptedException ex) {
            }

            boolean finished = true;

            //Polling
            itRec = recognizers_.iterator();
            while (itRec.hasNext()) {
                BaseRecognizer br = itRec.next();
                System.out.println("[DEBUG]:Polling " + br.getRecognizerName() + "Finished: " + br.isFinished()
                        + "Simbolo: " + br.getRecognizedSymbol().toString() + " Prob:" + br.getCorrespondency() + "%");
                finished = finished && br.isFinished();
            }
            itRec = null;
            //

            if (finished) {
                System.out.println("TUTTI HANNO FINITO");
                //ordina i recognizers in base alla correspondency
                Collections.sort(recognizers_);
                //in modo decrescente
                Collections.reverse(recognizers_);

                //controlla i primi 2 risultati, essendo la lista ordinata
                BaseRecognizer ar = recognizers_.get(0);
                int arC = ar.getCorrespondency();
                String arName = ar.getRecognizerName();
                Enum symbol = ar.getRecognizedSymbol();
                System.out.println("[DEBUG]:" + arName + "ha riconosciuto" + symbol + "con prob" + arC);

                BaseRecognizer arComp = recognizers_.get(1);
                int arCompC = arComp.getCorrespondency();
                String arCompName = arComp.getRecognizerName();
                Enum symbolC = arComp.getRecognizedSymbol();
                //caso di conflitto
                int diff=Math.abs(arC-arCompC);
                if (diff<=CONFLICT_THRESHOLD && !symbolC.equals(BaseRecognizer.SHAPE_TYPES.UNKNOWN)
                        && !symbol.equals(BaseRecognizer.SHAPE_TYPES.UNKNOWN)) {
                    if (!conflict) {
                        System.out.println("[DEBUG]: Conflitto fra " + arName + " e " + arCompName);
                        PanelAccessor.getOutputText().append("\n" + "Conflitto fra " + arName + " e " + arCompName);
                        ar.markForConflict(symbolC.toString());
                        arComp.markForConflict(symbol.toString());
                        conflict = true;
                    } else {
                        PanelAccessor.getOutputText().append("\n" + "Conflitto non risolto, scelta arbitraria");
                        conflict = false;
                        RecognizerManager.correspondency_ = arC;
                        RecognizerManager.symbolName_ = symbol.toString();
                        RecognizerManager.running_ = false;
                    }
                } else {
                    //non c'e conflitto
                    RecognizerManager.correspondency_ = arC;
                    RecognizerManager.symbolName_ = symbol.toString();
                    RecognizerManager.running_ = false;
                }
            }
        }
        endTime_ = System.currentTimeMillis();
        System.out.println("[DEBUG]: Riconosciuto simbolo: " + RecognizerManager.symbolName_ + "con probabilita: " + RecognizerManager.correspondency_ + "%");
        PanelAccessor.getOutputText().append("\n" + "Riconosciuto simbolo: " + RecognizerManager.symbolName_ + "con probabilita: " + RecognizerManager.correspondency_ + "%");
        long tempo = endTime_ - startTime_;
        PanelAccessor.getOutputText().append("\n" + "Riconoscimento in" + tempo + " ms");

        PanelAccessor.getSvgPanel().drawBeautifiedSymbol(symbolName_, 0.0, 0.0, 0.0, 0.0);
        //ferma gli agenti
        resetAgents();
        if (RecognizerManager.contraintRecognition_) {
            symbolNames_.add(symbolName_);
            if (strokeList_.size() > 0) {
                //ricomincia il processo di riconoscimento per il prossimo stroke
                recognizeWithConstraints(strokeList_);
            }else{
                RecognizerManager.contraintRecognition_=false;
                //TODO: chiamata al constraint checker

                //reinizializzazione a vuoto della lista di simboli riconosciuti
                RecognizerManager.symbolNames_=null;
                RecognizerManager.symbolNames_=new LinkedList<String>();
            }
        }
    }

    /**
     * Resets the agents preparing them for the next stroke recognition
     */
    private synchronized void resetAgents() {
        RecognizerManager.correspondency_ = 0;
        RecognizerManager.symbolName_ = BaseRecognizer.SHAPE_TYPES.UNKNOWN.toString();
        Iterator<BaseRecognizer> it = recognizers_.iterator();
        while (it.hasNext()) {
            BaseRecognizer ar = it.next();
            ar.resetAgent();
        }

    }

    /**
     * Returns a stroke to be recognized
     *
     * @return the stroke, a stroke to be recognized
     */
    public Stroke getStroke() {
        return stroke_;
    }

    /**
     * Sets the stroke for recognition
     *
     * @param stroke_ the stroke to set for recognition
     */
    public void setStroke(Stroke stroke_) {
        RecognizerManager.stroke_ = stroke_;
    }

    /**
     * return the common special polygons values
     *
     * @return the common hull values
     */
    public PolygonValues getHullValues() {
        return RecognizerManager.hullValues_;
    }

    /**
     * Sets the common convex hull values to be calculated
     *
     * @param hullValues the common convex hull values to be calculated
     */
    public void setHullValues(PolygonValues hullValues) {
        RecognizerManager.hullValues_ = hullValues;
    }

    /**
     * Returns the running, true if thread is running, false otherwise
     *
     * @return the running
     */
    public synchronized boolean isRunning() {
        return running_;
    }

    /**
     * Sets the running, true if thread is running, false otherwise
     *
     * @param running the running to set
     */
    public synchronized void setRunning(boolean running) {
        RecognizerManager.running_ = running;
    }

    /**
     * Returns the name of the symbol
     *
     * @return the name of the symbol
     */
    public synchronized String getSymbolName() {
        return symbolName_;
    }

    /**
     * Sets the name of the symbol
     *
     * @param symbolName_ the symbol Name to set
     */
    public synchronized void setSymbolName(String symbolName_) {
        RecognizerManager.symbolName_ = symbolName_;
    }

    /**
     * Returns the list of recognizers
     *
     * @return the list of recognizers
     */
    public synchronized List<BaseRecognizer> getRecognizers_() {
        return recognizers_;
    }

    /**
     * return the list of strokes
     *
     * @return the strokeList_
     */
    public static List<Stroke> getStrokeList_() {
        return strokeList_;
    }

    /**
     * Sets the stroke list
     *
     * @param aStrokeList_ the stroke List to set
     */
    public static void setStrokeList_(List<Stroke> aStrokeList_) {
        strokeList_ = aStrokeList_;
    }
}

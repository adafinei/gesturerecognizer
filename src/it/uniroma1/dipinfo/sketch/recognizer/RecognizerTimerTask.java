package it.uniroma1.dipinfo.sketch.recognizer;

import it.uniroma1.dipinfo.sketch.gui.strokeproducer.StrokeProducer;
import java.util.TimerTask;

/**
 * The run() method of this class is automatically invoked when 1800 sec. time out
 * This is the time to wait before deciding that the user has finished
 * drawing the symbol
 */
public class RecognizerTimerTask extends TimerTask {

    private StrokeProducer strokeProducer_;

    public RecognizerTimerTask(StrokeProducer theStrokeProducer) {

        strokeProducer_ = theStrokeProducer;
    }

    /**
     * Launches the recognition process, and clears the strokes traced
     * until now
     */
    public void run() {

        new RecognizerManager().recognize(strokeProducer_.getStrokes());
        strokeProducer_.clearStrokes();
    }
}

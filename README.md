# README #

### This project is a proof of concept for simple gesture recognition. It is composed by several parts: ###

- video capture (color-based)
- stroke analyser & controller
- agent-based stroke recognizer
 
### Work in progress for component extraction and mavenisation as a library ###

* The Recognizer comes as a buildable NetBeans project
* Extraction as a library ongoing
* Version 1.0

### How do I get set up? ###

* Netbeans 8.1 project

### Contribution guidelines ###

### Who do I talk to? ###

* Repo owner or admin: adafinei